# IO-Link Arduino Device Shield

Hardware developed for the Arduino IO-Link Device framework from the paper "Arduino based Framework for Rapid Application Development of a Generic IO-Link interface" https://doi.org/10.1007/978-3-662-59895-5_2.

## License

This design is licensed under the CERN-OHL-S-2.0 license. You may redistribute and modify this source and make products using it under the terms of the CERN-OHL-S v2 (https://ohwr.org/cern_ohl_s_v2.txt). 

This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions. As per CERN-OHL-S v2 section 4, should You produce hardware based on this source, You must where practicable maintain the Source Location visible on the external case of the IO-Link Arduino Device Shield or other products you make using this source.

## Directory structure

- 3D Models: Step file models for the 3D PCB View
- Calculations: Any type of calculations done for the design.
- CAM: CAM files ready for production
- Design: The design files for the PCB
- FH logo: Logo embedded in the PCB Design
- Quotes: Quotations done for the PCB 

## Requirements

- Eagle >=9.1.x

## Pinout

![Pinout](images/pinout_topview.png)


| Shield Pin | Arduino Pin  |  TIOL111 Pin  |
| :-------- | :--------:    |:--------:     |
| 1         |     TX        | TX            |
| 2         |     RX        | RX            |
| 4         |     GND       | L-            |
| 5         |     D2        | WAKE          |
| 7         |     D4        | EN            |
### Notes

The design does not include any type of protection against and not limited to:
- Overcurrent
- Over voltage
- Reverse polarity
- Short circuit
- ESD
- Over temperature

