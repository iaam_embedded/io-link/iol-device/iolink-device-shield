<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.4.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="190" name="Class0" color="2" fill="1" visible="no" active="yes"/>
<layer number="191" name="Class1" color="7" fill="1" visible="no" active="yes"/>
<layer number="192" name="Class2" color="15" fill="1" visible="no" active="yes"/>
<layer number="193" name="Class3" color="13" fill="1" visible="no" active="yes"/>
<layer number="194" name="Class4" color="12" fill="1" visible="no" active="yes"/>
<layer number="195" name="Class5" color="6" fill="1" visible="no" active="yes"/>
<layer number="196" name="Class6" color="9" fill="1" visible="no" active="yes"/>
<layer number="197" name="Class7" color="1" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="11" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="10" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="14" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="12" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="9" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="252" name="Spannung" color="9" fill="1" visible="no" active="yes"/>
<layer number="253" name="Strom" color="12" fill="1" visible="no" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Arduino-clone">
<description>Arduino Clone pinheaders
By cl@xganon.com
http://www.xganon.com</description>
<packages>
<package name="NANO">
<pad name="TX0" x="7.62" y="-10.16" drill="1.02" shape="long"/>
<pad name="RX1" x="7.62" y="-7.62" drill="1.02" shape="long"/>
<pad name="RST1" x="7.62" y="-5.08" drill="1.02" shape="long"/>
<pad name="GND1" x="7.62" y="-2.54" drill="1.02" shape="long"/>
<pad name="D2" x="7.62" y="0" drill="1.02" shape="long"/>
<pad name="D3" x="7.62" y="2.54" drill="1.02" shape="long"/>
<pad name="D4" x="7.62" y="5.08" drill="1.02" shape="long"/>
<pad name="D5" x="7.62" y="7.62" drill="1.02" shape="long"/>
<pad name="D6" x="7.62" y="10.16" drill="1.02" shape="long"/>
<pad name="D7" x="7.62" y="12.7" drill="1.02" shape="long"/>
<pad name="D8" x="7.62" y="15.24" drill="1.02" shape="long"/>
<pad name="D9" x="7.62" y="17.78" drill="1.02" shape="long"/>
<pad name="RAW" x="-7.62" y="-10.16" drill="1.02" shape="long"/>
<pad name="GND" x="-7.62" y="-7.62" drill="1.02" shape="long"/>
<pad name="RST" x="-7.62" y="-5.08" drill="1.02" shape="long"/>
<pad name="A3" x="-7.62" y="10.16" drill="1.02" shape="long"/>
<pad name="A2" x="-7.62" y="12.7" drill="1.02" shape="long"/>
<pad name="A1" x="-7.62" y="15.24" drill="1.02" shape="long"/>
<pad name="A0" x="-7.62" y="17.78" drill="1.02" shape="long"/>
<pad name="D13" x="-7.62" y="25.4" drill="1.02" shape="long"/>
<pad name="D12" x="7.62" y="25.4" drill="1.02" shape="long"/>
<pad name="D11" x="7.62" y="22.86" drill="1.02" shape="long"/>
<pad name="D10" x="7.62" y="20.32" drill="1.02" shape="long"/>
<pad name="3.3V" x="-7.62" y="22.86" drill="1.02" shape="long" rot="R180"/>
<pad name="AREF" x="-7.62" y="20.32" drill="1.02" shape="long" rot="R180"/>
<pad name="5V" x="-7.62" y="-2.54" drill="1.02" shape="long"/>
<pad name="A4" x="-7.62" y="7.62" drill="1.02" shape="long" rot="R180"/>
<pad name="A5" x="-7.62" y="5.08" drill="1.02" shape="long" rot="R180"/>
<pad name="A6" x="-7.62" y="2.54" drill="1.02" shape="long" rot="R180"/>
<pad name="A7" x="-7.62" y="0" drill="1.02" shape="long" rot="R180"/>
<text x="-4.4" y="7.6" size="0.8" layer="37" ratio="16" distance="219" align="center">D13
3.3V
REF
A0
A1
A2
A3
A4
A5
A6
A7
5V
RST
GND
VIN</text>
<pad name="GND2" x="-7.62" y="27.94" drill="1.02" shape="long"/>
<pad name="GND3" x="7.62" y="27.94" drill="1.02" shape="long"/>
</package>
</packages>
<symbols>
<symbol name="NANO">
<pin name="3.3V" x="-22.86" y="12.7" visible="pin" length="middle" direction="pwr"/>
<pin name="AREF" x="-22.86" y="10.16" visible="pin" length="middle"/>
<pin name="TX0" x="10.16" y="-20.32" visible="pin" length="middle" rot="R180"/>
<pin name="RX1" x="10.16" y="-17.78" visible="pin" length="middle" rot="R180"/>
<pin name="RST1" x="10.16" y="-15.24" visible="pin" length="middle" rot="R180"/>
<pin name="GND2" x="10.16" y="-12.7" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="D2" x="10.16" y="-10.16" visible="pin" length="middle" rot="R180"/>
<pin name="D3" x="10.16" y="-7.62" visible="pin" length="middle" rot="R180"/>
<pin name="D4" x="10.16" y="-5.08" visible="pin" length="middle" rot="R180"/>
<pin name="D5" x="10.16" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="D6" x="10.16" y="0" visible="pin" length="middle" rot="R180"/>
<pin name="D7" x="10.16" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="D8" x="10.16" y="5.08" visible="pin" length="middle" rot="R180"/>
<pin name="D9" x="10.16" y="7.62" visible="pin" length="middle" rot="R180"/>
<pin name="RAW" x="-22.86" y="-20.32" visible="pin" length="middle" direction="pwr"/>
<pin name="GND" x="-22.86" y="-17.78" visible="pin" length="middle" direction="pwr"/>
<pin name="RST" x="-22.86" y="-15.24" visible="pin" length="middle"/>
<pin name="5.5V" x="-22.86" y="-12.7" visible="pin" length="middle"/>
<pin name="A3" x="-22.86" y="0" visible="pin" length="middle"/>
<pin name="A2" x="-22.86" y="2.54" visible="pin" length="middle"/>
<pin name="A1" x="-22.86" y="5.08" visible="pin" length="middle"/>
<pin name="A0" x="-22.86" y="7.62" visible="pin" length="middle"/>
<pin name="D13" x="-22.86" y="15.24" visible="pin" length="middle" direction="pwr"/>
<pin name="D12" x="10.16" y="15.24" visible="pin" length="middle" rot="R180"/>
<pin name="D11" x="10.16" y="12.7" visible="pin" length="middle" rot="R180"/>
<pin name="D10" x="10.16" y="10.16" visible="pin" length="middle" rot="R180"/>
<pin name="A7" x="-22.86" y="-10.16" visible="pin" length="middle"/>
<pin name="A6" x="-22.86" y="-7.62" visible="pin" length="middle"/>
<pin name="A5" x="-22.86" y="-5.08" visible="pin" length="middle"/>
<pin name="A4" x="-22.86" y="-2.54" visible="pin" length="middle"/>
<wire x1="-17.78" y1="17.78" x2="-17.78" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-30.48" x2="5.08" y2="-30.48" width="0.254" layer="94"/>
<wire x1="5.08" y1="-30.48" x2="5.08" y2="17.78" width="0.254" layer="94"/>
<wire x1="5.08" y1="17.78" x2="-17.78" y2="17.78" width="0.254" layer="94"/>
<text x="-12.7" y="20.32" size="1.778" layer="95">Arduino Nano</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="NANO" prefix="U">
<description>Arduino Nano</description>
<gates>
<gate name="G$1" symbol="NANO" x="7.62" y="2.54"/>
</gates>
<devices>
<device name="" package="NANO">
<connects>
<connect gate="G$1" pin="3.3V" pad="3.3V"/>
<connect gate="G$1" pin="5.5V" pad="5V"/>
<connect gate="G$1" pin="A0" pad="A0"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="A6" pad="A6"/>
<connect gate="G$1" pin="A7" pad="A7"/>
<connect gate="G$1" pin="AREF" pad="AREF"/>
<connect gate="G$1" pin="D10" pad="D10"/>
<connect gate="G$1" pin="D11" pad="D11"/>
<connect gate="G$1" pin="D12" pad="D12"/>
<connect gate="G$1" pin="D13" pad="D13"/>
<connect gate="G$1" pin="D2" pad="D2"/>
<connect gate="G$1" pin="D3" pad="D3"/>
<connect gate="G$1" pin="D4" pad="D4"/>
<connect gate="G$1" pin="D5" pad="D5"/>
<connect gate="G$1" pin="D6" pad="D6"/>
<connect gate="G$1" pin="D7" pad="D7"/>
<connect gate="G$1" pin="D8" pad="D8"/>
<connect gate="G$1" pin="D9" pad="D9"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND2" pad="GND1"/>
<connect gate="G$1" pin="RAW" pad="RAW"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="RST1" pad="RST1"/>
<connect gate="G$1" pin="RX1" pad="RX1"/>
<connect gate="G$1" pin="TX0" pad="TX0"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Resistors">
<description>&lt;h3&gt;SparkFun Resistors&lt;/h3&gt;
This library contains resistors. Reference designator:R. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="AXIAL-0.3">
<description>&lt;h3&gt;AXIAL-0.3&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;/p&gt;</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.016" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.016" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="AXIAL-0.1">
<description>&lt;h3&gt;AXIAL-0.1&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.1" pitch between holes.&lt;/p&gt;</description>
<wire x1="0" y1="-0.762" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="0.762" width="0.2032" layer="21"/>
<wire x1="0.254" y1="0" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-0.254" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-1.27" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="1.27" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.143" size="0.6096" layer="21" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="AXIAL-0.1-KIT">
<description>&lt;h3&gt;AXIAL-0.1-KIT&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.1" pitch between holes.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.1 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;</description>
<wire x1="0" y1="-0.762" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="0.762" width="0.2032" layer="21"/>
<wire x1="0.254" y1="0" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-0.254" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-1.27" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="1.27" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<circle x="-1.27" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="-1.27" y="0" radius="1.016" width="0" layer="30"/>
<circle x="1.27" y="0" radius="1.016" width="0" layer="30"/>
<circle x="-1.27" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="1.27" y="0" radius="0.4572" width="0" layer="29"/>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="0" y="1.524" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="0" y="1.524" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="10KOHM" prefix="R">
<description>&lt;h3&gt;10kΩ resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-HORIZ-1/4W-1%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/4W-1%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/4W-1%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/4W-5%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/4W-5%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ-1/4W-5%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/4W-5%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/4W-1%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/6W-5%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/6W-5%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ-1/6W-5%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/6W-5%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-0603-1/10W-1%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-00824"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Capacitors&lt;/h3&gt;
This library contains capacitors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="1206">
<description>&lt;p&gt;&lt;b&gt;Generic 3216 (1206) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-2.4" y1="1.1" x2="2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="-1.1" x2="-2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-2.4" y1="-1.1" x2="-2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="1.1" x2="2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0805">
<description>&lt;p&gt;&lt;b&gt;Generic 2012 (0805) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="0" y="0.889" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.889" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-1.5" y1="0.8" x2="1.5" y2="0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="-0.8" x2="-1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="-1.5" y1="-0.8" x2="-1.5" y2="0.8" width="0.0508" layer="39"/>
</package>
<package name="1210">
<description>&lt;p&gt;&lt;b&gt;Generic 3225 (1210) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.5365" y1="1.1865" x2="1.5365" y2="1.1865" width="0.127" layer="51"/>
<wire x1="1.5365" y1="1.1865" x2="1.5365" y2="-1.1865" width="0.127" layer="51"/>
<wire x1="1.5365" y1="-1.1865" x2="-1.5365" y2="-1.1865" width="0.127" layer="51"/>
<wire x1="-1.5365" y1="-1.1865" x2="-1.5365" y2="1.1865" width="0.127" layer="51"/>
<smd name="1" x="-1.755" y="0" dx="1.27" dy="2.06" layer="1"/>
<smd name="2" x="1.755" y="0" dx="1.27" dy="2.06" layer="1"/>
<text x="0" y="1.397" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.397" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-2.59" y1="1.45" x2="2.59" y2="1.45" width="0.0508" layer="39"/>
<wire x1="2.59" y1="1.45" x2="2.59" y2="-1.45" width="0.0508" layer="39"/>
<wire x1="2.59" y1="-1.45" x2="-2.59" y2="-1.45" width="0.0508" layer="39"/>
<wire x1="-2.59" y1="-1.45" x2="-2.59" y2="1.45" width="0.0508" layer="39"/>
</package>
<package name="0402">
<description>&lt;p&gt;&lt;b&gt;Generic 1005 (0402) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-0.2704" y1="0.2286" x2="0.2704" y2="0.2286" width="0.1524" layer="51"/>
<wire x1="0.2704" y1="-0.2286" x2="-0.2704" y2="-0.2286" width="0.1524" layer="51"/>
<wire x1="-1.2" y1="0.65" x2="1.2" y2="0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="0.65" x2="1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="-0.65" x2="-1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="-1.2" y1="-0.65" x2="-1.2" y2="0.65" width="0.0508" layer="39"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.3048" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="10UF" prefix="C">
<description>&lt;h3&gt;10.0µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-6.3V-20%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-11015"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-1206-6.3V-20%" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-10057"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-0805-10V-10%" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-11330"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-1210-50V-20%" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-09824"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1.0UF" prefix="C">
<description>&lt;h3&gt;1µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-16V-10%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00868"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0402-16V-10%" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-12417"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0805-25V-(+80/-20%)" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-11625"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-1206-50V-10%" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-09822"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0805-25V-10%" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08064"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0603-16V-10%-X7R" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-13930"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors" urn="urn:adsk.eagle:library:510">
<description>&lt;h3&gt;SparkFun Capacitors&lt;/h3&gt;
This library contains capacitors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1206" urn="urn:adsk.eagle:footprint:37399/1" library_version="1">
<description>&lt;p&gt;&lt;b&gt;Generic 3216 (1206) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-2.4" y1="1.1" x2="2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="-1.1" x2="-2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-2.4" y1="-1.1" x2="-2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="1.1" x2="2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0603" urn="urn:adsk.eagle:footprint:37386/1" library_version="1">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402" urn="urn:adsk.eagle:footprint:37389/1" library_version="1">
<description>&lt;p&gt;&lt;b&gt;Generic 1005 (0402) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-0.2704" y1="0.2286" x2="0.2704" y2="0.2286" width="0.1524" layer="51"/>
<wire x1="0.2704" y1="-0.2286" x2="-0.2704" y2="-0.2286" width="0.1524" layer="51"/>
<wire x1="-1.2" y1="0.65" x2="1.2" y2="0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="0.65" x2="1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="-0.65" x2="-1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="-1.2" y1="-0.65" x2="-1.2" y2="0.65" width="0.0508" layer="39"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.3048" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0805" urn="urn:adsk.eagle:footprint:37400/1" library_version="1">
<description>&lt;p&gt;&lt;b&gt;Generic 2012 (0805) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="0" y="0.889" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.889" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-1.5" y1="0.8" x2="1.5" y2="0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="-0.8" x2="-1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="-1.5" y1="-0.8" x2="-1.5" y2="0.8" width="0.0508" layer="39"/>
</package>
<package name="1210" urn="urn:adsk.eagle:footprint:37401/1" library_version="1">
<description>&lt;p&gt;&lt;b&gt;Generic 3225 (1210) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.5365" y1="1.1865" x2="1.5365" y2="1.1865" width="0.127" layer="51"/>
<wire x1="1.5365" y1="1.1865" x2="1.5365" y2="-1.1865" width="0.127" layer="51"/>
<wire x1="1.5365" y1="-1.1865" x2="-1.5365" y2="-1.1865" width="0.127" layer="51"/>
<wire x1="-1.5365" y1="-1.1865" x2="-1.5365" y2="1.1865" width="0.127" layer="51"/>
<wire x1="-2.59" y1="1.45" x2="2.59" y2="1.45" width="0.0508" layer="39"/>
<wire x1="2.59" y1="1.45" x2="2.59" y2="-1.45" width="0.0508" layer="39"/>
<wire x1="2.59" y1="-1.45" x2="-2.59" y2="-1.45" width="0.0508" layer="39"/>
<wire x1="-2.59" y1="-1.45" x2="-2.59" y2="1.45" width="0.0508" layer="39"/>
<smd name="1" x="-1.755" y="0" dx="1.27" dy="2.06" layer="1"/>
<smd name="2" x="1.755" y="0" dx="1.27" dy="2.06" layer="1"/>
<text x="0" y="1.397" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.397" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="CAP-PTH-SMALL-KIT" urn="urn:adsk.eagle:footprint:37404/1" library_version="1">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
</packages>
<packages3d>
<package3d name="1206" urn="urn:adsk.eagle:package:37426/1" type="box" library_version="1">
<description>Generic 3216 (1206) package
0.2mm courtyard excess rounded to nearest 0.05mm.</description>
<packageinstances>
<packageinstance name="1206"/>
</packageinstances>
</package3d>
<package3d name="0603" urn="urn:adsk.eagle:package:37414/1" type="box" library_version="1">
<description>Generic 1608 (0603) package
0.2mm courtyard excess rounded to nearest 0.05mm.</description>
<packageinstances>
<packageinstance name="0603"/>
</packageinstances>
</package3d>
<package3d name="0402" urn="urn:adsk.eagle:package:37413/1" type="box" library_version="1">
<description>Generic 1005 (0402) package
0.2mm courtyard excess rounded to nearest 0.05mm.</description>
<packageinstances>
<packageinstance name="0402"/>
</packageinstances>
</package3d>
<package3d name="0805" urn="urn:adsk.eagle:package:37429/1" type="box" library_version="1">
<description>Generic 2012 (0805) package
0.2mm courtyard excess rounded to nearest 0.05mm.</description>
<packageinstances>
<packageinstance name="0805"/>
</packageinstances>
</package3d>
<package3d name="1210" urn="urn:adsk.eagle:package:37436/1" type="box" library_version="1">
<description>Generic 3225 (1210) package
0.2mm courtyard excess rounded to nearest 0.05mm.</description>
<packageinstances>
<packageinstance name="1210"/>
</packageinstances>
</package3d>
<package3d name="CAP-PTH-SMALL-KIT" urn="urn:adsk.eagle:package:37428/1" type="box" library_version="1">
<description>CAP-PTH-SMALL-KIT
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).

Warning: This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<packageinstances>
<packageinstance name="CAP-PTH-SMALL-KIT"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="CAP" urn="urn:adsk.eagle:symbol:37385/1" library_version="1">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1.0UF" urn="urn:adsk.eagle:component:37474/1" prefix="C" library_version="1">
<description>&lt;h3&gt;1µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-16V-10%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37414/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00868"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0402-16V-10%" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37413/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-12417"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0805-25V-(+80/-20%)" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37429/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-11625"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-1206-50V-10%" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37426/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-09822"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0805-25V-10%" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37429/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08064"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="100PF" urn="urn:adsk.eagle:component:37440/1" prefix="C" library_version="1">
<description>&lt;h3&gt;100pF/0.1nF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-50V-5%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37414/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-07883"/>
<attribute name="VALUE" value="100pF"/>
</technology>
</technologies>
</device>
<device name="-0402-50V-5%" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37413/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-13458" constant="no"/>
<attribute name="VALUE" value="100PF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="22UF" urn="urn:adsk.eagle:component:37479/1" prefix="C" library_version="1">
<description>&lt;h3&gt;22µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0805-6.3V-20%" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37429/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08402"/>
<attribute name="VALUE" value="22uF"/>
</technology>
</technologies>
</device>
<device name="-1210-16V-20%" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37436/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0.1UF" urn="urn:adsk.eagle:component:37472/1" prefix="C" library_version="1">
<description>&lt;h3&gt;0.1µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0402-16V-10%" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37413/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-12416"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-25V-(+80/-20%)" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37414/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00810"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-25V-5%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37414/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08604"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-KIT-EZ-50V-20%" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37428/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08370"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-100V-10%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37414/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08390"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="diode" urn="urn:adsk.eagle:library:210">
<description>&lt;b&gt;Diodes&lt;/b&gt;&lt;p&gt;
Based on the following sources:
&lt;ul&gt;
&lt;li&gt;Motorola : www.onsemi.com
&lt;li&gt;Fairchild : www.fairchildsemi.com
&lt;li&gt;Philips : www.semiconductors.com
&lt;li&gt;Vishay : www.vishay.de
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SOD323_ST" urn="urn:adsk.eagle:footprint:43248/1" library_version="2">
<description>&lt;b&gt;SOD-323&lt;/b&gt;&lt;p&gt;
Source: www.st.com, BAT60J.pdf</description>
<wire x1="-0.85" y1="0.55" x2="0.85" y2="0.55" width="0.1016" layer="21"/>
<wire x1="0.85" y1="0.55" x2="0.85" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="0.85" y1="-0.55" x2="-0.85" y2="-0.55" width="0.1016" layer="21"/>
<wire x1="-0.85" y1="-0.55" x2="-0.85" y2="0.55" width="0.1016" layer="51"/>
<smd name="C" x="-1.25" y="0" dx="0.8" dy="0.6" layer="1"/>
<smd name="A" x="1.25" y="0" dx="0.8" dy="0.6" layer="1"/>
<text x="-1.65" y="0.75" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.65" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.65" y1="-0.55" x2="-0.05" y2="0.55" layer="21"/>
<rectangle x1="-1.35" y1="-0.15" x2="-0.8" y2="0.15" layer="51"/>
<rectangle x1="0.8" y1="-0.15" x2="1.35" y2="0.15" layer="51"/>
<rectangle x1="-0.85" y1="-0.55" x2="-0.05" y2="0.55" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="SOD323_ST" urn="urn:adsk.eagle:package:43461/1" type="box" library_version="2">
<description>SOD-323
Source: www.st.com, BAT60J.pdf</description>
<packageinstances>
<packageinstance name="SOD323_ST"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SCHOTTKY" urn="urn:adsk.eagle:symbol:43101/2" library_version="7">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.016" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<text x="-2.286" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BAT60J" urn="urn:adsk.eagle:component:43665/3" prefix="D" library_version="8">
<description>&lt;b&gt;Schottky barrier diode&lt;/b&gt;&lt;p&gt;
Source: www.st.com, BAT60J.pdf</description>
<gates>
<gate name="G$1" symbol="SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD323_ST">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:43461/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="7" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC" urn="urn:adsk.eagle:symbol:13874/1" library_version="1">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" urn="urn:adsk.eagle:component:13926/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IO-Link_Shield">
<packages>
<package name="SOIC127P700X170-9T262X351" urn="urn:adsk.eagle:footprint:10239987/1" locally_modified="yes">
<description>8-SOIC, 1.27 mm pitch, 7.00 mm span, 4.90 X 3.90 X 1.70 mm body, 3.51 X 2.62 mm thermal pad
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 7.00 mm span with body size 4.90 X 3.90 X 1.70 mm and thermal pad size 3.51 X 2.62 mm&lt;/p&gt;</description>
<circle x="-2.953" y="2.7999" radius="0.25" width="0" layer="21"/>
<wire x1="-2" y1="2.6099" x2="2" y2="2.6099" width="0.12" layer="21"/>
<wire x1="-2" y1="-2.6099" x2="2" y2="-2.6099" width="0.12" layer="21"/>
<wire x1="2" y1="-2.5" x2="-2" y2="-2.5" width="0.12" layer="51"/>
<wire x1="-2" y1="-2.5" x2="-2" y2="2.5" width="0.12" layer="51"/>
<wire x1="-2" y1="2.5" x2="2" y2="2.5" width="0.12" layer="51"/>
<wire x1="2" y1="2.5" x2="2" y2="-2.5" width="0.12" layer="51"/>
<smd name="1" x="-2.7" y="1.905" dx="1.6" dy="0.61" layer="1"/>
<smd name="2" x="-2.7" y="0.635" dx="1.6" dy="0.61" layer="1"/>
<smd name="3" x="-2.7" y="-0.635" dx="1.6" dy="0.61" layer="1"/>
<smd name="4" x="-2.7" y="-1.905" dx="1.6" dy="0.61" layer="1"/>
<smd name="5" x="2.7" y="-1.905" dx="1.6" dy="0.61" layer="1"/>
<smd name="6" x="2.7" y="-0.635" dx="1.6" dy="0.61" layer="1"/>
<smd name="7" x="2.7" y="0.635" dx="1.6" dy="0.61" layer="1"/>
<smd name="8" x="2.7" y="1.905" dx="1.6" dy="0.61" layer="1"/>
<smd name="9" x="0" y="0" dx="2.62" dy="3.51" layer="1" thermals="no"/>
<text x="0" y="3.6849" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.2449" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="VSON-10">
<circle x="-2.159" y="1.27" radius="0.1" width="0.2" layer="21"/>
<text x="-2.529240625" y="2.016459375" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-2.517559375" y="-2.54333125" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-1.55" y1="1.3" x2="1.55" y2="1.3" width="0.05" layer="39"/>
<wire x1="1.55" y1="1.3" x2="1.55" y2="-1.3" width="0.05" layer="39"/>
<wire x1="1.55" y1="-1.3" x2="-1.55" y2="-1.3" width="0.05" layer="39"/>
<wire x1="-1.55" y1="-1.3" x2="-1.55" y2="1.3" width="0.05" layer="39"/>
<smd name="1" x="-1.49875" y="1" dx="0.69" dy="0.24" layer="1" roundness="8"/>
<smd name="2" x="-1.49875" y="0.5" dx="0.7475" dy="0.24" layer="1" roundness="8"/>
<smd name="4" x="-1.49875" y="-0.5" dx="0.7475" dy="0.24" layer="1" roundness="8"/>
<smd name="3" x="-1.49875" y="0" dx="0.7475" dy="0.24" layer="1" roundness="8"/>
<smd name="8" x="1.49875" y="0" dx="0.7475" dy="0.24" layer="1" roundness="8"/>
<smd name="7" x="1.49875" y="-0.5" dx="0.7475" dy="0.24" layer="1" roundness="8"/>
<smd name="5" x="-1.49875" y="-1" dx="0.7475" dy="0.24" layer="1" roundness="8"/>
<smd name="6" x="1.49875" y="-1" dx="0.7475" dy="0.24" layer="1"/>
<smd name="9" x="1.49875" y="0.5" dx="0.7475" dy="0.24" layer="1" roundness="8"/>
<smd name="10" x="1.49875" y="1" dx="0.7475" dy="0.24" layer="1" roundness="8"/>
<smd name="11" x="0" y="0" dx="1.65" dy="1.95" layer="1" roundness="8" cream="no"/>
</package>
<package name="CONEC_43-01204">
<hole x="7.5" y="0" drill="2.35"/>
<hole x="-7.5" y="0" drill="2.35"/>
<hole x="-7.5" y="4" drill="2.8"/>
<hole x="7.5" y="4" drill="2.8"/>
<pad name="3" x="1.767765625" y="1.767765625" drill="1.2" diameter="2.4"/>
<pad name="4" x="-1.767765625" y="1.767765625" drill="1.2" diameter="2.4"/>
<pad name="1" x="-1.767765625" y="-1.767765625" drill="1.2" diameter="2.4"/>
<pad name="2" x="1.767765625" y="-1.767765625" drill="1.2" diameter="2.4"/>
<text x="-13.433240625" y="3.286459375" size="2.2971" layer="25" font="fixed">&gt;NAME</text>
<text x="-13.802559375" y="-0.71833125" size="2.275959375" layer="27" font="fixed">&gt;VALUE</text>
<wire x1="-9" y1="5.4" x2="-7" y2="5.4" width="0.127" layer="21"/>
<wire x1="-7" y1="5.4" x2="7" y2="5.4" width="0.127" layer="21"/>
<wire x1="7" y1="5.4" x2="9" y2="5.4" width="0.127" layer="21"/>
<wire x1="9" y1="5.4" x2="9" y2="-1.767765625" width="0.127" layer="21"/>
<wire x1="-9" y1="5.4" x2="-9" y2="-1.767765625" width="0.127" layer="21"/>
<wire x1="-6" y1="24.8" x2="6" y2="24.8" width="0.127" layer="39"/>
<wire x1="-6" y1="24.8" x2="-6" y2="9" width="0.127" layer="39"/>
<wire x1="6" y1="24.8" x2="6" y2="9" width="0.127" layer="39"/>
<wire x1="-7" y1="5.4" x2="-7" y2="9" width="0.127" layer="39"/>
<wire x1="-7" y1="9" x2="7" y2="9" width="0.127" layer="39"/>
<wire x1="7" y1="9" x2="7" y2="5.4" width="0.127" layer="39"/>
<wire x1="9" y1="-1.767765625" x2="3.299459375" y2="-1.767765625" width="0.127" layer="21"/>
<wire x1="-9" y1="-1.7907" x2="-3.3147" y2="-1.7907" width="0.127" layer="21"/>
</package>
<package name="FHLOGO_PACK">
<rectangle x1="-0.00635" y1="0.00635" x2="0.84455" y2="0.01905" layer="21"/>
<rectangle x1="2.92735" y1="0.00635" x2="5.59435" y2="0.01905" layer="21"/>
<rectangle x1="6.29285" y1="0.00635" x2="7.14375" y2="0.01905" layer="21"/>
<rectangle x1="8.65505" y1="0.00635" x2="9.49325" y2="0.01905" layer="21"/>
<rectangle x1="-0.00635" y1="0.01905" x2="0.84455" y2="0.03175" layer="21"/>
<rectangle x1="2.92735" y1="0.01905" x2="5.59435" y2="0.03175" layer="21"/>
<rectangle x1="6.29285" y1="0.01905" x2="7.14375" y2="0.03175" layer="21"/>
<rectangle x1="8.65505" y1="0.01905" x2="9.49325" y2="0.03175" layer="21"/>
<rectangle x1="-0.00635" y1="0.03175" x2="0.84455" y2="0.04445" layer="21"/>
<rectangle x1="2.92735" y1="0.03175" x2="5.59435" y2="0.04445" layer="21"/>
<rectangle x1="6.29285" y1="0.03175" x2="7.14375" y2="0.04445" layer="21"/>
<rectangle x1="8.65505" y1="0.03175" x2="9.49325" y2="0.04445" layer="21"/>
<rectangle x1="-0.00635" y1="0.04445" x2="0.84455" y2="0.05715" layer="21"/>
<rectangle x1="2.92735" y1="0.04445" x2="5.59435" y2="0.05715" layer="21"/>
<rectangle x1="6.29285" y1="0.04445" x2="7.14375" y2="0.05715" layer="21"/>
<rectangle x1="8.65505" y1="0.04445" x2="9.49325" y2="0.05715" layer="21"/>
<rectangle x1="-0.00635" y1="0.05715" x2="0.84455" y2="0.06985" layer="21"/>
<rectangle x1="2.92735" y1="0.05715" x2="5.59435" y2="0.06985" layer="21"/>
<rectangle x1="6.29285" y1="0.05715" x2="7.14375" y2="0.06985" layer="21"/>
<rectangle x1="8.65505" y1="0.05715" x2="9.49325" y2="0.06985" layer="21"/>
<rectangle x1="-0.00635" y1="0.06985" x2="0.84455" y2="0.08255" layer="21"/>
<rectangle x1="2.92735" y1="0.06985" x2="5.59435" y2="0.08255" layer="21"/>
<rectangle x1="6.29285" y1="0.06985" x2="7.14375" y2="0.08255" layer="21"/>
<rectangle x1="8.65505" y1="0.06985" x2="9.49325" y2="0.08255" layer="21"/>
<rectangle x1="-0.00635" y1="0.08255" x2="0.84455" y2="0.09525" layer="21"/>
<rectangle x1="2.92735" y1="0.08255" x2="5.59435" y2="0.09525" layer="21"/>
<rectangle x1="6.29285" y1="0.08255" x2="7.14375" y2="0.09525" layer="21"/>
<rectangle x1="8.65505" y1="0.08255" x2="9.49325" y2="0.09525" layer="21"/>
<rectangle x1="-0.00635" y1="0.09525" x2="0.84455" y2="0.10795" layer="21"/>
<rectangle x1="2.92735" y1="0.09525" x2="5.59435" y2="0.10795" layer="21"/>
<rectangle x1="6.29285" y1="0.09525" x2="7.14375" y2="0.10795" layer="21"/>
<rectangle x1="8.65505" y1="0.09525" x2="9.49325" y2="0.10795" layer="21"/>
<rectangle x1="-0.00635" y1="0.10795" x2="0.84455" y2="0.12065" layer="21"/>
<rectangle x1="2.92735" y1="0.10795" x2="5.59435" y2="0.12065" layer="21"/>
<rectangle x1="6.29285" y1="0.10795" x2="7.14375" y2="0.12065" layer="21"/>
<rectangle x1="8.65505" y1="0.10795" x2="9.49325" y2="0.12065" layer="21"/>
<rectangle x1="-0.00635" y1="0.12065" x2="0.84455" y2="0.13335" layer="21"/>
<rectangle x1="2.92735" y1="0.12065" x2="5.59435" y2="0.13335" layer="21"/>
<rectangle x1="6.29285" y1="0.12065" x2="7.14375" y2="0.13335" layer="21"/>
<rectangle x1="8.65505" y1="0.12065" x2="9.49325" y2="0.13335" layer="21"/>
<rectangle x1="-0.00635" y1="0.13335" x2="0.84455" y2="0.14605" layer="21"/>
<rectangle x1="2.92735" y1="0.13335" x2="5.59435" y2="0.14605" layer="21"/>
<rectangle x1="6.29285" y1="0.13335" x2="7.14375" y2="0.14605" layer="21"/>
<rectangle x1="8.65505" y1="0.13335" x2="9.49325" y2="0.14605" layer="21"/>
<rectangle x1="-0.00635" y1="0.14605" x2="0.84455" y2="0.15875" layer="21"/>
<rectangle x1="2.92735" y1="0.14605" x2="5.59435" y2="0.15875" layer="21"/>
<rectangle x1="6.29285" y1="0.14605" x2="7.14375" y2="0.15875" layer="21"/>
<rectangle x1="8.65505" y1="0.14605" x2="9.49325" y2="0.15875" layer="21"/>
<rectangle x1="-0.00635" y1="0.15875" x2="0.84455" y2="0.17145" layer="21"/>
<rectangle x1="2.92735" y1="0.15875" x2="5.59435" y2="0.17145" layer="21"/>
<rectangle x1="6.29285" y1="0.15875" x2="7.14375" y2="0.17145" layer="21"/>
<rectangle x1="8.65505" y1="0.15875" x2="9.49325" y2="0.17145" layer="21"/>
<rectangle x1="-0.00635" y1="0.17145" x2="0.84455" y2="0.18415" layer="21"/>
<rectangle x1="2.92735" y1="0.17145" x2="5.59435" y2="0.18415" layer="21"/>
<rectangle x1="6.29285" y1="0.17145" x2="7.14375" y2="0.18415" layer="21"/>
<rectangle x1="8.65505" y1="0.17145" x2="9.49325" y2="0.18415" layer="21"/>
<rectangle x1="-0.00635" y1="0.18415" x2="0.84455" y2="0.19685" layer="21"/>
<rectangle x1="2.92735" y1="0.18415" x2="5.59435" y2="0.19685" layer="21"/>
<rectangle x1="6.29285" y1="0.18415" x2="7.14375" y2="0.19685" layer="21"/>
<rectangle x1="8.65505" y1="0.18415" x2="9.49325" y2="0.19685" layer="21"/>
<rectangle x1="-0.00635" y1="0.19685" x2="0.84455" y2="0.20955" layer="21"/>
<rectangle x1="2.92735" y1="0.19685" x2="5.59435" y2="0.20955" layer="21"/>
<rectangle x1="6.29285" y1="0.19685" x2="7.14375" y2="0.20955" layer="21"/>
<rectangle x1="8.65505" y1="0.19685" x2="9.49325" y2="0.20955" layer="21"/>
<rectangle x1="-0.00635" y1="0.20955" x2="0.84455" y2="0.22225" layer="21"/>
<rectangle x1="2.92735" y1="0.20955" x2="5.59435" y2="0.22225" layer="21"/>
<rectangle x1="6.29285" y1="0.20955" x2="7.14375" y2="0.22225" layer="21"/>
<rectangle x1="8.65505" y1="0.20955" x2="9.49325" y2="0.22225" layer="21"/>
<rectangle x1="-0.00635" y1="0.22225" x2="0.84455" y2="0.23495" layer="21"/>
<rectangle x1="2.92735" y1="0.22225" x2="5.59435" y2="0.23495" layer="21"/>
<rectangle x1="6.29285" y1="0.22225" x2="7.14375" y2="0.23495" layer="21"/>
<rectangle x1="8.65505" y1="0.22225" x2="9.49325" y2="0.23495" layer="21"/>
<rectangle x1="-0.00635" y1="0.23495" x2="0.84455" y2="0.24765" layer="21"/>
<rectangle x1="2.92735" y1="0.23495" x2="5.59435" y2="0.24765" layer="21"/>
<rectangle x1="6.29285" y1="0.23495" x2="7.14375" y2="0.24765" layer="21"/>
<rectangle x1="8.65505" y1="0.23495" x2="9.49325" y2="0.24765" layer="21"/>
<rectangle x1="-0.00635" y1="0.24765" x2="0.84455" y2="0.26035" layer="21"/>
<rectangle x1="2.92735" y1="0.24765" x2="5.59435" y2="0.26035" layer="21"/>
<rectangle x1="6.29285" y1="0.24765" x2="7.14375" y2="0.26035" layer="21"/>
<rectangle x1="8.65505" y1="0.24765" x2="9.49325" y2="0.26035" layer="21"/>
<rectangle x1="-0.00635" y1="0.26035" x2="0.84455" y2="0.27305" layer="21"/>
<rectangle x1="2.92735" y1="0.26035" x2="5.59435" y2="0.27305" layer="21"/>
<rectangle x1="6.29285" y1="0.26035" x2="7.14375" y2="0.27305" layer="21"/>
<rectangle x1="8.65505" y1="0.26035" x2="9.49325" y2="0.27305" layer="21"/>
<rectangle x1="-0.00635" y1="0.27305" x2="0.84455" y2="0.28575" layer="21"/>
<rectangle x1="2.92735" y1="0.27305" x2="5.59435" y2="0.28575" layer="21"/>
<rectangle x1="6.29285" y1="0.27305" x2="7.14375" y2="0.28575" layer="21"/>
<rectangle x1="8.65505" y1="0.27305" x2="9.49325" y2="0.28575" layer="21"/>
<rectangle x1="-0.00635" y1="0.28575" x2="0.84455" y2="0.29845" layer="21"/>
<rectangle x1="2.92735" y1="0.28575" x2="5.59435" y2="0.29845" layer="21"/>
<rectangle x1="6.29285" y1="0.28575" x2="7.14375" y2="0.29845" layer="21"/>
<rectangle x1="8.65505" y1="0.28575" x2="9.49325" y2="0.29845" layer="21"/>
<rectangle x1="-0.00635" y1="0.29845" x2="0.84455" y2="0.31115" layer="21"/>
<rectangle x1="2.92735" y1="0.29845" x2="5.59435" y2="0.31115" layer="21"/>
<rectangle x1="6.29285" y1="0.29845" x2="7.14375" y2="0.31115" layer="21"/>
<rectangle x1="8.65505" y1="0.29845" x2="9.49325" y2="0.31115" layer="21"/>
<rectangle x1="-0.00635" y1="0.31115" x2="0.84455" y2="0.32385" layer="21"/>
<rectangle x1="2.92735" y1="0.31115" x2="5.59435" y2="0.32385" layer="21"/>
<rectangle x1="6.29285" y1="0.31115" x2="7.14375" y2="0.32385" layer="21"/>
<rectangle x1="8.65505" y1="0.31115" x2="9.49325" y2="0.32385" layer="21"/>
<rectangle x1="-0.00635" y1="0.32385" x2="0.84455" y2="0.33655" layer="21"/>
<rectangle x1="2.92735" y1="0.32385" x2="5.59435" y2="0.33655" layer="21"/>
<rectangle x1="6.29285" y1="0.32385" x2="7.14375" y2="0.33655" layer="21"/>
<rectangle x1="8.65505" y1="0.32385" x2="9.49325" y2="0.33655" layer="21"/>
<rectangle x1="-0.00635" y1="0.33655" x2="0.84455" y2="0.34925" layer="21"/>
<rectangle x1="2.92735" y1="0.33655" x2="5.59435" y2="0.34925" layer="21"/>
<rectangle x1="6.29285" y1="0.33655" x2="7.14375" y2="0.34925" layer="21"/>
<rectangle x1="8.65505" y1="0.33655" x2="9.49325" y2="0.34925" layer="21"/>
<rectangle x1="-0.00635" y1="0.34925" x2="0.84455" y2="0.36195" layer="21"/>
<rectangle x1="2.92735" y1="0.34925" x2="5.59435" y2="0.36195" layer="21"/>
<rectangle x1="6.29285" y1="0.34925" x2="7.14375" y2="0.36195" layer="21"/>
<rectangle x1="8.65505" y1="0.34925" x2="9.49325" y2="0.36195" layer="21"/>
<rectangle x1="-0.00635" y1="0.36195" x2="0.84455" y2="0.37465" layer="21"/>
<rectangle x1="2.92735" y1="0.36195" x2="5.59435" y2="0.37465" layer="21"/>
<rectangle x1="6.29285" y1="0.36195" x2="7.14375" y2="0.37465" layer="21"/>
<rectangle x1="8.65505" y1="0.36195" x2="9.49325" y2="0.37465" layer="21"/>
<rectangle x1="-0.00635" y1="0.37465" x2="0.84455" y2="0.38735" layer="21"/>
<rectangle x1="2.92735" y1="0.37465" x2="5.59435" y2="0.38735" layer="21"/>
<rectangle x1="6.29285" y1="0.37465" x2="7.14375" y2="0.38735" layer="21"/>
<rectangle x1="8.65505" y1="0.37465" x2="9.49325" y2="0.38735" layer="21"/>
<rectangle x1="-0.00635" y1="0.38735" x2="0.84455" y2="0.40005" layer="21"/>
<rectangle x1="2.92735" y1="0.38735" x2="5.59435" y2="0.40005" layer="21"/>
<rectangle x1="6.29285" y1="0.38735" x2="7.14375" y2="0.40005" layer="21"/>
<rectangle x1="8.65505" y1="0.38735" x2="9.49325" y2="0.40005" layer="21"/>
<rectangle x1="-0.00635" y1="0.40005" x2="0.84455" y2="0.41275" layer="21"/>
<rectangle x1="2.92735" y1="0.40005" x2="5.59435" y2="0.41275" layer="21"/>
<rectangle x1="6.29285" y1="0.40005" x2="7.14375" y2="0.41275" layer="21"/>
<rectangle x1="8.65505" y1="0.40005" x2="9.49325" y2="0.41275" layer="21"/>
<rectangle x1="-0.00635" y1="0.41275" x2="0.84455" y2="0.42545" layer="21"/>
<rectangle x1="2.92735" y1="0.41275" x2="5.59435" y2="0.42545" layer="21"/>
<rectangle x1="6.29285" y1="0.41275" x2="7.14375" y2="0.42545" layer="21"/>
<rectangle x1="8.65505" y1="0.41275" x2="9.49325" y2="0.42545" layer="21"/>
<rectangle x1="-0.00635" y1="0.42545" x2="0.84455" y2="0.43815" layer="21"/>
<rectangle x1="2.92735" y1="0.42545" x2="5.59435" y2="0.43815" layer="21"/>
<rectangle x1="6.29285" y1="0.42545" x2="7.14375" y2="0.43815" layer="21"/>
<rectangle x1="8.65505" y1="0.42545" x2="9.49325" y2="0.43815" layer="21"/>
<rectangle x1="-0.00635" y1="0.43815" x2="0.84455" y2="0.45085" layer="21"/>
<rectangle x1="2.92735" y1="0.43815" x2="5.59435" y2="0.45085" layer="21"/>
<rectangle x1="6.29285" y1="0.43815" x2="7.14375" y2="0.45085" layer="21"/>
<rectangle x1="8.65505" y1="0.43815" x2="9.49325" y2="0.45085" layer="21"/>
<rectangle x1="-0.00635" y1="0.45085" x2="0.84455" y2="0.46355" layer="21"/>
<rectangle x1="2.92735" y1="0.45085" x2="5.59435" y2="0.46355" layer="21"/>
<rectangle x1="6.29285" y1="0.45085" x2="7.14375" y2="0.46355" layer="21"/>
<rectangle x1="8.65505" y1="0.45085" x2="9.49325" y2="0.46355" layer="21"/>
<rectangle x1="-0.00635" y1="0.46355" x2="0.84455" y2="0.47625" layer="21"/>
<rectangle x1="2.92735" y1="0.46355" x2="5.59435" y2="0.47625" layer="21"/>
<rectangle x1="6.29285" y1="0.46355" x2="7.14375" y2="0.47625" layer="21"/>
<rectangle x1="8.65505" y1="0.46355" x2="9.49325" y2="0.47625" layer="21"/>
<rectangle x1="-0.00635" y1="0.47625" x2="0.84455" y2="0.48895" layer="21"/>
<rectangle x1="2.92735" y1="0.47625" x2="5.59435" y2="0.48895" layer="21"/>
<rectangle x1="6.29285" y1="0.47625" x2="7.14375" y2="0.48895" layer="21"/>
<rectangle x1="8.65505" y1="0.47625" x2="9.49325" y2="0.48895" layer="21"/>
<rectangle x1="-0.00635" y1="0.48895" x2="0.84455" y2="0.50165" layer="21"/>
<rectangle x1="2.92735" y1="0.48895" x2="5.59435" y2="0.50165" layer="21"/>
<rectangle x1="6.29285" y1="0.48895" x2="7.14375" y2="0.50165" layer="21"/>
<rectangle x1="8.65505" y1="0.48895" x2="9.49325" y2="0.50165" layer="21"/>
<rectangle x1="-0.00635" y1="0.50165" x2="0.84455" y2="0.51435" layer="21"/>
<rectangle x1="2.92735" y1="0.50165" x2="5.59435" y2="0.51435" layer="21"/>
<rectangle x1="6.29285" y1="0.50165" x2="7.14375" y2="0.51435" layer="21"/>
<rectangle x1="8.65505" y1="0.50165" x2="9.49325" y2="0.51435" layer="21"/>
<rectangle x1="-0.00635" y1="0.51435" x2="0.84455" y2="0.52705" layer="21"/>
<rectangle x1="2.92735" y1="0.51435" x2="5.59435" y2="0.52705" layer="21"/>
<rectangle x1="6.29285" y1="0.51435" x2="7.14375" y2="0.52705" layer="21"/>
<rectangle x1="8.65505" y1="0.51435" x2="9.49325" y2="0.52705" layer="21"/>
<rectangle x1="-0.00635" y1="0.52705" x2="0.84455" y2="0.53975" layer="21"/>
<rectangle x1="2.92735" y1="0.52705" x2="5.59435" y2="0.53975" layer="21"/>
<rectangle x1="6.29285" y1="0.52705" x2="7.14375" y2="0.53975" layer="21"/>
<rectangle x1="8.65505" y1="0.52705" x2="9.49325" y2="0.53975" layer="21"/>
<rectangle x1="-0.00635" y1="0.53975" x2="0.84455" y2="0.55245" layer="21"/>
<rectangle x1="2.92735" y1="0.53975" x2="5.59435" y2="0.55245" layer="21"/>
<rectangle x1="6.29285" y1="0.53975" x2="7.14375" y2="0.55245" layer="21"/>
<rectangle x1="8.65505" y1="0.53975" x2="9.49325" y2="0.55245" layer="21"/>
<rectangle x1="-0.00635" y1="0.55245" x2="0.84455" y2="0.56515" layer="21"/>
<rectangle x1="2.92735" y1="0.55245" x2="5.59435" y2="0.56515" layer="21"/>
<rectangle x1="6.29285" y1="0.55245" x2="7.14375" y2="0.56515" layer="21"/>
<rectangle x1="8.65505" y1="0.55245" x2="9.49325" y2="0.56515" layer="21"/>
<rectangle x1="-0.00635" y1="0.56515" x2="0.84455" y2="0.57785" layer="21"/>
<rectangle x1="2.92735" y1="0.56515" x2="5.59435" y2="0.57785" layer="21"/>
<rectangle x1="6.29285" y1="0.56515" x2="7.14375" y2="0.57785" layer="21"/>
<rectangle x1="8.65505" y1="0.56515" x2="9.49325" y2="0.57785" layer="21"/>
<rectangle x1="-0.00635" y1="0.57785" x2="0.84455" y2="0.59055" layer="21"/>
<rectangle x1="2.92735" y1="0.57785" x2="5.59435" y2="0.59055" layer="21"/>
<rectangle x1="6.29285" y1="0.57785" x2="7.14375" y2="0.59055" layer="21"/>
<rectangle x1="8.65505" y1="0.57785" x2="9.49325" y2="0.59055" layer="21"/>
<rectangle x1="-0.00635" y1="0.59055" x2="0.84455" y2="0.60325" layer="21"/>
<rectangle x1="2.92735" y1="0.59055" x2="5.59435" y2="0.60325" layer="21"/>
<rectangle x1="6.29285" y1="0.59055" x2="7.14375" y2="0.60325" layer="21"/>
<rectangle x1="8.65505" y1="0.59055" x2="9.49325" y2="0.60325" layer="21"/>
<rectangle x1="-0.00635" y1="0.60325" x2="0.84455" y2="0.61595" layer="21"/>
<rectangle x1="2.92735" y1="0.60325" x2="5.59435" y2="0.61595" layer="21"/>
<rectangle x1="6.29285" y1="0.60325" x2="7.14375" y2="0.61595" layer="21"/>
<rectangle x1="8.65505" y1="0.60325" x2="9.49325" y2="0.61595" layer="21"/>
<rectangle x1="-0.00635" y1="0.61595" x2="0.84455" y2="0.62865" layer="21"/>
<rectangle x1="2.92735" y1="0.61595" x2="5.59435" y2="0.62865" layer="21"/>
<rectangle x1="6.29285" y1="0.61595" x2="7.14375" y2="0.62865" layer="21"/>
<rectangle x1="8.65505" y1="0.61595" x2="9.49325" y2="0.62865" layer="21"/>
<rectangle x1="-0.00635" y1="0.62865" x2="0.84455" y2="0.64135" layer="21"/>
<rectangle x1="2.92735" y1="0.62865" x2="5.59435" y2="0.64135" layer="21"/>
<rectangle x1="6.29285" y1="0.62865" x2="7.14375" y2="0.64135" layer="21"/>
<rectangle x1="8.65505" y1="0.62865" x2="9.49325" y2="0.64135" layer="21"/>
<rectangle x1="-0.00635" y1="0.64135" x2="0.84455" y2="0.65405" layer="21"/>
<rectangle x1="2.92735" y1="0.64135" x2="5.59435" y2="0.65405" layer="21"/>
<rectangle x1="6.29285" y1="0.64135" x2="7.14375" y2="0.65405" layer="21"/>
<rectangle x1="8.65505" y1="0.64135" x2="9.49325" y2="0.65405" layer="21"/>
<rectangle x1="-0.00635" y1="0.65405" x2="0.84455" y2="0.66675" layer="21"/>
<rectangle x1="2.92735" y1="0.65405" x2="5.59435" y2="0.66675" layer="21"/>
<rectangle x1="6.29285" y1="0.65405" x2="7.14375" y2="0.66675" layer="21"/>
<rectangle x1="8.65505" y1="0.65405" x2="9.49325" y2="0.66675" layer="21"/>
<rectangle x1="-0.00635" y1="0.66675" x2="0.84455" y2="0.67945" layer="21"/>
<rectangle x1="2.92735" y1="0.66675" x2="5.59435" y2="0.67945" layer="21"/>
<rectangle x1="6.29285" y1="0.66675" x2="7.14375" y2="0.67945" layer="21"/>
<rectangle x1="8.65505" y1="0.66675" x2="9.49325" y2="0.67945" layer="21"/>
<rectangle x1="-0.00635" y1="0.67945" x2="0.84455" y2="0.69215" layer="21"/>
<rectangle x1="2.92735" y1="0.67945" x2="5.59435" y2="0.69215" layer="21"/>
<rectangle x1="6.29285" y1="0.67945" x2="7.14375" y2="0.69215" layer="21"/>
<rectangle x1="8.65505" y1="0.67945" x2="9.49325" y2="0.69215" layer="21"/>
<rectangle x1="-0.00635" y1="0.69215" x2="0.84455" y2="0.70485" layer="21"/>
<rectangle x1="2.92735" y1="0.69215" x2="5.59435" y2="0.70485" layer="21"/>
<rectangle x1="6.29285" y1="0.69215" x2="7.14375" y2="0.70485" layer="21"/>
<rectangle x1="8.65505" y1="0.69215" x2="9.49325" y2="0.70485" layer="21"/>
<rectangle x1="-0.00635" y1="0.70485" x2="0.84455" y2="0.71755" layer="21"/>
<rectangle x1="2.92735" y1="0.70485" x2="5.59435" y2="0.71755" layer="21"/>
<rectangle x1="6.29285" y1="0.70485" x2="7.14375" y2="0.71755" layer="21"/>
<rectangle x1="8.65505" y1="0.70485" x2="9.49325" y2="0.71755" layer="21"/>
<rectangle x1="-0.00635" y1="0.71755" x2="0.84455" y2="0.73025" layer="21"/>
<rectangle x1="2.92735" y1="0.71755" x2="5.59435" y2="0.73025" layer="21"/>
<rectangle x1="6.29285" y1="0.71755" x2="7.14375" y2="0.73025" layer="21"/>
<rectangle x1="8.65505" y1="0.71755" x2="9.49325" y2="0.73025" layer="21"/>
<rectangle x1="-0.00635" y1="0.73025" x2="0.84455" y2="0.74295" layer="21"/>
<rectangle x1="2.92735" y1="0.73025" x2="5.59435" y2="0.74295" layer="21"/>
<rectangle x1="6.29285" y1="0.73025" x2="7.14375" y2="0.74295" layer="21"/>
<rectangle x1="8.65505" y1="0.73025" x2="9.49325" y2="0.74295" layer="21"/>
<rectangle x1="-0.00635" y1="0.74295" x2="0.84455" y2="0.75565" layer="21"/>
<rectangle x1="2.92735" y1="0.74295" x2="5.59435" y2="0.75565" layer="21"/>
<rectangle x1="6.29285" y1="0.74295" x2="7.14375" y2="0.75565" layer="21"/>
<rectangle x1="8.65505" y1="0.74295" x2="9.49325" y2="0.75565" layer="21"/>
<rectangle x1="-0.00635" y1="0.75565" x2="0.84455" y2="0.76835" layer="21"/>
<rectangle x1="2.92735" y1="0.75565" x2="5.59435" y2="0.76835" layer="21"/>
<rectangle x1="6.29285" y1="0.75565" x2="7.14375" y2="0.76835" layer="21"/>
<rectangle x1="8.65505" y1="0.75565" x2="9.49325" y2="0.76835" layer="21"/>
<rectangle x1="-0.00635" y1="0.76835" x2="0.84455" y2="0.78105" layer="21"/>
<rectangle x1="2.92735" y1="0.76835" x2="5.59435" y2="0.78105" layer="21"/>
<rectangle x1="6.29285" y1="0.76835" x2="7.14375" y2="0.78105" layer="21"/>
<rectangle x1="8.65505" y1="0.76835" x2="9.49325" y2="0.78105" layer="21"/>
<rectangle x1="-0.00635" y1="0.78105" x2="0.84455" y2="0.79375" layer="21"/>
<rectangle x1="2.92735" y1="0.78105" x2="5.59435" y2="0.79375" layer="21"/>
<rectangle x1="6.29285" y1="0.78105" x2="7.14375" y2="0.79375" layer="21"/>
<rectangle x1="8.65505" y1="0.78105" x2="9.49325" y2="0.79375" layer="21"/>
<rectangle x1="-0.00635" y1="0.79375" x2="0.84455" y2="0.80645" layer="21"/>
<rectangle x1="2.92735" y1="0.79375" x2="5.59435" y2="0.80645" layer="21"/>
<rectangle x1="6.29285" y1="0.79375" x2="7.14375" y2="0.80645" layer="21"/>
<rectangle x1="8.65505" y1="0.79375" x2="9.49325" y2="0.80645" layer="21"/>
<rectangle x1="-0.00635" y1="0.80645" x2="0.84455" y2="0.81915" layer="21"/>
<rectangle x1="2.92735" y1="0.80645" x2="5.59435" y2="0.81915" layer="21"/>
<rectangle x1="6.29285" y1="0.80645" x2="7.14375" y2="0.81915" layer="21"/>
<rectangle x1="8.65505" y1="0.80645" x2="9.49325" y2="0.81915" layer="21"/>
<rectangle x1="-0.00635" y1="0.81915" x2="0.84455" y2="0.83185" layer="21"/>
<rectangle x1="2.92735" y1="0.81915" x2="5.59435" y2="0.83185" layer="21"/>
<rectangle x1="6.29285" y1="0.81915" x2="7.14375" y2="0.83185" layer="21"/>
<rectangle x1="8.65505" y1="0.81915" x2="9.49325" y2="0.83185" layer="21"/>
<rectangle x1="-0.00635" y1="0.83185" x2="0.84455" y2="0.84455" layer="21"/>
<rectangle x1="2.92735" y1="0.83185" x2="5.59435" y2="0.84455" layer="21"/>
<rectangle x1="6.29285" y1="0.83185" x2="7.14375" y2="0.84455" layer="21"/>
<rectangle x1="8.65505" y1="0.83185" x2="9.49325" y2="0.84455" layer="21"/>
<rectangle x1="-0.00635" y1="0.84455" x2="0.84455" y2="0.85725" layer="21"/>
<rectangle x1="2.92735" y1="0.84455" x2="5.59435" y2="0.85725" layer="21"/>
<rectangle x1="6.29285" y1="0.84455" x2="7.14375" y2="0.85725" layer="21"/>
<rectangle x1="8.65505" y1="0.84455" x2="9.49325" y2="0.85725" layer="21"/>
<rectangle x1="-0.00635" y1="0.85725" x2="0.84455" y2="0.86995" layer="21"/>
<rectangle x1="2.92735" y1="0.85725" x2="5.59435" y2="0.86995" layer="21"/>
<rectangle x1="6.29285" y1="0.85725" x2="7.14375" y2="0.86995" layer="21"/>
<rectangle x1="8.65505" y1="0.85725" x2="9.49325" y2="0.86995" layer="21"/>
<rectangle x1="-0.00635" y1="0.86995" x2="0.84455" y2="0.88265" layer="21"/>
<rectangle x1="2.92735" y1="0.86995" x2="5.59435" y2="0.88265" layer="21"/>
<rectangle x1="6.29285" y1="0.86995" x2="7.14375" y2="0.88265" layer="21"/>
<rectangle x1="8.65505" y1="0.86995" x2="9.49325" y2="0.88265" layer="21"/>
<rectangle x1="-0.00635" y1="0.88265" x2="0.84455" y2="0.89535" layer="21"/>
<rectangle x1="2.92735" y1="0.88265" x2="5.59435" y2="0.89535" layer="21"/>
<rectangle x1="6.29285" y1="0.88265" x2="7.14375" y2="0.89535" layer="21"/>
<rectangle x1="8.65505" y1="0.88265" x2="9.49325" y2="0.89535" layer="21"/>
<rectangle x1="-0.00635" y1="0.89535" x2="0.84455" y2="0.90805" layer="21"/>
<rectangle x1="2.92735" y1="0.89535" x2="5.59435" y2="0.90805" layer="21"/>
<rectangle x1="6.29285" y1="0.89535" x2="7.14375" y2="0.90805" layer="21"/>
<rectangle x1="8.65505" y1="0.89535" x2="9.49325" y2="0.90805" layer="21"/>
<rectangle x1="-0.00635" y1="0.90805" x2="0.84455" y2="0.92075" layer="21"/>
<rectangle x1="2.92735" y1="0.90805" x2="5.59435" y2="0.92075" layer="21"/>
<rectangle x1="6.29285" y1="0.90805" x2="7.14375" y2="0.92075" layer="21"/>
<rectangle x1="8.65505" y1="0.90805" x2="9.49325" y2="0.92075" layer="21"/>
<rectangle x1="-0.00635" y1="0.92075" x2="0.84455" y2="0.93345" layer="21"/>
<rectangle x1="2.92735" y1="0.92075" x2="5.59435" y2="0.93345" layer="21"/>
<rectangle x1="6.29285" y1="0.92075" x2="7.14375" y2="0.93345" layer="21"/>
<rectangle x1="8.65505" y1="0.92075" x2="9.49325" y2="0.93345" layer="21"/>
<rectangle x1="-0.00635" y1="0.93345" x2="0.84455" y2="0.94615" layer="21"/>
<rectangle x1="2.92735" y1="0.93345" x2="5.59435" y2="0.94615" layer="21"/>
<rectangle x1="6.29285" y1="0.93345" x2="7.14375" y2="0.94615" layer="21"/>
<rectangle x1="8.65505" y1="0.93345" x2="9.49325" y2="0.94615" layer="21"/>
<rectangle x1="-0.00635" y1="0.94615" x2="0.84455" y2="0.95885" layer="21"/>
<rectangle x1="2.92735" y1="0.94615" x2="5.59435" y2="0.95885" layer="21"/>
<rectangle x1="6.29285" y1="0.94615" x2="7.14375" y2="0.95885" layer="21"/>
<rectangle x1="8.65505" y1="0.94615" x2="9.49325" y2="0.95885" layer="21"/>
<rectangle x1="-0.00635" y1="0.95885" x2="0.84455" y2="0.97155" layer="21"/>
<rectangle x1="2.92735" y1="0.95885" x2="5.59435" y2="0.97155" layer="21"/>
<rectangle x1="6.29285" y1="0.95885" x2="7.14375" y2="0.97155" layer="21"/>
<rectangle x1="8.65505" y1="0.95885" x2="9.49325" y2="0.97155" layer="21"/>
<rectangle x1="-0.00635" y1="0.97155" x2="0.84455" y2="0.98425" layer="21"/>
<rectangle x1="2.92735" y1="0.97155" x2="5.59435" y2="0.98425" layer="21"/>
<rectangle x1="6.29285" y1="0.97155" x2="7.14375" y2="0.98425" layer="21"/>
<rectangle x1="8.65505" y1="0.97155" x2="9.49325" y2="0.98425" layer="21"/>
<rectangle x1="-0.00635" y1="0.98425" x2="0.84455" y2="0.99695" layer="21"/>
<rectangle x1="2.92735" y1="0.98425" x2="5.59435" y2="0.99695" layer="21"/>
<rectangle x1="6.29285" y1="0.98425" x2="7.14375" y2="0.99695" layer="21"/>
<rectangle x1="8.65505" y1="0.98425" x2="9.49325" y2="0.99695" layer="21"/>
<rectangle x1="-0.00635" y1="0.99695" x2="0.84455" y2="1.00965" layer="21"/>
<rectangle x1="2.92735" y1="0.99695" x2="5.59435" y2="1.00965" layer="21"/>
<rectangle x1="6.29285" y1="0.99695" x2="7.14375" y2="1.00965" layer="21"/>
<rectangle x1="8.65505" y1="0.99695" x2="9.49325" y2="1.00965" layer="21"/>
<rectangle x1="-0.00635" y1="1.00965" x2="0.84455" y2="1.02235" layer="21"/>
<rectangle x1="2.92735" y1="1.00965" x2="5.59435" y2="1.02235" layer="21"/>
<rectangle x1="6.29285" y1="1.00965" x2="7.14375" y2="1.02235" layer="21"/>
<rectangle x1="8.65505" y1="1.00965" x2="9.49325" y2="1.02235" layer="21"/>
<rectangle x1="-0.00635" y1="1.02235" x2="0.84455" y2="1.03505" layer="21"/>
<rectangle x1="2.92735" y1="1.02235" x2="5.59435" y2="1.03505" layer="21"/>
<rectangle x1="6.29285" y1="1.02235" x2="7.14375" y2="1.03505" layer="21"/>
<rectangle x1="8.65505" y1="1.02235" x2="9.49325" y2="1.03505" layer="21"/>
<rectangle x1="-0.00635" y1="1.03505" x2="0.84455" y2="1.04775" layer="21"/>
<rectangle x1="2.92735" y1="1.03505" x2="5.59435" y2="1.04775" layer="21"/>
<rectangle x1="6.29285" y1="1.03505" x2="7.14375" y2="1.04775" layer="21"/>
<rectangle x1="8.65505" y1="1.03505" x2="9.49325" y2="1.04775" layer="21"/>
<rectangle x1="-0.00635" y1="1.04775" x2="0.84455" y2="1.06045" layer="21"/>
<rectangle x1="2.92735" y1="1.04775" x2="5.59435" y2="1.06045" layer="21"/>
<rectangle x1="6.29285" y1="1.04775" x2="7.14375" y2="1.06045" layer="21"/>
<rectangle x1="8.65505" y1="1.04775" x2="9.49325" y2="1.06045" layer="21"/>
<rectangle x1="-0.00635" y1="1.06045" x2="0.84455" y2="1.07315" layer="21"/>
<rectangle x1="2.92735" y1="1.06045" x2="5.59435" y2="1.07315" layer="21"/>
<rectangle x1="6.29285" y1="1.06045" x2="7.14375" y2="1.07315" layer="21"/>
<rectangle x1="8.65505" y1="1.06045" x2="9.49325" y2="1.07315" layer="21"/>
<rectangle x1="-0.00635" y1="1.07315" x2="0.84455" y2="1.08585" layer="21"/>
<rectangle x1="2.92735" y1="1.07315" x2="5.59435" y2="1.08585" layer="21"/>
<rectangle x1="6.29285" y1="1.07315" x2="7.14375" y2="1.08585" layer="21"/>
<rectangle x1="8.65505" y1="1.07315" x2="9.49325" y2="1.08585" layer="21"/>
<rectangle x1="-0.00635" y1="1.08585" x2="0.84455" y2="1.09855" layer="21"/>
<rectangle x1="2.92735" y1="1.08585" x2="5.59435" y2="1.09855" layer="21"/>
<rectangle x1="6.29285" y1="1.08585" x2="7.14375" y2="1.09855" layer="21"/>
<rectangle x1="8.65505" y1="1.08585" x2="9.49325" y2="1.09855" layer="21"/>
<rectangle x1="-0.00635" y1="1.09855" x2="0.84455" y2="1.11125" layer="21"/>
<rectangle x1="2.92735" y1="1.09855" x2="5.59435" y2="1.11125" layer="21"/>
<rectangle x1="6.29285" y1="1.09855" x2="7.14375" y2="1.11125" layer="21"/>
<rectangle x1="8.65505" y1="1.09855" x2="9.49325" y2="1.11125" layer="21"/>
<rectangle x1="-0.00635" y1="1.11125" x2="0.84455" y2="1.12395" layer="21"/>
<rectangle x1="2.92735" y1="1.11125" x2="5.59435" y2="1.12395" layer="21"/>
<rectangle x1="6.29285" y1="1.11125" x2="7.14375" y2="1.12395" layer="21"/>
<rectangle x1="8.65505" y1="1.11125" x2="9.49325" y2="1.12395" layer="21"/>
<rectangle x1="-0.00635" y1="1.12395" x2="0.84455" y2="1.13665" layer="21"/>
<rectangle x1="2.92735" y1="1.12395" x2="5.59435" y2="1.13665" layer="21"/>
<rectangle x1="6.29285" y1="1.12395" x2="7.14375" y2="1.13665" layer="21"/>
<rectangle x1="8.65505" y1="1.12395" x2="9.49325" y2="1.13665" layer="21"/>
<rectangle x1="-0.00635" y1="1.13665" x2="0.84455" y2="1.14935" layer="21"/>
<rectangle x1="2.92735" y1="1.13665" x2="5.59435" y2="1.14935" layer="21"/>
<rectangle x1="6.29285" y1="1.13665" x2="7.14375" y2="1.14935" layer="21"/>
<rectangle x1="8.65505" y1="1.13665" x2="9.49325" y2="1.14935" layer="21"/>
<rectangle x1="-0.00635" y1="1.14935" x2="0.84455" y2="1.16205" layer="21"/>
<rectangle x1="2.92735" y1="1.14935" x2="5.59435" y2="1.16205" layer="21"/>
<rectangle x1="6.29285" y1="1.14935" x2="7.14375" y2="1.16205" layer="21"/>
<rectangle x1="8.65505" y1="1.14935" x2="9.49325" y2="1.16205" layer="21"/>
<rectangle x1="-0.00635" y1="1.16205" x2="0.84455" y2="1.17475" layer="21"/>
<rectangle x1="2.92735" y1="1.16205" x2="5.59435" y2="1.17475" layer="21"/>
<rectangle x1="6.29285" y1="1.16205" x2="7.14375" y2="1.17475" layer="21"/>
<rectangle x1="8.65505" y1="1.16205" x2="9.49325" y2="1.17475" layer="21"/>
<rectangle x1="-0.00635" y1="1.17475" x2="0.84455" y2="1.18745" layer="21"/>
<rectangle x1="2.92735" y1="1.17475" x2="5.59435" y2="1.18745" layer="21"/>
<rectangle x1="6.29285" y1="1.17475" x2="7.14375" y2="1.18745" layer="21"/>
<rectangle x1="8.65505" y1="1.17475" x2="9.49325" y2="1.18745" layer="21"/>
<rectangle x1="-0.00635" y1="1.18745" x2="0.84455" y2="1.20015" layer="21"/>
<rectangle x1="2.92735" y1="1.18745" x2="5.59435" y2="1.20015" layer="21"/>
<rectangle x1="6.29285" y1="1.18745" x2="7.14375" y2="1.20015" layer="21"/>
<rectangle x1="8.65505" y1="1.18745" x2="9.49325" y2="1.20015" layer="21"/>
<rectangle x1="-0.00635" y1="1.20015" x2="0.84455" y2="1.21285" layer="21"/>
<rectangle x1="2.92735" y1="1.20015" x2="5.59435" y2="1.21285" layer="21"/>
<rectangle x1="6.29285" y1="1.20015" x2="7.14375" y2="1.21285" layer="21"/>
<rectangle x1="8.65505" y1="1.20015" x2="9.49325" y2="1.21285" layer="21"/>
<rectangle x1="-0.00635" y1="1.21285" x2="0.84455" y2="1.22555" layer="21"/>
<rectangle x1="2.92735" y1="1.21285" x2="5.59435" y2="1.22555" layer="21"/>
<rectangle x1="6.29285" y1="1.21285" x2="7.14375" y2="1.22555" layer="21"/>
<rectangle x1="8.65505" y1="1.21285" x2="9.49325" y2="1.22555" layer="21"/>
<rectangle x1="-0.00635" y1="1.22555" x2="0.84455" y2="1.23825" layer="21"/>
<rectangle x1="2.92735" y1="1.22555" x2="5.59435" y2="1.23825" layer="21"/>
<rectangle x1="6.29285" y1="1.22555" x2="7.14375" y2="1.23825" layer="21"/>
<rectangle x1="8.65505" y1="1.22555" x2="9.49325" y2="1.23825" layer="21"/>
<rectangle x1="-0.00635" y1="1.23825" x2="0.84455" y2="1.25095" layer="21"/>
<rectangle x1="2.92735" y1="1.23825" x2="5.59435" y2="1.25095" layer="21"/>
<rectangle x1="6.29285" y1="1.23825" x2="7.14375" y2="1.25095" layer="21"/>
<rectangle x1="8.65505" y1="1.23825" x2="9.49325" y2="1.25095" layer="21"/>
<rectangle x1="-0.00635" y1="1.25095" x2="0.84455" y2="1.26365" layer="21"/>
<rectangle x1="2.92735" y1="1.25095" x2="5.59435" y2="1.26365" layer="21"/>
<rectangle x1="6.29285" y1="1.25095" x2="7.14375" y2="1.26365" layer="21"/>
<rectangle x1="8.65505" y1="1.25095" x2="9.49325" y2="1.26365" layer="21"/>
<rectangle x1="-0.00635" y1="1.26365" x2="0.84455" y2="1.27635" layer="21"/>
<rectangle x1="2.92735" y1="1.26365" x2="5.59435" y2="1.27635" layer="21"/>
<rectangle x1="6.29285" y1="1.26365" x2="7.14375" y2="1.27635" layer="21"/>
<rectangle x1="8.65505" y1="1.26365" x2="9.49325" y2="1.27635" layer="21"/>
<rectangle x1="-0.00635" y1="1.27635" x2="0.84455" y2="1.28905" layer="21"/>
<rectangle x1="2.92735" y1="1.27635" x2="5.59435" y2="1.28905" layer="21"/>
<rectangle x1="6.29285" y1="1.27635" x2="7.14375" y2="1.28905" layer="21"/>
<rectangle x1="8.65505" y1="1.27635" x2="9.49325" y2="1.28905" layer="21"/>
<rectangle x1="-0.00635" y1="1.28905" x2="0.84455" y2="1.30175" layer="21"/>
<rectangle x1="2.92735" y1="1.28905" x2="5.59435" y2="1.30175" layer="21"/>
<rectangle x1="6.29285" y1="1.28905" x2="7.14375" y2="1.30175" layer="21"/>
<rectangle x1="8.65505" y1="1.28905" x2="9.49325" y2="1.30175" layer="21"/>
<rectangle x1="-0.00635" y1="1.30175" x2="0.84455" y2="1.31445" layer="21"/>
<rectangle x1="2.92735" y1="1.30175" x2="5.59435" y2="1.31445" layer="21"/>
<rectangle x1="6.29285" y1="1.30175" x2="7.14375" y2="1.31445" layer="21"/>
<rectangle x1="8.65505" y1="1.30175" x2="9.49325" y2="1.31445" layer="21"/>
<rectangle x1="-0.00635" y1="1.31445" x2="0.84455" y2="1.32715" layer="21"/>
<rectangle x1="2.92735" y1="1.31445" x2="5.59435" y2="1.32715" layer="21"/>
<rectangle x1="6.29285" y1="1.31445" x2="7.14375" y2="1.32715" layer="21"/>
<rectangle x1="8.65505" y1="1.31445" x2="9.49325" y2="1.32715" layer="21"/>
<rectangle x1="-0.00635" y1="1.32715" x2="0.84455" y2="1.33985" layer="21"/>
<rectangle x1="2.92735" y1="1.32715" x2="5.59435" y2="1.33985" layer="21"/>
<rectangle x1="6.29285" y1="1.32715" x2="7.14375" y2="1.33985" layer="21"/>
<rectangle x1="8.65505" y1="1.32715" x2="9.49325" y2="1.33985" layer="21"/>
<rectangle x1="-0.00635" y1="1.33985" x2="0.84455" y2="1.35255" layer="21"/>
<rectangle x1="2.92735" y1="1.33985" x2="5.59435" y2="1.35255" layer="21"/>
<rectangle x1="6.29285" y1="1.33985" x2="7.14375" y2="1.35255" layer="21"/>
<rectangle x1="8.65505" y1="1.33985" x2="9.49325" y2="1.35255" layer="21"/>
<rectangle x1="-0.00635" y1="1.35255" x2="0.84455" y2="1.36525" layer="21"/>
<rectangle x1="2.92735" y1="1.35255" x2="5.59435" y2="1.36525" layer="21"/>
<rectangle x1="6.29285" y1="1.35255" x2="7.14375" y2="1.36525" layer="21"/>
<rectangle x1="8.65505" y1="1.35255" x2="9.49325" y2="1.36525" layer="21"/>
<rectangle x1="-0.00635" y1="1.36525" x2="0.84455" y2="1.37795" layer="21"/>
<rectangle x1="2.92735" y1="1.36525" x2="5.59435" y2="1.37795" layer="21"/>
<rectangle x1="6.29285" y1="1.36525" x2="7.14375" y2="1.37795" layer="21"/>
<rectangle x1="8.65505" y1="1.36525" x2="9.49325" y2="1.37795" layer="21"/>
<rectangle x1="-0.00635" y1="1.37795" x2="0.84455" y2="1.39065" layer="21"/>
<rectangle x1="2.92735" y1="1.37795" x2="5.59435" y2="1.39065" layer="21"/>
<rectangle x1="6.29285" y1="1.37795" x2="7.14375" y2="1.39065" layer="21"/>
<rectangle x1="8.65505" y1="1.37795" x2="9.49325" y2="1.39065" layer="21"/>
<rectangle x1="-0.00635" y1="1.39065" x2="0.84455" y2="1.40335" layer="21"/>
<rectangle x1="2.92735" y1="1.39065" x2="5.59435" y2="1.40335" layer="21"/>
<rectangle x1="6.29285" y1="1.39065" x2="7.14375" y2="1.40335" layer="21"/>
<rectangle x1="8.65505" y1="1.39065" x2="9.49325" y2="1.40335" layer="21"/>
<rectangle x1="-0.00635" y1="1.40335" x2="0.84455" y2="1.41605" layer="21"/>
<rectangle x1="2.92735" y1="1.40335" x2="5.59435" y2="1.41605" layer="21"/>
<rectangle x1="6.29285" y1="1.40335" x2="7.14375" y2="1.41605" layer="21"/>
<rectangle x1="8.65505" y1="1.40335" x2="9.49325" y2="1.41605" layer="21"/>
<rectangle x1="-0.00635" y1="1.41605" x2="0.84455" y2="1.42875" layer="21"/>
<rectangle x1="2.92735" y1="1.41605" x2="5.59435" y2="1.42875" layer="21"/>
<rectangle x1="6.29285" y1="1.41605" x2="7.14375" y2="1.42875" layer="21"/>
<rectangle x1="8.65505" y1="1.41605" x2="9.49325" y2="1.42875" layer="21"/>
<rectangle x1="-0.00635" y1="1.42875" x2="0.84455" y2="1.44145" layer="21"/>
<rectangle x1="2.92735" y1="1.42875" x2="5.59435" y2="1.44145" layer="21"/>
<rectangle x1="6.29285" y1="1.42875" x2="7.14375" y2="1.44145" layer="21"/>
<rectangle x1="8.65505" y1="1.42875" x2="9.49325" y2="1.44145" layer="21"/>
<rectangle x1="-0.00635" y1="1.44145" x2="0.84455" y2="1.45415" layer="21"/>
<rectangle x1="2.92735" y1="1.44145" x2="5.59435" y2="1.45415" layer="21"/>
<rectangle x1="6.29285" y1="1.44145" x2="7.14375" y2="1.45415" layer="21"/>
<rectangle x1="8.65505" y1="1.44145" x2="9.49325" y2="1.45415" layer="21"/>
<rectangle x1="-0.00635" y1="1.45415" x2="0.84455" y2="1.46685" layer="21"/>
<rectangle x1="2.92735" y1="1.45415" x2="5.59435" y2="1.46685" layer="21"/>
<rectangle x1="6.29285" y1="1.45415" x2="7.14375" y2="1.46685" layer="21"/>
<rectangle x1="8.65505" y1="1.45415" x2="9.49325" y2="1.46685" layer="21"/>
<rectangle x1="-0.00635" y1="1.46685" x2="0.84455" y2="1.47955" layer="21"/>
<rectangle x1="2.92735" y1="1.46685" x2="5.59435" y2="1.47955" layer="21"/>
<rectangle x1="6.29285" y1="1.46685" x2="7.14375" y2="1.47955" layer="21"/>
<rectangle x1="8.65505" y1="1.46685" x2="9.49325" y2="1.47955" layer="21"/>
<rectangle x1="-0.00635" y1="1.47955" x2="0.84455" y2="1.49225" layer="21"/>
<rectangle x1="2.92735" y1="1.47955" x2="5.59435" y2="1.49225" layer="21"/>
<rectangle x1="6.29285" y1="1.47955" x2="7.14375" y2="1.49225" layer="21"/>
<rectangle x1="8.65505" y1="1.47955" x2="9.49325" y2="1.49225" layer="21"/>
<rectangle x1="-0.00635" y1="1.49225" x2="0.84455" y2="1.50495" layer="21"/>
<rectangle x1="2.92735" y1="1.49225" x2="5.59435" y2="1.50495" layer="21"/>
<rectangle x1="6.29285" y1="1.49225" x2="7.14375" y2="1.50495" layer="21"/>
<rectangle x1="8.65505" y1="1.49225" x2="9.49325" y2="1.50495" layer="21"/>
<rectangle x1="-0.00635" y1="1.50495" x2="0.84455" y2="1.51765" layer="21"/>
<rectangle x1="2.92735" y1="1.50495" x2="5.59435" y2="1.51765" layer="21"/>
<rectangle x1="6.29285" y1="1.50495" x2="7.14375" y2="1.51765" layer="21"/>
<rectangle x1="8.65505" y1="1.50495" x2="9.49325" y2="1.51765" layer="21"/>
<rectangle x1="-0.00635" y1="1.51765" x2="0.84455" y2="1.53035" layer="21"/>
<rectangle x1="2.92735" y1="1.51765" x2="5.59435" y2="1.53035" layer="21"/>
<rectangle x1="6.29285" y1="1.51765" x2="7.14375" y2="1.53035" layer="21"/>
<rectangle x1="8.65505" y1="1.51765" x2="9.49325" y2="1.53035" layer="21"/>
<rectangle x1="-0.00635" y1="1.53035" x2="0.84455" y2="1.54305" layer="21"/>
<rectangle x1="2.92735" y1="1.53035" x2="5.59435" y2="1.54305" layer="21"/>
<rectangle x1="6.29285" y1="1.53035" x2="7.14375" y2="1.54305" layer="21"/>
<rectangle x1="8.65505" y1="1.53035" x2="9.49325" y2="1.54305" layer="21"/>
<rectangle x1="-0.00635" y1="1.54305" x2="0.84455" y2="1.55575" layer="21"/>
<rectangle x1="2.92735" y1="1.54305" x2="5.59435" y2="1.55575" layer="21"/>
<rectangle x1="6.29285" y1="1.54305" x2="7.14375" y2="1.55575" layer="21"/>
<rectangle x1="8.65505" y1="1.54305" x2="9.49325" y2="1.55575" layer="21"/>
<rectangle x1="-0.00635" y1="1.55575" x2="0.84455" y2="1.56845" layer="21"/>
<rectangle x1="2.92735" y1="1.55575" x2="5.59435" y2="1.56845" layer="21"/>
<rectangle x1="6.29285" y1="1.55575" x2="7.14375" y2="1.56845" layer="21"/>
<rectangle x1="8.65505" y1="1.55575" x2="9.49325" y2="1.56845" layer="21"/>
<rectangle x1="-0.00635" y1="1.56845" x2="0.84455" y2="1.58115" layer="21"/>
<rectangle x1="2.92735" y1="1.56845" x2="5.59435" y2="1.58115" layer="21"/>
<rectangle x1="6.29285" y1="1.56845" x2="7.14375" y2="1.58115" layer="21"/>
<rectangle x1="8.65505" y1="1.56845" x2="9.49325" y2="1.58115" layer="21"/>
<rectangle x1="-0.00635" y1="1.58115" x2="0.84455" y2="1.59385" layer="21"/>
<rectangle x1="2.92735" y1="1.58115" x2="5.59435" y2="1.59385" layer="21"/>
<rectangle x1="6.29285" y1="1.58115" x2="7.14375" y2="1.59385" layer="21"/>
<rectangle x1="8.65505" y1="1.58115" x2="9.49325" y2="1.59385" layer="21"/>
<rectangle x1="-0.00635" y1="1.59385" x2="0.84455" y2="1.60655" layer="21"/>
<rectangle x1="2.92735" y1="1.59385" x2="5.59435" y2="1.60655" layer="21"/>
<rectangle x1="6.29285" y1="1.59385" x2="7.14375" y2="1.60655" layer="21"/>
<rectangle x1="8.65505" y1="1.59385" x2="9.49325" y2="1.60655" layer="21"/>
<rectangle x1="-0.00635" y1="1.60655" x2="0.84455" y2="1.61925" layer="21"/>
<rectangle x1="2.92735" y1="1.60655" x2="5.59435" y2="1.61925" layer="21"/>
<rectangle x1="6.29285" y1="1.60655" x2="7.14375" y2="1.61925" layer="21"/>
<rectangle x1="8.65505" y1="1.60655" x2="9.49325" y2="1.61925" layer="21"/>
<rectangle x1="-0.00635" y1="1.61925" x2="0.84455" y2="1.63195" layer="21"/>
<rectangle x1="2.92735" y1="1.61925" x2="5.59435" y2="1.63195" layer="21"/>
<rectangle x1="6.29285" y1="1.61925" x2="7.14375" y2="1.63195" layer="21"/>
<rectangle x1="8.65505" y1="1.61925" x2="9.49325" y2="1.63195" layer="21"/>
<rectangle x1="-0.00635" y1="1.63195" x2="0.84455" y2="1.64465" layer="21"/>
<rectangle x1="2.92735" y1="1.63195" x2="5.59435" y2="1.64465" layer="21"/>
<rectangle x1="6.29285" y1="1.63195" x2="7.14375" y2="1.64465" layer="21"/>
<rectangle x1="8.65505" y1="1.63195" x2="9.49325" y2="1.64465" layer="21"/>
<rectangle x1="-0.00635" y1="1.64465" x2="0.84455" y2="1.65735" layer="21"/>
<rectangle x1="2.92735" y1="1.64465" x2="5.59435" y2="1.65735" layer="21"/>
<rectangle x1="6.29285" y1="1.64465" x2="7.14375" y2="1.65735" layer="21"/>
<rectangle x1="8.65505" y1="1.64465" x2="9.49325" y2="1.65735" layer="21"/>
<rectangle x1="-0.00635" y1="1.65735" x2="0.84455" y2="1.67005" layer="21"/>
<rectangle x1="2.92735" y1="1.65735" x2="5.59435" y2="1.67005" layer="21"/>
<rectangle x1="6.29285" y1="1.65735" x2="7.14375" y2="1.67005" layer="21"/>
<rectangle x1="8.65505" y1="1.65735" x2="9.49325" y2="1.67005" layer="21"/>
<rectangle x1="-0.00635" y1="1.67005" x2="0.84455" y2="1.68275" layer="21"/>
<rectangle x1="2.92735" y1="1.67005" x2="5.59435" y2="1.68275" layer="21"/>
<rectangle x1="6.29285" y1="1.67005" x2="7.14375" y2="1.68275" layer="21"/>
<rectangle x1="8.65505" y1="1.67005" x2="9.49325" y2="1.68275" layer="21"/>
<rectangle x1="-0.00635" y1="1.68275" x2="0.84455" y2="1.69545" layer="21"/>
<rectangle x1="2.92735" y1="1.68275" x2="5.59435" y2="1.69545" layer="21"/>
<rectangle x1="6.29285" y1="1.68275" x2="7.14375" y2="1.69545" layer="21"/>
<rectangle x1="8.65505" y1="1.68275" x2="9.49325" y2="1.69545" layer="21"/>
<rectangle x1="-0.00635" y1="1.69545" x2="0.84455" y2="1.70815" layer="21"/>
<rectangle x1="2.92735" y1="1.69545" x2="5.59435" y2="1.70815" layer="21"/>
<rectangle x1="6.29285" y1="1.69545" x2="7.14375" y2="1.70815" layer="21"/>
<rectangle x1="8.65505" y1="1.69545" x2="9.49325" y2="1.70815" layer="21"/>
<rectangle x1="-0.00635" y1="1.70815" x2="0.84455" y2="1.72085" layer="21"/>
<rectangle x1="2.92735" y1="1.70815" x2="5.59435" y2="1.72085" layer="21"/>
<rectangle x1="6.29285" y1="1.70815" x2="7.14375" y2="1.72085" layer="21"/>
<rectangle x1="8.65505" y1="1.70815" x2="9.49325" y2="1.72085" layer="21"/>
<rectangle x1="-0.00635" y1="1.72085" x2="0.84455" y2="1.73355" layer="21"/>
<rectangle x1="2.92735" y1="1.72085" x2="5.59435" y2="1.73355" layer="21"/>
<rectangle x1="6.29285" y1="1.72085" x2="7.14375" y2="1.73355" layer="21"/>
<rectangle x1="8.65505" y1="1.72085" x2="9.49325" y2="1.73355" layer="21"/>
<rectangle x1="-0.00635" y1="1.73355" x2="0.84455" y2="1.74625" layer="21"/>
<rectangle x1="2.92735" y1="1.73355" x2="5.59435" y2="1.74625" layer="21"/>
<rectangle x1="6.29285" y1="1.73355" x2="7.14375" y2="1.74625" layer="21"/>
<rectangle x1="8.65505" y1="1.73355" x2="9.49325" y2="1.74625" layer="21"/>
<rectangle x1="-0.00635" y1="1.74625" x2="0.84455" y2="1.75895" layer="21"/>
<rectangle x1="2.92735" y1="1.74625" x2="5.59435" y2="1.75895" layer="21"/>
<rectangle x1="6.29285" y1="1.74625" x2="7.14375" y2="1.75895" layer="21"/>
<rectangle x1="8.65505" y1="1.74625" x2="9.49325" y2="1.75895" layer="21"/>
<rectangle x1="-0.00635" y1="1.75895" x2="0.84455" y2="1.77165" layer="21"/>
<rectangle x1="2.92735" y1="1.75895" x2="5.59435" y2="1.77165" layer="21"/>
<rectangle x1="6.29285" y1="1.75895" x2="7.14375" y2="1.77165" layer="21"/>
<rectangle x1="8.65505" y1="1.75895" x2="9.49325" y2="1.77165" layer="21"/>
<rectangle x1="-0.00635" y1="1.77165" x2="0.84455" y2="1.78435" layer="21"/>
<rectangle x1="2.92735" y1="1.77165" x2="5.59435" y2="1.78435" layer="21"/>
<rectangle x1="6.29285" y1="1.77165" x2="7.14375" y2="1.78435" layer="21"/>
<rectangle x1="8.65505" y1="1.77165" x2="9.49325" y2="1.78435" layer="21"/>
<rectangle x1="-0.00635" y1="1.78435" x2="0.84455" y2="1.79705" layer="21"/>
<rectangle x1="2.92735" y1="1.78435" x2="5.59435" y2="1.79705" layer="21"/>
<rectangle x1="6.29285" y1="1.78435" x2="7.14375" y2="1.79705" layer="21"/>
<rectangle x1="8.65505" y1="1.78435" x2="9.49325" y2="1.79705" layer="21"/>
<rectangle x1="-0.00635" y1="1.79705" x2="0.84455" y2="1.80975" layer="21"/>
<rectangle x1="2.92735" y1="1.79705" x2="5.59435" y2="1.80975" layer="21"/>
<rectangle x1="6.29285" y1="1.79705" x2="7.14375" y2="1.80975" layer="21"/>
<rectangle x1="8.65505" y1="1.79705" x2="9.49325" y2="1.80975" layer="21"/>
<rectangle x1="-0.00635" y1="1.80975" x2="0.84455" y2="1.82245" layer="21"/>
<rectangle x1="2.92735" y1="1.80975" x2="5.59435" y2="1.82245" layer="21"/>
<rectangle x1="6.29285" y1="1.80975" x2="7.14375" y2="1.82245" layer="21"/>
<rectangle x1="8.65505" y1="1.80975" x2="9.49325" y2="1.82245" layer="21"/>
<rectangle x1="-0.00635" y1="1.82245" x2="0.84455" y2="1.83515" layer="21"/>
<rectangle x1="2.92735" y1="1.82245" x2="5.59435" y2="1.83515" layer="21"/>
<rectangle x1="6.29285" y1="1.82245" x2="7.14375" y2="1.83515" layer="21"/>
<rectangle x1="8.65505" y1="1.82245" x2="9.49325" y2="1.83515" layer="21"/>
<rectangle x1="-0.00635" y1="1.83515" x2="0.84455" y2="1.84785" layer="21"/>
<rectangle x1="2.92735" y1="1.83515" x2="5.59435" y2="1.84785" layer="21"/>
<rectangle x1="6.29285" y1="1.83515" x2="7.14375" y2="1.84785" layer="21"/>
<rectangle x1="8.65505" y1="1.83515" x2="9.49325" y2="1.84785" layer="21"/>
<rectangle x1="-0.00635" y1="1.84785" x2="0.84455" y2="1.86055" layer="21"/>
<rectangle x1="2.92735" y1="1.84785" x2="5.59435" y2="1.86055" layer="21"/>
<rectangle x1="6.29285" y1="1.84785" x2="7.14375" y2="1.86055" layer="21"/>
<rectangle x1="8.65505" y1="1.84785" x2="9.49325" y2="1.86055" layer="21"/>
<rectangle x1="-0.00635" y1="1.86055" x2="0.84455" y2="1.87325" layer="21"/>
<rectangle x1="2.92735" y1="1.86055" x2="5.59435" y2="1.87325" layer="21"/>
<rectangle x1="6.29285" y1="1.86055" x2="7.14375" y2="1.87325" layer="21"/>
<rectangle x1="8.65505" y1="1.86055" x2="9.49325" y2="1.87325" layer="21"/>
<rectangle x1="-0.00635" y1="1.87325" x2="0.84455" y2="1.88595" layer="21"/>
<rectangle x1="2.92735" y1="1.87325" x2="5.59435" y2="1.88595" layer="21"/>
<rectangle x1="6.29285" y1="1.87325" x2="7.14375" y2="1.88595" layer="21"/>
<rectangle x1="8.65505" y1="1.87325" x2="9.49325" y2="1.88595" layer="21"/>
<rectangle x1="-0.00635" y1="1.88595" x2="0.84455" y2="1.89865" layer="21"/>
<rectangle x1="2.92735" y1="1.88595" x2="5.59435" y2="1.89865" layer="21"/>
<rectangle x1="6.29285" y1="1.88595" x2="7.14375" y2="1.89865" layer="21"/>
<rectangle x1="8.65505" y1="1.88595" x2="9.49325" y2="1.89865" layer="21"/>
<rectangle x1="-0.00635" y1="1.89865" x2="0.84455" y2="1.91135" layer="21"/>
<rectangle x1="2.92735" y1="1.89865" x2="5.59435" y2="1.91135" layer="21"/>
<rectangle x1="6.29285" y1="1.89865" x2="7.14375" y2="1.91135" layer="21"/>
<rectangle x1="8.65505" y1="1.89865" x2="9.49325" y2="1.91135" layer="21"/>
<rectangle x1="-0.00635" y1="1.91135" x2="0.84455" y2="1.92405" layer="21"/>
<rectangle x1="2.92735" y1="1.91135" x2="5.59435" y2="1.92405" layer="21"/>
<rectangle x1="6.29285" y1="1.91135" x2="7.14375" y2="1.92405" layer="21"/>
<rectangle x1="8.65505" y1="1.91135" x2="9.49325" y2="1.92405" layer="21"/>
<rectangle x1="-0.00635" y1="1.92405" x2="0.84455" y2="1.93675" layer="21"/>
<rectangle x1="2.92735" y1="1.92405" x2="5.59435" y2="1.93675" layer="21"/>
<rectangle x1="6.29285" y1="1.92405" x2="7.14375" y2="1.93675" layer="21"/>
<rectangle x1="8.65505" y1="1.92405" x2="9.49325" y2="1.93675" layer="21"/>
<rectangle x1="-0.00635" y1="1.93675" x2="0.84455" y2="1.94945" layer="21"/>
<rectangle x1="2.92735" y1="1.93675" x2="5.59435" y2="1.94945" layer="21"/>
<rectangle x1="6.29285" y1="1.93675" x2="7.14375" y2="1.94945" layer="21"/>
<rectangle x1="8.65505" y1="1.93675" x2="9.49325" y2="1.94945" layer="21"/>
<rectangle x1="-0.00635" y1="1.94945" x2="0.84455" y2="1.96215" layer="21"/>
<rectangle x1="2.92735" y1="1.94945" x2="5.59435" y2="1.96215" layer="21"/>
<rectangle x1="6.29285" y1="1.94945" x2="7.14375" y2="1.96215" layer="21"/>
<rectangle x1="8.65505" y1="1.94945" x2="9.49325" y2="1.96215" layer="21"/>
<rectangle x1="-0.00635" y1="1.96215" x2="0.84455" y2="1.97485" layer="21"/>
<rectangle x1="2.92735" y1="1.96215" x2="5.59435" y2="1.97485" layer="21"/>
<rectangle x1="6.29285" y1="1.96215" x2="7.14375" y2="1.97485" layer="21"/>
<rectangle x1="8.65505" y1="1.96215" x2="9.49325" y2="1.97485" layer="21"/>
<rectangle x1="-0.00635" y1="1.97485" x2="0.84455" y2="1.98755" layer="21"/>
<rectangle x1="2.92735" y1="1.97485" x2="5.59435" y2="1.98755" layer="21"/>
<rectangle x1="6.29285" y1="1.97485" x2="7.14375" y2="1.98755" layer="21"/>
<rectangle x1="8.65505" y1="1.97485" x2="9.49325" y2="1.98755" layer="21"/>
<rectangle x1="-0.00635" y1="1.98755" x2="0.84455" y2="2.00025" layer="21"/>
<rectangle x1="2.92735" y1="1.98755" x2="5.59435" y2="2.00025" layer="21"/>
<rectangle x1="6.29285" y1="1.98755" x2="7.14375" y2="2.00025" layer="21"/>
<rectangle x1="8.65505" y1="1.98755" x2="9.49325" y2="2.00025" layer="21"/>
<rectangle x1="-0.00635" y1="2.00025" x2="0.84455" y2="2.01295" layer="21"/>
<rectangle x1="2.92735" y1="2.00025" x2="5.59435" y2="2.01295" layer="21"/>
<rectangle x1="6.29285" y1="2.00025" x2="7.14375" y2="2.01295" layer="21"/>
<rectangle x1="8.65505" y1="2.00025" x2="9.49325" y2="2.01295" layer="21"/>
<rectangle x1="-0.00635" y1="2.01295" x2="0.84455" y2="2.02565" layer="21"/>
<rectangle x1="2.92735" y1="2.01295" x2="5.59435" y2="2.02565" layer="21"/>
<rectangle x1="6.29285" y1="2.01295" x2="7.14375" y2="2.02565" layer="21"/>
<rectangle x1="8.65505" y1="2.01295" x2="9.49325" y2="2.02565" layer="21"/>
<rectangle x1="-0.00635" y1="2.02565" x2="0.84455" y2="2.03835" layer="21"/>
<rectangle x1="2.92735" y1="2.02565" x2="5.59435" y2="2.03835" layer="21"/>
<rectangle x1="6.29285" y1="2.02565" x2="7.14375" y2="2.03835" layer="21"/>
<rectangle x1="8.65505" y1="2.02565" x2="9.49325" y2="2.03835" layer="21"/>
<rectangle x1="-0.00635" y1="2.03835" x2="0.84455" y2="2.05105" layer="21"/>
<rectangle x1="2.92735" y1="2.03835" x2="5.59435" y2="2.05105" layer="21"/>
<rectangle x1="6.29285" y1="2.03835" x2="7.14375" y2="2.05105" layer="21"/>
<rectangle x1="8.65505" y1="2.03835" x2="9.49325" y2="2.05105" layer="21"/>
<rectangle x1="-0.00635" y1="2.05105" x2="0.84455" y2="2.06375" layer="21"/>
<rectangle x1="2.92735" y1="2.05105" x2="5.59435" y2="2.06375" layer="21"/>
<rectangle x1="6.29285" y1="2.05105" x2="7.14375" y2="2.06375" layer="21"/>
<rectangle x1="8.65505" y1="2.05105" x2="9.49325" y2="2.06375" layer="21"/>
<rectangle x1="-0.00635" y1="2.06375" x2="0.84455" y2="2.07645" layer="21"/>
<rectangle x1="2.92735" y1="2.06375" x2="5.59435" y2="2.07645" layer="21"/>
<rectangle x1="6.29285" y1="2.06375" x2="7.14375" y2="2.07645" layer="21"/>
<rectangle x1="8.65505" y1="2.06375" x2="9.49325" y2="2.07645" layer="21"/>
<rectangle x1="-0.00635" y1="2.07645" x2="0.84455" y2="2.08915" layer="21"/>
<rectangle x1="2.92735" y1="2.07645" x2="5.59435" y2="2.08915" layer="21"/>
<rectangle x1="6.29285" y1="2.07645" x2="7.14375" y2="2.08915" layer="21"/>
<rectangle x1="8.65505" y1="2.07645" x2="9.49325" y2="2.08915" layer="21"/>
<rectangle x1="-0.00635" y1="2.08915" x2="0.84455" y2="2.10185" layer="21"/>
<rectangle x1="2.92735" y1="2.08915" x2="5.59435" y2="2.10185" layer="21"/>
<rectangle x1="6.29285" y1="2.08915" x2="7.14375" y2="2.10185" layer="21"/>
<rectangle x1="8.65505" y1="2.08915" x2="9.49325" y2="2.10185" layer="21"/>
<rectangle x1="-0.00635" y1="2.10185" x2="0.84455" y2="2.11455" layer="21"/>
<rectangle x1="2.92735" y1="2.10185" x2="5.59435" y2="2.11455" layer="21"/>
<rectangle x1="6.29285" y1="2.10185" x2="7.14375" y2="2.11455" layer="21"/>
<rectangle x1="8.65505" y1="2.10185" x2="9.49325" y2="2.11455" layer="21"/>
<rectangle x1="-0.00635" y1="2.11455" x2="0.84455" y2="2.12725" layer="21"/>
<rectangle x1="2.92735" y1="2.11455" x2="5.59435" y2="2.12725" layer="21"/>
<rectangle x1="6.29285" y1="2.11455" x2="7.14375" y2="2.12725" layer="21"/>
<rectangle x1="8.65505" y1="2.11455" x2="9.49325" y2="2.12725" layer="21"/>
<rectangle x1="-0.00635" y1="2.12725" x2="0.84455" y2="2.13995" layer="21"/>
<rectangle x1="2.92735" y1="2.12725" x2="5.59435" y2="2.13995" layer="21"/>
<rectangle x1="6.29285" y1="2.12725" x2="7.14375" y2="2.13995" layer="21"/>
<rectangle x1="8.65505" y1="2.12725" x2="9.49325" y2="2.13995" layer="21"/>
<rectangle x1="-0.00635" y1="2.13995" x2="0.84455" y2="2.15265" layer="21"/>
<rectangle x1="2.92735" y1="2.13995" x2="5.59435" y2="2.15265" layer="21"/>
<rectangle x1="6.29285" y1="2.13995" x2="7.14375" y2="2.15265" layer="21"/>
<rectangle x1="8.65505" y1="2.13995" x2="9.49325" y2="2.15265" layer="21"/>
<rectangle x1="-0.00635" y1="2.15265" x2="0.84455" y2="2.16535" layer="21"/>
<rectangle x1="2.92735" y1="2.15265" x2="5.59435" y2="2.16535" layer="21"/>
<rectangle x1="6.29285" y1="2.15265" x2="7.14375" y2="2.16535" layer="21"/>
<rectangle x1="8.65505" y1="2.15265" x2="9.49325" y2="2.16535" layer="21"/>
<rectangle x1="-0.00635" y1="2.16535" x2="0.84455" y2="2.17805" layer="21"/>
<rectangle x1="2.92735" y1="2.16535" x2="5.59435" y2="2.17805" layer="21"/>
<rectangle x1="6.29285" y1="2.16535" x2="7.14375" y2="2.17805" layer="21"/>
<rectangle x1="8.65505" y1="2.16535" x2="9.49325" y2="2.17805" layer="21"/>
<rectangle x1="-0.00635" y1="2.17805" x2="0.84455" y2="2.19075" layer="21"/>
<rectangle x1="2.92735" y1="2.17805" x2="5.59435" y2="2.19075" layer="21"/>
<rectangle x1="6.29285" y1="2.17805" x2="7.14375" y2="2.19075" layer="21"/>
<rectangle x1="8.65505" y1="2.17805" x2="9.49325" y2="2.19075" layer="21"/>
<rectangle x1="-0.00635" y1="2.19075" x2="0.84455" y2="2.20345" layer="21"/>
<rectangle x1="2.92735" y1="2.19075" x2="5.59435" y2="2.20345" layer="21"/>
<rectangle x1="6.29285" y1="2.19075" x2="7.14375" y2="2.20345" layer="21"/>
<rectangle x1="8.65505" y1="2.19075" x2="9.49325" y2="2.20345" layer="21"/>
<rectangle x1="-0.00635" y1="2.20345" x2="0.84455" y2="2.21615" layer="21"/>
<rectangle x1="2.92735" y1="2.20345" x2="5.59435" y2="2.21615" layer="21"/>
<rectangle x1="6.29285" y1="2.20345" x2="7.14375" y2="2.21615" layer="21"/>
<rectangle x1="8.65505" y1="2.20345" x2="9.49325" y2="2.21615" layer="21"/>
<rectangle x1="-0.00635" y1="2.21615" x2="0.84455" y2="2.22885" layer="21"/>
<rectangle x1="2.92735" y1="2.21615" x2="5.59435" y2="2.22885" layer="21"/>
<rectangle x1="6.29285" y1="2.21615" x2="7.14375" y2="2.22885" layer="21"/>
<rectangle x1="8.65505" y1="2.21615" x2="9.49325" y2="2.22885" layer="21"/>
<rectangle x1="-0.00635" y1="2.22885" x2="0.84455" y2="2.24155" layer="21"/>
<rectangle x1="2.92735" y1="2.22885" x2="5.59435" y2="2.24155" layer="21"/>
<rectangle x1="6.29285" y1="2.22885" x2="7.14375" y2="2.24155" layer="21"/>
<rectangle x1="8.65505" y1="2.22885" x2="9.49325" y2="2.24155" layer="21"/>
<rectangle x1="-0.00635" y1="2.24155" x2="0.84455" y2="2.25425" layer="21"/>
<rectangle x1="2.92735" y1="2.24155" x2="5.59435" y2="2.25425" layer="21"/>
<rectangle x1="6.29285" y1="2.24155" x2="7.14375" y2="2.25425" layer="21"/>
<rectangle x1="8.65505" y1="2.24155" x2="9.49325" y2="2.25425" layer="21"/>
<rectangle x1="-0.00635" y1="2.25425" x2="0.84455" y2="2.26695" layer="21"/>
<rectangle x1="2.92735" y1="2.25425" x2="5.59435" y2="2.26695" layer="21"/>
<rectangle x1="6.29285" y1="2.25425" x2="7.14375" y2="2.26695" layer="21"/>
<rectangle x1="8.65505" y1="2.25425" x2="9.49325" y2="2.26695" layer="21"/>
<rectangle x1="-0.00635" y1="2.26695" x2="0.84455" y2="2.27965" layer="21"/>
<rectangle x1="2.92735" y1="2.26695" x2="5.59435" y2="2.27965" layer="21"/>
<rectangle x1="6.29285" y1="2.26695" x2="7.14375" y2="2.27965" layer="21"/>
<rectangle x1="8.65505" y1="2.26695" x2="9.49325" y2="2.27965" layer="21"/>
<rectangle x1="-0.00635" y1="2.27965" x2="0.84455" y2="2.29235" layer="21"/>
<rectangle x1="2.92735" y1="2.27965" x2="5.59435" y2="2.29235" layer="21"/>
<rectangle x1="6.29285" y1="2.27965" x2="7.14375" y2="2.29235" layer="21"/>
<rectangle x1="8.65505" y1="2.27965" x2="9.49325" y2="2.29235" layer="21"/>
<rectangle x1="-0.00635" y1="2.29235" x2="0.84455" y2="2.30505" layer="21"/>
<rectangle x1="2.92735" y1="2.29235" x2="5.59435" y2="2.30505" layer="21"/>
<rectangle x1="6.29285" y1="2.29235" x2="7.14375" y2="2.30505" layer="21"/>
<rectangle x1="8.65505" y1="2.29235" x2="9.49325" y2="2.30505" layer="21"/>
<rectangle x1="-0.00635" y1="2.30505" x2="0.84455" y2="2.31775" layer="21"/>
<rectangle x1="2.92735" y1="2.30505" x2="5.59435" y2="2.31775" layer="21"/>
<rectangle x1="6.29285" y1="2.30505" x2="7.14375" y2="2.31775" layer="21"/>
<rectangle x1="8.65505" y1="2.30505" x2="9.49325" y2="2.31775" layer="21"/>
<rectangle x1="-0.00635" y1="2.31775" x2="0.84455" y2="2.33045" layer="21"/>
<rectangle x1="2.92735" y1="2.31775" x2="5.59435" y2="2.33045" layer="21"/>
<rectangle x1="6.29285" y1="2.31775" x2="7.14375" y2="2.33045" layer="21"/>
<rectangle x1="8.65505" y1="2.31775" x2="9.49325" y2="2.33045" layer="21"/>
<rectangle x1="-0.00635" y1="2.33045" x2="0.84455" y2="2.34315" layer="21"/>
<rectangle x1="2.92735" y1="2.33045" x2="5.59435" y2="2.34315" layer="21"/>
<rectangle x1="6.29285" y1="2.33045" x2="7.14375" y2="2.34315" layer="21"/>
<rectangle x1="8.65505" y1="2.33045" x2="9.49325" y2="2.34315" layer="21"/>
<rectangle x1="-0.00635" y1="2.34315" x2="0.84455" y2="2.35585" layer="21"/>
<rectangle x1="2.92735" y1="2.34315" x2="5.59435" y2="2.35585" layer="21"/>
<rectangle x1="6.29285" y1="2.34315" x2="7.14375" y2="2.35585" layer="21"/>
<rectangle x1="8.65505" y1="2.34315" x2="9.49325" y2="2.35585" layer="21"/>
<rectangle x1="-0.00635" y1="2.35585" x2="0.84455" y2="2.36855" layer="21"/>
<rectangle x1="2.92735" y1="2.35585" x2="5.59435" y2="2.36855" layer="21"/>
<rectangle x1="6.29285" y1="2.35585" x2="7.14375" y2="2.36855" layer="21"/>
<rectangle x1="8.65505" y1="2.35585" x2="9.49325" y2="2.36855" layer="21"/>
<rectangle x1="-0.00635" y1="2.36855" x2="0.84455" y2="2.38125" layer="21"/>
<rectangle x1="2.92735" y1="2.36855" x2="5.59435" y2="2.38125" layer="21"/>
<rectangle x1="6.29285" y1="2.36855" x2="7.14375" y2="2.38125" layer="21"/>
<rectangle x1="8.65505" y1="2.36855" x2="9.49325" y2="2.38125" layer="21"/>
<rectangle x1="-0.00635" y1="2.38125" x2="0.84455" y2="2.39395" layer="21"/>
<rectangle x1="2.92735" y1="2.38125" x2="5.59435" y2="2.39395" layer="21"/>
<rectangle x1="6.29285" y1="2.38125" x2="7.14375" y2="2.39395" layer="21"/>
<rectangle x1="8.65505" y1="2.38125" x2="9.49325" y2="2.39395" layer="21"/>
<rectangle x1="-0.00635" y1="2.39395" x2="0.84455" y2="2.40665" layer="21"/>
<rectangle x1="2.92735" y1="2.39395" x2="5.59435" y2="2.40665" layer="21"/>
<rectangle x1="6.29285" y1="2.39395" x2="7.14375" y2="2.40665" layer="21"/>
<rectangle x1="8.65505" y1="2.39395" x2="9.49325" y2="2.40665" layer="21"/>
<rectangle x1="-0.00635" y1="2.40665" x2="0.84455" y2="2.41935" layer="21"/>
<rectangle x1="2.92735" y1="2.40665" x2="5.59435" y2="2.41935" layer="21"/>
<rectangle x1="6.29285" y1="2.40665" x2="7.14375" y2="2.41935" layer="21"/>
<rectangle x1="8.65505" y1="2.40665" x2="9.49325" y2="2.41935" layer="21"/>
<rectangle x1="-0.00635" y1="2.41935" x2="0.84455" y2="2.43205" layer="21"/>
<rectangle x1="2.92735" y1="2.41935" x2="5.59435" y2="2.43205" layer="21"/>
<rectangle x1="6.29285" y1="2.41935" x2="7.14375" y2="2.43205" layer="21"/>
<rectangle x1="8.65505" y1="2.41935" x2="9.49325" y2="2.43205" layer="21"/>
<rectangle x1="-0.00635" y1="2.43205" x2="0.84455" y2="2.44475" layer="21"/>
<rectangle x1="2.92735" y1="2.43205" x2="5.59435" y2="2.44475" layer="21"/>
<rectangle x1="6.29285" y1="2.43205" x2="7.14375" y2="2.44475" layer="21"/>
<rectangle x1="8.65505" y1="2.43205" x2="9.49325" y2="2.44475" layer="21"/>
<rectangle x1="-0.00635" y1="2.44475" x2="0.84455" y2="2.45745" layer="21"/>
<rectangle x1="2.92735" y1="2.44475" x2="5.59435" y2="2.45745" layer="21"/>
<rectangle x1="6.29285" y1="2.44475" x2="7.14375" y2="2.45745" layer="21"/>
<rectangle x1="8.65505" y1="2.44475" x2="9.49325" y2="2.45745" layer="21"/>
<rectangle x1="-0.00635" y1="2.45745" x2="0.84455" y2="2.47015" layer="21"/>
<rectangle x1="2.92735" y1="2.45745" x2="5.59435" y2="2.47015" layer="21"/>
<rectangle x1="6.29285" y1="2.45745" x2="7.14375" y2="2.47015" layer="21"/>
<rectangle x1="8.65505" y1="2.45745" x2="9.49325" y2="2.47015" layer="21"/>
<rectangle x1="-0.00635" y1="2.47015" x2="0.84455" y2="2.48285" layer="21"/>
<rectangle x1="2.92735" y1="2.47015" x2="5.59435" y2="2.48285" layer="21"/>
<rectangle x1="6.29285" y1="2.47015" x2="7.14375" y2="2.48285" layer="21"/>
<rectangle x1="8.65505" y1="2.47015" x2="9.49325" y2="2.48285" layer="21"/>
<rectangle x1="-0.00635" y1="2.48285" x2="0.84455" y2="2.49555" layer="21"/>
<rectangle x1="2.92735" y1="2.48285" x2="5.59435" y2="2.49555" layer="21"/>
<rectangle x1="6.29285" y1="2.48285" x2="7.14375" y2="2.49555" layer="21"/>
<rectangle x1="8.65505" y1="2.48285" x2="9.49325" y2="2.49555" layer="21"/>
<rectangle x1="-0.00635" y1="2.49555" x2="0.84455" y2="2.50825" layer="21"/>
<rectangle x1="2.92735" y1="2.49555" x2="5.59435" y2="2.50825" layer="21"/>
<rectangle x1="6.29285" y1="2.49555" x2="7.14375" y2="2.50825" layer="21"/>
<rectangle x1="8.65505" y1="2.49555" x2="9.49325" y2="2.50825" layer="21"/>
<rectangle x1="-0.00635" y1="2.50825" x2="0.84455" y2="2.52095" layer="21"/>
<rectangle x1="2.92735" y1="2.50825" x2="5.59435" y2="2.52095" layer="21"/>
<rectangle x1="6.29285" y1="2.50825" x2="7.14375" y2="2.52095" layer="21"/>
<rectangle x1="8.65505" y1="2.50825" x2="9.49325" y2="2.52095" layer="21"/>
<rectangle x1="-0.00635" y1="2.52095" x2="0.84455" y2="2.53365" layer="21"/>
<rectangle x1="2.92735" y1="2.52095" x2="5.59435" y2="2.53365" layer="21"/>
<rectangle x1="6.29285" y1="2.52095" x2="7.14375" y2="2.53365" layer="21"/>
<rectangle x1="8.65505" y1="2.52095" x2="9.49325" y2="2.53365" layer="21"/>
<rectangle x1="-0.00635" y1="2.53365" x2="0.84455" y2="2.54635" layer="21"/>
<rectangle x1="2.92735" y1="2.53365" x2="5.59435" y2="2.54635" layer="21"/>
<rectangle x1="6.29285" y1="2.53365" x2="7.14375" y2="2.54635" layer="21"/>
<rectangle x1="8.65505" y1="2.53365" x2="9.49325" y2="2.54635" layer="21"/>
<rectangle x1="-0.00635" y1="2.54635" x2="0.84455" y2="2.55905" layer="21"/>
<rectangle x1="2.92735" y1="2.54635" x2="5.59435" y2="2.55905" layer="21"/>
<rectangle x1="6.29285" y1="2.54635" x2="7.14375" y2="2.55905" layer="21"/>
<rectangle x1="8.65505" y1="2.54635" x2="9.49325" y2="2.55905" layer="21"/>
<rectangle x1="-0.00635" y1="2.55905" x2="0.84455" y2="2.57175" layer="21"/>
<rectangle x1="2.92735" y1="2.55905" x2="5.59435" y2="2.57175" layer="21"/>
<rectangle x1="6.29285" y1="2.55905" x2="7.14375" y2="2.57175" layer="21"/>
<rectangle x1="8.65505" y1="2.55905" x2="9.49325" y2="2.57175" layer="21"/>
<rectangle x1="-0.00635" y1="2.57175" x2="0.84455" y2="2.58445" layer="21"/>
<rectangle x1="2.92735" y1="2.57175" x2="5.59435" y2="2.58445" layer="21"/>
<rectangle x1="6.29285" y1="2.57175" x2="7.14375" y2="2.58445" layer="21"/>
<rectangle x1="8.65505" y1="2.57175" x2="9.49325" y2="2.58445" layer="21"/>
<rectangle x1="-0.00635" y1="2.58445" x2="0.84455" y2="2.59715" layer="21"/>
<rectangle x1="2.92735" y1="2.58445" x2="5.59435" y2="2.59715" layer="21"/>
<rectangle x1="6.29285" y1="2.58445" x2="7.14375" y2="2.59715" layer="21"/>
<rectangle x1="8.65505" y1="2.58445" x2="9.49325" y2="2.59715" layer="21"/>
<rectangle x1="-0.00635" y1="2.59715" x2="0.84455" y2="2.60985" layer="21"/>
<rectangle x1="2.92735" y1="2.59715" x2="5.59435" y2="2.60985" layer="21"/>
<rectangle x1="6.29285" y1="2.59715" x2="7.14375" y2="2.60985" layer="21"/>
<rectangle x1="8.65505" y1="2.59715" x2="9.49325" y2="2.60985" layer="21"/>
<rectangle x1="-0.00635" y1="2.60985" x2="0.84455" y2="2.62255" layer="21"/>
<rectangle x1="2.92735" y1="2.60985" x2="5.59435" y2="2.62255" layer="21"/>
<rectangle x1="6.29285" y1="2.60985" x2="7.14375" y2="2.62255" layer="21"/>
<rectangle x1="8.65505" y1="2.60985" x2="9.49325" y2="2.62255" layer="21"/>
<rectangle x1="-0.00635" y1="2.62255" x2="0.84455" y2="2.63525" layer="21"/>
<rectangle x1="2.92735" y1="2.62255" x2="5.59435" y2="2.63525" layer="21"/>
<rectangle x1="6.29285" y1="2.62255" x2="7.14375" y2="2.63525" layer="21"/>
<rectangle x1="8.65505" y1="2.62255" x2="9.49325" y2="2.63525" layer="21"/>
<rectangle x1="-0.00635" y1="2.63525" x2="0.84455" y2="2.64795" layer="21"/>
<rectangle x1="2.92735" y1="2.63525" x2="5.59435" y2="2.64795" layer="21"/>
<rectangle x1="6.29285" y1="2.63525" x2="7.14375" y2="2.64795" layer="21"/>
<rectangle x1="8.65505" y1="2.63525" x2="9.49325" y2="2.64795" layer="21"/>
<rectangle x1="-0.00635" y1="2.64795" x2="0.84455" y2="2.66065" layer="21"/>
<rectangle x1="2.92735" y1="2.64795" x2="5.59435" y2="2.66065" layer="21"/>
<rectangle x1="6.29285" y1="2.64795" x2="7.14375" y2="2.66065" layer="21"/>
<rectangle x1="8.65505" y1="2.64795" x2="9.49325" y2="2.66065" layer="21"/>
<rectangle x1="-0.00635" y1="2.66065" x2="0.84455" y2="2.67335" layer="21"/>
<rectangle x1="2.92735" y1="2.66065" x2="5.59435" y2="2.67335" layer="21"/>
<rectangle x1="6.29285" y1="2.66065" x2="7.14375" y2="2.67335" layer="21"/>
<rectangle x1="8.65505" y1="2.66065" x2="9.49325" y2="2.67335" layer="21"/>
<rectangle x1="-0.00635" y1="2.67335" x2="0.84455" y2="2.68605" layer="21"/>
<rectangle x1="2.92735" y1="2.67335" x2="5.59435" y2="2.68605" layer="21"/>
<rectangle x1="6.29285" y1="2.67335" x2="7.14375" y2="2.68605" layer="21"/>
<rectangle x1="8.65505" y1="2.67335" x2="9.49325" y2="2.68605" layer="21"/>
<rectangle x1="-0.00635" y1="2.68605" x2="0.84455" y2="2.69875" layer="21"/>
<rectangle x1="2.92735" y1="2.68605" x2="4.94665" y2="2.69875" layer="21"/>
<rectangle x1="5.18795" y1="2.68605" x2="5.59435" y2="2.69875" layer="21"/>
<rectangle x1="6.29285" y1="2.68605" x2="7.14375" y2="2.69875" layer="21"/>
<rectangle x1="8.65505" y1="2.68605" x2="9.49325" y2="2.69875" layer="21"/>
<rectangle x1="-0.00635" y1="2.69875" x2="2.25425" y2="2.71145" layer="21"/>
<rectangle x1="2.92735" y1="2.69875" x2="4.85775" y2="2.71145" layer="21"/>
<rectangle x1="5.27685" y1="2.69875" x2="5.59435" y2="2.71145" layer="21"/>
<rectangle x1="6.29285" y1="2.69875" x2="9.49325" y2="2.71145" layer="21"/>
<rectangle x1="-0.00635" y1="2.71145" x2="2.25425" y2="2.72415" layer="21"/>
<rectangle x1="2.92735" y1="2.71145" x2="4.79425" y2="2.72415" layer="21"/>
<rectangle x1="5.32765" y1="2.71145" x2="5.59435" y2="2.72415" layer="21"/>
<rectangle x1="6.29285" y1="2.71145" x2="9.49325" y2="2.72415" layer="21"/>
<rectangle x1="-0.00635" y1="2.72415" x2="2.25425" y2="2.73685" layer="21"/>
<rectangle x1="2.92735" y1="2.72415" x2="4.74345" y2="2.73685" layer="21"/>
<rectangle x1="5.37845" y1="2.72415" x2="5.59435" y2="2.73685" layer="21"/>
<rectangle x1="6.29285" y1="2.72415" x2="9.49325" y2="2.73685" layer="21"/>
<rectangle x1="-0.00635" y1="2.73685" x2="2.25425" y2="2.74955" layer="21"/>
<rectangle x1="3.18135" y1="2.73685" x2="4.10845" y2="2.74955" layer="21"/>
<rectangle x1="4.65455" y1="2.73685" x2="4.70535" y2="2.74955" layer="21"/>
<rectangle x1="5.41655" y1="2.73685" x2="5.59435" y2="2.74955" layer="21"/>
<rectangle x1="6.29285" y1="2.73685" x2="9.49325" y2="2.74955" layer="21"/>
<rectangle x1="-0.00635" y1="2.74955" x2="2.25425" y2="2.76225" layer="21"/>
<rectangle x1="3.19405" y1="2.74955" x2="4.09575" y2="2.76225" layer="21"/>
<rectangle x1="4.65455" y1="2.74955" x2="4.66725" y2="2.76225" layer="21"/>
<rectangle x1="5.44195" y1="2.74955" x2="5.59435" y2="2.76225" layer="21"/>
<rectangle x1="6.29285" y1="2.74955" x2="9.49325" y2="2.76225" layer="21"/>
<rectangle x1="-0.00635" y1="2.76225" x2="2.25425" y2="2.77495" layer="21"/>
<rectangle x1="3.19405" y1="2.76225" x2="4.09575" y2="2.77495" layer="21"/>
<rectangle x1="5.46735" y1="2.76225" x2="5.59435" y2="2.77495" layer="21"/>
<rectangle x1="6.29285" y1="2.76225" x2="9.49325" y2="2.77495" layer="21"/>
<rectangle x1="-0.00635" y1="2.77495" x2="2.25425" y2="2.78765" layer="21"/>
<rectangle x1="3.19405" y1="2.77495" x2="4.09575" y2="2.78765" layer="21"/>
<rectangle x1="5.50545" y1="2.77495" x2="5.59435" y2="2.78765" layer="21"/>
<rectangle x1="6.29285" y1="2.77495" x2="9.49325" y2="2.78765" layer="21"/>
<rectangle x1="-0.00635" y1="2.78765" x2="2.25425" y2="2.80035" layer="21"/>
<rectangle x1="3.19405" y1="2.78765" x2="4.09575" y2="2.80035" layer="21"/>
<rectangle x1="5.51815" y1="2.78765" x2="5.59435" y2="2.80035" layer="21"/>
<rectangle x1="6.29285" y1="2.78765" x2="9.49325" y2="2.80035" layer="21"/>
<rectangle x1="-0.00635" y1="2.80035" x2="2.25425" y2="2.81305" layer="21"/>
<rectangle x1="3.20675" y1="2.80035" x2="4.09575" y2="2.81305" layer="21"/>
<rectangle x1="5.54355" y1="2.80035" x2="5.59435" y2="2.81305" layer="21"/>
<rectangle x1="6.29285" y1="2.80035" x2="9.49325" y2="2.81305" layer="21"/>
<rectangle x1="-0.00635" y1="2.81305" x2="2.25425" y2="2.82575" layer="21"/>
<rectangle x1="3.20675" y1="2.81305" x2="4.08305" y2="2.82575" layer="21"/>
<rectangle x1="5.55625" y1="2.81305" x2="5.59435" y2="2.82575" layer="21"/>
<rectangle x1="6.29285" y1="2.81305" x2="9.49325" y2="2.82575" layer="21"/>
<rectangle x1="-0.00635" y1="2.82575" x2="2.25425" y2="2.83845" layer="21"/>
<rectangle x1="3.20675" y1="2.82575" x2="4.08305" y2="2.83845" layer="21"/>
<rectangle x1="5.56895" y1="2.82575" x2="5.59435" y2="2.83845" layer="21"/>
<rectangle x1="6.29285" y1="2.82575" x2="9.49325" y2="2.83845" layer="21"/>
<rectangle x1="-0.00635" y1="2.83845" x2="2.25425" y2="2.85115" layer="21"/>
<rectangle x1="3.20675" y1="2.83845" x2="4.08305" y2="2.85115" layer="21"/>
<rectangle x1="6.29285" y1="2.83845" x2="9.49325" y2="2.85115" layer="21"/>
<rectangle x1="-0.00635" y1="2.85115" x2="2.25425" y2="2.86385" layer="21"/>
<rectangle x1="3.20675" y1="2.85115" x2="4.08305" y2="2.86385" layer="21"/>
<rectangle x1="6.29285" y1="2.85115" x2="9.49325" y2="2.86385" layer="21"/>
<rectangle x1="-0.00635" y1="2.86385" x2="2.25425" y2="2.87655" layer="21"/>
<rectangle x1="3.21945" y1="2.86385" x2="4.08305" y2="2.87655" layer="21"/>
<rectangle x1="6.29285" y1="2.86385" x2="9.49325" y2="2.87655" layer="21"/>
<rectangle x1="-0.00635" y1="2.87655" x2="2.25425" y2="2.88925" layer="21"/>
<rectangle x1="3.21945" y1="2.87655" x2="4.07035" y2="2.88925" layer="21"/>
<rectangle x1="6.29285" y1="2.87655" x2="9.49325" y2="2.88925" layer="21"/>
<rectangle x1="-0.00635" y1="2.88925" x2="2.25425" y2="2.90195" layer="21"/>
<rectangle x1="3.21945" y1="2.88925" x2="4.07035" y2="2.90195" layer="21"/>
<rectangle x1="6.29285" y1="2.88925" x2="9.49325" y2="2.90195" layer="21"/>
<rectangle x1="-0.00635" y1="2.90195" x2="2.25425" y2="2.91465" layer="21"/>
<rectangle x1="3.21945" y1="2.90195" x2="4.07035" y2="2.91465" layer="21"/>
<rectangle x1="6.29285" y1="2.90195" x2="9.49325" y2="2.91465" layer="21"/>
<rectangle x1="-0.00635" y1="2.91465" x2="2.25425" y2="2.92735" layer="21"/>
<rectangle x1="3.21945" y1="2.91465" x2="4.07035" y2="2.92735" layer="21"/>
<rectangle x1="6.29285" y1="2.91465" x2="9.49325" y2="2.92735" layer="21"/>
<rectangle x1="-0.00635" y1="2.92735" x2="2.25425" y2="2.94005" layer="21"/>
<rectangle x1="3.23215" y1="2.92735" x2="4.07035" y2="2.94005" layer="21"/>
<rectangle x1="6.29285" y1="2.92735" x2="9.49325" y2="2.94005" layer="21"/>
<rectangle x1="-0.00635" y1="2.94005" x2="2.25425" y2="2.95275" layer="21"/>
<rectangle x1="3.23215" y1="2.94005" x2="4.05765" y2="2.95275" layer="21"/>
<rectangle x1="6.29285" y1="2.94005" x2="9.49325" y2="2.95275" layer="21"/>
<rectangle x1="-0.00635" y1="2.95275" x2="2.25425" y2="2.96545" layer="21"/>
<rectangle x1="3.23215" y1="2.95275" x2="4.05765" y2="2.96545" layer="21"/>
<rectangle x1="6.29285" y1="2.95275" x2="9.49325" y2="2.96545" layer="21"/>
<rectangle x1="-0.00635" y1="2.96545" x2="2.25425" y2="2.97815" layer="21"/>
<rectangle x1="3.23215" y1="2.96545" x2="4.05765" y2="2.97815" layer="21"/>
<rectangle x1="6.29285" y1="2.96545" x2="9.49325" y2="2.97815" layer="21"/>
<rectangle x1="-0.00635" y1="2.97815" x2="2.25425" y2="2.99085" layer="21"/>
<rectangle x1="3.23215" y1="2.97815" x2="4.05765" y2="2.99085" layer="21"/>
<rectangle x1="6.29285" y1="2.97815" x2="9.49325" y2="2.99085" layer="21"/>
<rectangle x1="-0.00635" y1="2.99085" x2="2.25425" y2="3.00355" layer="21"/>
<rectangle x1="3.24485" y1="2.99085" x2="4.05765" y2="3.00355" layer="21"/>
<rectangle x1="6.29285" y1="2.99085" x2="9.49325" y2="3.00355" layer="21"/>
<rectangle x1="-0.00635" y1="3.00355" x2="2.25425" y2="3.01625" layer="21"/>
<rectangle x1="3.24485" y1="3.00355" x2="4.04495" y2="3.01625" layer="21"/>
<rectangle x1="6.29285" y1="3.00355" x2="9.49325" y2="3.01625" layer="21"/>
<rectangle x1="-0.00635" y1="3.01625" x2="2.25425" y2="3.02895" layer="21"/>
<rectangle x1="3.24485" y1="3.01625" x2="4.04495" y2="3.02895" layer="21"/>
<rectangle x1="6.29285" y1="3.01625" x2="9.49325" y2="3.02895" layer="21"/>
<rectangle x1="-0.00635" y1="3.02895" x2="2.25425" y2="3.04165" layer="21"/>
<rectangle x1="3.24485" y1="3.02895" x2="4.04495" y2="3.04165" layer="21"/>
<rectangle x1="6.29285" y1="3.02895" x2="9.49325" y2="3.04165" layer="21"/>
<rectangle x1="-0.00635" y1="3.04165" x2="2.25425" y2="3.05435" layer="21"/>
<rectangle x1="3.24485" y1="3.04165" x2="4.04495" y2="3.05435" layer="21"/>
<rectangle x1="6.29285" y1="3.04165" x2="9.49325" y2="3.05435" layer="21"/>
<rectangle x1="-0.00635" y1="3.05435" x2="2.25425" y2="3.06705" layer="21"/>
<rectangle x1="3.25755" y1="3.05435" x2="4.03225" y2="3.06705" layer="21"/>
<rectangle x1="6.29285" y1="3.05435" x2="9.49325" y2="3.06705" layer="21"/>
<rectangle x1="-0.00635" y1="3.06705" x2="2.25425" y2="3.07975" layer="21"/>
<rectangle x1="3.25755" y1="3.06705" x2="4.03225" y2="3.07975" layer="21"/>
<rectangle x1="6.29285" y1="3.06705" x2="9.49325" y2="3.07975" layer="21"/>
<rectangle x1="-0.00635" y1="3.07975" x2="2.25425" y2="3.09245" layer="21"/>
<rectangle x1="3.25755" y1="3.07975" x2="4.03225" y2="3.09245" layer="21"/>
<rectangle x1="6.29285" y1="3.07975" x2="9.49325" y2="3.09245" layer="21"/>
<rectangle x1="-0.00635" y1="3.09245" x2="2.25425" y2="3.10515" layer="21"/>
<rectangle x1="3.25755" y1="3.09245" x2="4.03225" y2="3.10515" layer="21"/>
<rectangle x1="6.29285" y1="3.09245" x2="9.49325" y2="3.10515" layer="21"/>
<rectangle x1="-0.00635" y1="3.10515" x2="2.25425" y2="3.11785" layer="21"/>
<rectangle x1="3.27025" y1="3.10515" x2="4.03225" y2="3.11785" layer="21"/>
<rectangle x1="4.94665" y1="3.10515" x2="5.12445" y2="3.11785" layer="21"/>
<rectangle x1="6.29285" y1="3.10515" x2="9.49325" y2="3.11785" layer="21"/>
<rectangle x1="-0.00635" y1="3.11785" x2="2.25425" y2="3.13055" layer="21"/>
<rectangle x1="3.27025" y1="3.11785" x2="4.01955" y2="3.13055" layer="21"/>
<rectangle x1="4.92125" y1="3.11785" x2="5.14985" y2="3.13055" layer="21"/>
<rectangle x1="6.29285" y1="3.11785" x2="9.49325" y2="3.13055" layer="21"/>
<rectangle x1="-0.00635" y1="3.13055" x2="2.25425" y2="3.14325" layer="21"/>
<rectangle x1="3.27025" y1="3.13055" x2="4.01955" y2="3.14325" layer="21"/>
<rectangle x1="4.89585" y1="3.13055" x2="5.17525" y2="3.14325" layer="21"/>
<rectangle x1="6.29285" y1="3.13055" x2="9.49325" y2="3.14325" layer="21"/>
<rectangle x1="-0.00635" y1="3.14325" x2="2.25425" y2="3.15595" layer="21"/>
<rectangle x1="3.27025" y1="3.14325" x2="4.01955" y2="3.15595" layer="21"/>
<rectangle x1="4.88315" y1="3.14325" x2="5.20065" y2="3.15595" layer="21"/>
<rectangle x1="6.29285" y1="3.14325" x2="9.49325" y2="3.15595" layer="21"/>
<rectangle x1="-0.00635" y1="3.15595" x2="2.25425" y2="3.16865" layer="21"/>
<rectangle x1="3.27025" y1="3.15595" x2="4.01955" y2="3.16865" layer="21"/>
<rectangle x1="4.85775" y1="3.15595" x2="5.21335" y2="3.16865" layer="21"/>
<rectangle x1="6.29285" y1="3.15595" x2="9.49325" y2="3.16865" layer="21"/>
<rectangle x1="-0.00635" y1="3.16865" x2="2.25425" y2="3.18135" layer="21"/>
<rectangle x1="3.28295" y1="3.16865" x2="4.01955" y2="3.18135" layer="21"/>
<rectangle x1="4.85775" y1="3.16865" x2="5.22605" y2="3.18135" layer="21"/>
<rectangle x1="6.29285" y1="3.16865" x2="9.49325" y2="3.18135" layer="21"/>
<rectangle x1="-0.00635" y1="3.18135" x2="2.25425" y2="3.19405" layer="21"/>
<rectangle x1="3.28295" y1="3.18135" x2="4.00685" y2="3.19405" layer="21"/>
<rectangle x1="4.84505" y1="3.18135" x2="5.23875" y2="3.19405" layer="21"/>
<rectangle x1="6.29285" y1="3.18135" x2="9.49325" y2="3.19405" layer="21"/>
<rectangle x1="-0.00635" y1="3.19405" x2="2.25425" y2="3.20675" layer="21"/>
<rectangle x1="3.28295" y1="3.19405" x2="4.00685" y2="3.20675" layer="21"/>
<rectangle x1="4.83235" y1="3.19405" x2="5.25145" y2="3.20675" layer="21"/>
<rectangle x1="6.29285" y1="3.19405" x2="9.49325" y2="3.20675" layer="21"/>
<rectangle x1="-0.00635" y1="3.20675" x2="2.25425" y2="3.21945" layer="21"/>
<rectangle x1="3.28295" y1="3.20675" x2="4.00685" y2="3.21945" layer="21"/>
<rectangle x1="4.81965" y1="3.20675" x2="5.25145" y2="3.21945" layer="21"/>
<rectangle x1="6.29285" y1="3.20675" x2="9.49325" y2="3.21945" layer="21"/>
<rectangle x1="-0.00635" y1="3.21945" x2="2.25425" y2="3.23215" layer="21"/>
<rectangle x1="3.28295" y1="3.21945" x2="4.00685" y2="3.23215" layer="21"/>
<rectangle x1="4.81965" y1="3.21945" x2="5.26415" y2="3.23215" layer="21"/>
<rectangle x1="6.29285" y1="3.21945" x2="9.49325" y2="3.23215" layer="21"/>
<rectangle x1="-0.00635" y1="3.23215" x2="2.25425" y2="3.24485" layer="21"/>
<rectangle x1="3.29565" y1="3.23215" x2="4.00685" y2="3.24485" layer="21"/>
<rectangle x1="4.80695" y1="3.23215" x2="5.27685" y2="3.24485" layer="21"/>
<rectangle x1="6.29285" y1="3.23215" x2="9.49325" y2="3.24485" layer="21"/>
<rectangle x1="-0.00635" y1="3.24485" x2="2.25425" y2="3.25755" layer="21"/>
<rectangle x1="3.29565" y1="3.24485" x2="3.99415" y2="3.25755" layer="21"/>
<rectangle x1="4.80695" y1="3.24485" x2="5.27685" y2="3.25755" layer="21"/>
<rectangle x1="6.29285" y1="3.24485" x2="9.49325" y2="3.25755" layer="21"/>
<rectangle x1="-0.00635" y1="3.25755" x2="2.25425" y2="3.27025" layer="21"/>
<rectangle x1="3.29565" y1="3.25755" x2="3.99415" y2="3.27025" layer="21"/>
<rectangle x1="4.79425" y1="3.25755" x2="5.28955" y2="3.27025" layer="21"/>
<rectangle x1="6.29285" y1="3.25755" x2="9.49325" y2="3.27025" layer="21"/>
<rectangle x1="-0.00635" y1="3.27025" x2="2.25425" y2="3.28295" layer="21"/>
<rectangle x1="3.29565" y1="3.27025" x2="3.99415" y2="3.28295" layer="21"/>
<rectangle x1="4.79425" y1="3.27025" x2="5.28955" y2="3.28295" layer="21"/>
<rectangle x1="6.29285" y1="3.27025" x2="9.49325" y2="3.28295" layer="21"/>
<rectangle x1="-0.00635" y1="3.28295" x2="2.25425" y2="3.29565" layer="21"/>
<rectangle x1="3.29565" y1="3.28295" x2="3.99415" y2="3.29565" layer="21"/>
<rectangle x1="4.79425" y1="3.28295" x2="5.30225" y2="3.29565" layer="21"/>
<rectangle x1="6.29285" y1="3.28295" x2="9.49325" y2="3.29565" layer="21"/>
<rectangle x1="-0.00635" y1="3.29565" x2="2.25425" y2="3.30835" layer="21"/>
<rectangle x1="3.30835" y1="3.29565" x2="3.99415" y2="3.30835" layer="21"/>
<rectangle x1="4.78155" y1="3.29565" x2="5.30225" y2="3.30835" layer="21"/>
<rectangle x1="6.29285" y1="3.29565" x2="9.49325" y2="3.30835" layer="21"/>
<rectangle x1="-0.00635" y1="3.30835" x2="2.25425" y2="3.32105" layer="21"/>
<rectangle x1="4.78155" y1="3.30835" x2="5.30225" y2="3.32105" layer="21"/>
<rectangle x1="6.29285" y1="3.30835" x2="9.49325" y2="3.32105" layer="21"/>
<rectangle x1="-0.00635" y1="3.32105" x2="2.25425" y2="3.33375" layer="21"/>
<rectangle x1="4.78155" y1="3.32105" x2="5.31495" y2="3.33375" layer="21"/>
<rectangle x1="6.29285" y1="3.32105" x2="9.49325" y2="3.33375" layer="21"/>
<rectangle x1="-0.00635" y1="3.33375" x2="2.25425" y2="3.34645" layer="21"/>
<rectangle x1="4.76885" y1="3.33375" x2="5.31495" y2="3.34645" layer="21"/>
<rectangle x1="6.29285" y1="3.33375" x2="9.49325" y2="3.34645" layer="21"/>
<rectangle x1="-0.00635" y1="3.34645" x2="2.25425" y2="3.35915" layer="21"/>
<rectangle x1="4.76885" y1="3.34645" x2="5.31495" y2="3.35915" layer="21"/>
<rectangle x1="6.29285" y1="3.34645" x2="9.49325" y2="3.35915" layer="21"/>
<rectangle x1="-0.00635" y1="3.35915" x2="0.84455" y2="3.37185" layer="21"/>
<rectangle x1="4.76885" y1="3.35915" x2="5.31495" y2="3.37185" layer="21"/>
<rectangle x1="6.29285" y1="3.35915" x2="7.14375" y2="3.37185" layer="21"/>
<rectangle x1="8.65505" y1="3.35915" x2="9.49325" y2="3.37185" layer="21"/>
<rectangle x1="-0.00635" y1="3.37185" x2="0.84455" y2="3.38455" layer="21"/>
<rectangle x1="4.76885" y1="3.37185" x2="5.32765" y2="3.38455" layer="21"/>
<rectangle x1="6.29285" y1="3.37185" x2="7.14375" y2="3.38455" layer="21"/>
<rectangle x1="8.65505" y1="3.37185" x2="9.49325" y2="3.38455" layer="21"/>
<rectangle x1="-0.00635" y1="3.38455" x2="0.84455" y2="3.39725" layer="21"/>
<rectangle x1="4.75615" y1="3.38455" x2="5.32765" y2="3.39725" layer="21"/>
<rectangle x1="6.29285" y1="3.38455" x2="7.14375" y2="3.39725" layer="21"/>
<rectangle x1="8.65505" y1="3.38455" x2="9.49325" y2="3.39725" layer="21"/>
<rectangle x1="-0.00635" y1="3.39725" x2="0.84455" y2="3.40995" layer="21"/>
<rectangle x1="4.75615" y1="3.39725" x2="5.32765" y2="3.40995" layer="21"/>
<rectangle x1="6.29285" y1="3.39725" x2="7.14375" y2="3.40995" layer="21"/>
<rectangle x1="8.65505" y1="3.39725" x2="9.49325" y2="3.40995" layer="21"/>
<rectangle x1="-0.00635" y1="3.40995" x2="0.84455" y2="3.42265" layer="21"/>
<rectangle x1="4.75615" y1="3.40995" x2="5.32765" y2="3.42265" layer="21"/>
<rectangle x1="6.29285" y1="3.40995" x2="7.14375" y2="3.42265" layer="21"/>
<rectangle x1="8.65505" y1="3.40995" x2="9.49325" y2="3.42265" layer="21"/>
<rectangle x1="-0.00635" y1="3.42265" x2="0.84455" y2="3.43535" layer="21"/>
<rectangle x1="4.75615" y1="3.42265" x2="5.32765" y2="3.43535" layer="21"/>
<rectangle x1="6.29285" y1="3.42265" x2="7.14375" y2="3.43535" layer="21"/>
<rectangle x1="8.65505" y1="3.42265" x2="9.49325" y2="3.43535" layer="21"/>
<rectangle x1="-0.00635" y1="3.43535" x2="0.84455" y2="3.44805" layer="21"/>
<rectangle x1="4.75615" y1="3.43535" x2="5.34035" y2="3.44805" layer="21"/>
<rectangle x1="6.29285" y1="3.43535" x2="7.14375" y2="3.44805" layer="21"/>
<rectangle x1="8.65505" y1="3.43535" x2="9.49325" y2="3.44805" layer="21"/>
<rectangle x1="-0.00635" y1="3.44805" x2="0.84455" y2="3.46075" layer="21"/>
<rectangle x1="4.74345" y1="3.44805" x2="5.34035" y2="3.46075" layer="21"/>
<rectangle x1="6.29285" y1="3.44805" x2="7.14375" y2="3.46075" layer="21"/>
<rectangle x1="8.65505" y1="3.44805" x2="9.49325" y2="3.46075" layer="21"/>
<rectangle x1="-0.00635" y1="3.46075" x2="0.84455" y2="3.47345" layer="21"/>
<rectangle x1="4.74345" y1="3.46075" x2="5.34035" y2="3.47345" layer="21"/>
<rectangle x1="6.29285" y1="3.46075" x2="7.14375" y2="3.47345" layer="21"/>
<rectangle x1="8.65505" y1="3.46075" x2="9.49325" y2="3.47345" layer="21"/>
<rectangle x1="-0.00635" y1="3.47345" x2="0.84455" y2="3.48615" layer="21"/>
<rectangle x1="4.74345" y1="3.47345" x2="5.34035" y2="3.48615" layer="21"/>
<rectangle x1="6.29285" y1="3.47345" x2="7.14375" y2="3.48615" layer="21"/>
<rectangle x1="8.65505" y1="3.47345" x2="9.49325" y2="3.48615" layer="21"/>
<rectangle x1="-0.00635" y1="3.48615" x2="0.84455" y2="3.49885" layer="21"/>
<rectangle x1="4.74345" y1="3.48615" x2="5.34035" y2="3.49885" layer="21"/>
<rectangle x1="6.29285" y1="3.48615" x2="7.14375" y2="3.49885" layer="21"/>
<rectangle x1="8.65505" y1="3.48615" x2="9.49325" y2="3.49885" layer="21"/>
<rectangle x1="-0.00635" y1="3.49885" x2="0.84455" y2="3.51155" layer="21"/>
<rectangle x1="4.74345" y1="3.49885" x2="5.34035" y2="3.51155" layer="21"/>
<rectangle x1="6.29285" y1="3.49885" x2="7.14375" y2="3.51155" layer="21"/>
<rectangle x1="8.65505" y1="3.49885" x2="9.49325" y2="3.51155" layer="21"/>
<rectangle x1="-0.00635" y1="3.51155" x2="0.84455" y2="3.52425" layer="21"/>
<rectangle x1="4.74345" y1="3.51155" x2="5.34035" y2="3.52425" layer="21"/>
<rectangle x1="6.29285" y1="3.51155" x2="7.14375" y2="3.52425" layer="21"/>
<rectangle x1="8.65505" y1="3.51155" x2="9.49325" y2="3.52425" layer="21"/>
<rectangle x1="-0.00635" y1="3.52425" x2="0.84455" y2="3.53695" layer="21"/>
<rectangle x1="4.74345" y1="3.52425" x2="5.34035" y2="3.53695" layer="21"/>
<rectangle x1="6.29285" y1="3.52425" x2="7.14375" y2="3.53695" layer="21"/>
<rectangle x1="8.65505" y1="3.52425" x2="9.49325" y2="3.53695" layer="21"/>
<rectangle x1="-0.00635" y1="3.53695" x2="0.84455" y2="3.54965" layer="21"/>
<rectangle x1="4.73075" y1="3.53695" x2="5.34035" y2="3.54965" layer="21"/>
<rectangle x1="6.29285" y1="3.53695" x2="7.14375" y2="3.54965" layer="21"/>
<rectangle x1="8.65505" y1="3.53695" x2="9.49325" y2="3.54965" layer="21"/>
<rectangle x1="-0.00635" y1="3.54965" x2="0.84455" y2="3.56235" layer="21"/>
<rectangle x1="4.73075" y1="3.54965" x2="5.34035" y2="3.56235" layer="21"/>
<rectangle x1="6.29285" y1="3.54965" x2="7.14375" y2="3.56235" layer="21"/>
<rectangle x1="8.65505" y1="3.54965" x2="9.49325" y2="3.56235" layer="21"/>
<rectangle x1="-0.00635" y1="3.56235" x2="0.84455" y2="3.57505" layer="21"/>
<rectangle x1="4.73075" y1="3.56235" x2="5.35305" y2="3.57505" layer="21"/>
<rectangle x1="6.29285" y1="3.56235" x2="7.14375" y2="3.57505" layer="21"/>
<rectangle x1="8.65505" y1="3.56235" x2="9.49325" y2="3.57505" layer="21"/>
<rectangle x1="-0.00635" y1="3.57505" x2="0.84455" y2="3.58775" layer="21"/>
<rectangle x1="4.73075" y1="3.57505" x2="5.35305" y2="3.58775" layer="21"/>
<rectangle x1="6.29285" y1="3.57505" x2="7.14375" y2="3.58775" layer="21"/>
<rectangle x1="8.65505" y1="3.57505" x2="9.49325" y2="3.58775" layer="21"/>
<rectangle x1="-0.00635" y1="3.58775" x2="0.84455" y2="3.60045" layer="21"/>
<rectangle x1="4.73075" y1="3.58775" x2="5.35305" y2="3.60045" layer="21"/>
<rectangle x1="6.29285" y1="3.58775" x2="7.14375" y2="3.60045" layer="21"/>
<rectangle x1="8.65505" y1="3.58775" x2="9.49325" y2="3.60045" layer="21"/>
<rectangle x1="-0.00635" y1="3.60045" x2="0.84455" y2="3.61315" layer="21"/>
<rectangle x1="4.73075" y1="3.60045" x2="5.35305" y2="3.61315" layer="21"/>
<rectangle x1="6.29285" y1="3.60045" x2="7.14375" y2="3.61315" layer="21"/>
<rectangle x1="8.65505" y1="3.60045" x2="9.49325" y2="3.61315" layer="21"/>
<rectangle x1="-0.00635" y1="3.61315" x2="0.84455" y2="3.62585" layer="21"/>
<rectangle x1="4.73075" y1="3.61315" x2="5.35305" y2="3.62585" layer="21"/>
<rectangle x1="6.29285" y1="3.61315" x2="7.14375" y2="3.62585" layer="21"/>
<rectangle x1="8.65505" y1="3.61315" x2="9.49325" y2="3.62585" layer="21"/>
<rectangle x1="-0.00635" y1="3.62585" x2="0.84455" y2="3.63855" layer="21"/>
<rectangle x1="4.73075" y1="3.62585" x2="5.35305" y2="3.63855" layer="21"/>
<rectangle x1="6.29285" y1="3.62585" x2="7.14375" y2="3.63855" layer="21"/>
<rectangle x1="8.65505" y1="3.62585" x2="9.49325" y2="3.63855" layer="21"/>
<rectangle x1="-0.00635" y1="3.63855" x2="0.84455" y2="3.65125" layer="21"/>
<rectangle x1="4.73075" y1="3.63855" x2="5.35305" y2="3.65125" layer="21"/>
<rectangle x1="6.29285" y1="3.63855" x2="7.14375" y2="3.65125" layer="21"/>
<rectangle x1="8.65505" y1="3.63855" x2="9.49325" y2="3.65125" layer="21"/>
<rectangle x1="-0.00635" y1="3.65125" x2="0.84455" y2="3.66395" layer="21"/>
<rectangle x1="4.73075" y1="3.65125" x2="5.35305" y2="3.66395" layer="21"/>
<rectangle x1="6.29285" y1="3.65125" x2="7.14375" y2="3.66395" layer="21"/>
<rectangle x1="8.65505" y1="3.65125" x2="9.49325" y2="3.66395" layer="21"/>
<rectangle x1="-0.00635" y1="3.66395" x2="0.84455" y2="3.67665" layer="21"/>
<rectangle x1="4.73075" y1="3.66395" x2="5.35305" y2="3.67665" layer="21"/>
<rectangle x1="6.29285" y1="3.66395" x2="7.14375" y2="3.67665" layer="21"/>
<rectangle x1="8.65505" y1="3.66395" x2="9.49325" y2="3.67665" layer="21"/>
<rectangle x1="-0.00635" y1="3.67665" x2="0.84455" y2="3.68935" layer="21"/>
<rectangle x1="4.71805" y1="3.67665" x2="5.35305" y2="3.68935" layer="21"/>
<rectangle x1="6.29285" y1="3.67665" x2="7.14375" y2="3.68935" layer="21"/>
<rectangle x1="8.65505" y1="3.67665" x2="9.49325" y2="3.68935" layer="21"/>
<rectangle x1="-0.00635" y1="3.68935" x2="0.84455" y2="3.70205" layer="21"/>
<rectangle x1="4.71805" y1="3.68935" x2="5.35305" y2="3.70205" layer="21"/>
<rectangle x1="6.29285" y1="3.68935" x2="7.14375" y2="3.70205" layer="21"/>
<rectangle x1="8.65505" y1="3.68935" x2="9.49325" y2="3.70205" layer="21"/>
<rectangle x1="-0.00635" y1="3.70205" x2="0.84455" y2="3.71475" layer="21"/>
<rectangle x1="4.71805" y1="3.70205" x2="5.35305" y2="3.71475" layer="21"/>
<rectangle x1="6.29285" y1="3.70205" x2="7.14375" y2="3.71475" layer="21"/>
<rectangle x1="8.65505" y1="3.70205" x2="9.49325" y2="3.71475" layer="21"/>
<rectangle x1="-0.00635" y1="3.71475" x2="0.84455" y2="3.72745" layer="21"/>
<rectangle x1="4.71805" y1="3.71475" x2="5.35305" y2="3.72745" layer="21"/>
<rectangle x1="6.29285" y1="3.71475" x2="7.14375" y2="3.72745" layer="21"/>
<rectangle x1="8.65505" y1="3.71475" x2="9.49325" y2="3.72745" layer="21"/>
<rectangle x1="-0.00635" y1="3.72745" x2="0.84455" y2="3.74015" layer="21"/>
<rectangle x1="4.71805" y1="3.72745" x2="5.35305" y2="3.74015" layer="21"/>
<rectangle x1="6.29285" y1="3.72745" x2="7.14375" y2="3.74015" layer="21"/>
<rectangle x1="8.65505" y1="3.72745" x2="9.49325" y2="3.74015" layer="21"/>
<rectangle x1="-0.00635" y1="3.74015" x2="0.84455" y2="3.75285" layer="21"/>
<rectangle x1="4.71805" y1="3.74015" x2="5.35305" y2="3.75285" layer="21"/>
<rectangle x1="6.29285" y1="3.74015" x2="7.14375" y2="3.75285" layer="21"/>
<rectangle x1="8.65505" y1="3.74015" x2="9.49325" y2="3.75285" layer="21"/>
<rectangle x1="-0.00635" y1="3.75285" x2="0.84455" y2="3.76555" layer="21"/>
<rectangle x1="4.71805" y1="3.75285" x2="5.35305" y2="3.76555" layer="21"/>
<rectangle x1="6.29285" y1="3.75285" x2="7.14375" y2="3.76555" layer="21"/>
<rectangle x1="8.65505" y1="3.75285" x2="9.49325" y2="3.76555" layer="21"/>
<rectangle x1="-0.00635" y1="3.76555" x2="0.84455" y2="3.77825" layer="21"/>
<rectangle x1="3.39725" y1="3.76555" x2="3.87985" y2="3.77825" layer="21"/>
<rectangle x1="4.71805" y1="3.76555" x2="5.35305" y2="3.77825" layer="21"/>
<rectangle x1="6.29285" y1="3.76555" x2="7.14375" y2="3.77825" layer="21"/>
<rectangle x1="8.65505" y1="3.76555" x2="9.49325" y2="3.77825" layer="21"/>
<rectangle x1="-0.00635" y1="3.77825" x2="0.84455" y2="3.79095" layer="21"/>
<rectangle x1="3.39725" y1="3.77825" x2="3.87985" y2="3.79095" layer="21"/>
<rectangle x1="4.71805" y1="3.77825" x2="5.35305" y2="3.79095" layer="21"/>
<rectangle x1="6.29285" y1="3.77825" x2="7.14375" y2="3.79095" layer="21"/>
<rectangle x1="8.65505" y1="3.77825" x2="9.49325" y2="3.79095" layer="21"/>
<rectangle x1="-0.00635" y1="3.79095" x2="0.84455" y2="3.80365" layer="21"/>
<rectangle x1="3.39725" y1="3.79095" x2="3.87985" y2="3.80365" layer="21"/>
<rectangle x1="4.71805" y1="3.79095" x2="5.59435" y2="3.80365" layer="21"/>
<rectangle x1="6.29285" y1="3.79095" x2="7.14375" y2="3.80365" layer="21"/>
<rectangle x1="8.65505" y1="3.79095" x2="9.49325" y2="3.80365" layer="21"/>
<rectangle x1="-0.00635" y1="3.80365" x2="0.84455" y2="3.81635" layer="21"/>
<rectangle x1="3.40995" y1="3.80365" x2="3.87985" y2="3.81635" layer="21"/>
<rectangle x1="4.71805" y1="3.80365" x2="5.59435" y2="3.81635" layer="21"/>
<rectangle x1="6.29285" y1="3.80365" x2="7.14375" y2="3.81635" layer="21"/>
<rectangle x1="8.65505" y1="3.80365" x2="9.49325" y2="3.81635" layer="21"/>
<rectangle x1="-0.00635" y1="3.81635" x2="0.84455" y2="3.82905" layer="21"/>
<rectangle x1="3.40995" y1="3.81635" x2="3.87985" y2="3.82905" layer="21"/>
<rectangle x1="4.71805" y1="3.81635" x2="5.59435" y2="3.82905" layer="21"/>
<rectangle x1="6.29285" y1="3.81635" x2="7.14375" y2="3.82905" layer="21"/>
<rectangle x1="8.65505" y1="3.81635" x2="9.49325" y2="3.82905" layer="21"/>
<rectangle x1="-0.00635" y1="3.82905" x2="0.84455" y2="3.84175" layer="21"/>
<rectangle x1="3.40995" y1="3.82905" x2="3.86715" y2="3.84175" layer="21"/>
<rectangle x1="4.71805" y1="3.82905" x2="5.59435" y2="3.84175" layer="21"/>
<rectangle x1="6.29285" y1="3.82905" x2="7.14375" y2="3.84175" layer="21"/>
<rectangle x1="8.65505" y1="3.82905" x2="9.49325" y2="3.84175" layer="21"/>
<rectangle x1="-0.00635" y1="3.84175" x2="0.84455" y2="3.85445" layer="21"/>
<rectangle x1="3.40995" y1="3.84175" x2="3.86715" y2="3.85445" layer="21"/>
<rectangle x1="4.71805" y1="3.84175" x2="5.59435" y2="3.85445" layer="21"/>
<rectangle x1="6.29285" y1="3.84175" x2="7.14375" y2="3.85445" layer="21"/>
<rectangle x1="8.65505" y1="3.84175" x2="9.49325" y2="3.85445" layer="21"/>
<rectangle x1="-0.00635" y1="3.85445" x2="0.84455" y2="3.86715" layer="21"/>
<rectangle x1="3.42265" y1="3.85445" x2="3.86715" y2="3.86715" layer="21"/>
<rectangle x1="4.71805" y1="3.85445" x2="5.59435" y2="3.86715" layer="21"/>
<rectangle x1="6.29285" y1="3.85445" x2="7.14375" y2="3.86715" layer="21"/>
<rectangle x1="8.65505" y1="3.85445" x2="9.49325" y2="3.86715" layer="21"/>
<rectangle x1="-0.00635" y1="3.86715" x2="0.84455" y2="3.87985" layer="21"/>
<rectangle x1="3.42265" y1="3.86715" x2="3.86715" y2="3.87985" layer="21"/>
<rectangle x1="4.71805" y1="3.86715" x2="5.59435" y2="3.87985" layer="21"/>
<rectangle x1="6.29285" y1="3.86715" x2="7.14375" y2="3.87985" layer="21"/>
<rectangle x1="8.65505" y1="3.86715" x2="9.49325" y2="3.87985" layer="21"/>
<rectangle x1="-0.00635" y1="3.87985" x2="0.84455" y2="3.89255" layer="21"/>
<rectangle x1="3.42265" y1="3.87985" x2="3.86715" y2="3.89255" layer="21"/>
<rectangle x1="4.71805" y1="3.87985" x2="5.59435" y2="3.89255" layer="21"/>
<rectangle x1="6.29285" y1="3.87985" x2="7.14375" y2="3.89255" layer="21"/>
<rectangle x1="8.65505" y1="3.87985" x2="9.49325" y2="3.89255" layer="21"/>
<rectangle x1="-0.00635" y1="3.89255" x2="0.84455" y2="3.90525" layer="21"/>
<rectangle x1="3.42265" y1="3.89255" x2="3.85445" y2="3.90525" layer="21"/>
<rectangle x1="4.71805" y1="3.89255" x2="5.59435" y2="3.90525" layer="21"/>
<rectangle x1="6.29285" y1="3.89255" x2="7.14375" y2="3.90525" layer="21"/>
<rectangle x1="8.65505" y1="3.89255" x2="9.49325" y2="3.90525" layer="21"/>
<rectangle x1="-0.00635" y1="3.90525" x2="0.84455" y2="3.91795" layer="21"/>
<rectangle x1="3.42265" y1="3.90525" x2="3.85445" y2="3.91795" layer="21"/>
<rectangle x1="4.70535" y1="3.90525" x2="5.59435" y2="3.91795" layer="21"/>
<rectangle x1="6.29285" y1="3.90525" x2="7.14375" y2="3.91795" layer="21"/>
<rectangle x1="8.65505" y1="3.90525" x2="9.49325" y2="3.91795" layer="21"/>
<rectangle x1="-0.00635" y1="3.91795" x2="0.84455" y2="3.93065" layer="21"/>
<rectangle x1="3.43535" y1="3.91795" x2="3.85445" y2="3.93065" layer="21"/>
<rectangle x1="4.70535" y1="3.91795" x2="5.59435" y2="3.93065" layer="21"/>
<rectangle x1="6.29285" y1="3.91795" x2="7.14375" y2="3.93065" layer="21"/>
<rectangle x1="8.65505" y1="3.91795" x2="9.49325" y2="3.93065" layer="21"/>
<rectangle x1="-0.00635" y1="3.93065" x2="0.84455" y2="3.94335" layer="21"/>
<rectangle x1="2.92735" y1="3.93065" x2="2.94005" y2="3.94335" layer="21"/>
<rectangle x1="3.43535" y1="3.93065" x2="3.85445" y2="3.94335" layer="21"/>
<rectangle x1="4.70535" y1="3.93065" x2="5.59435" y2="3.94335" layer="21"/>
<rectangle x1="6.29285" y1="3.93065" x2="7.14375" y2="3.94335" layer="21"/>
<rectangle x1="8.65505" y1="3.93065" x2="9.49325" y2="3.94335" layer="21"/>
<rectangle x1="-0.00635" y1="3.94335" x2="0.84455" y2="3.95605" layer="21"/>
<rectangle x1="2.92735" y1="3.94335" x2="2.94005" y2="3.95605" layer="21"/>
<rectangle x1="3.43535" y1="3.94335" x2="3.85445" y2="3.95605" layer="21"/>
<rectangle x1="4.70535" y1="3.94335" x2="5.59435" y2="3.95605" layer="21"/>
<rectangle x1="6.29285" y1="3.94335" x2="7.14375" y2="3.95605" layer="21"/>
<rectangle x1="8.65505" y1="3.94335" x2="9.49325" y2="3.95605" layer="21"/>
<rectangle x1="-0.00635" y1="3.95605" x2="0.84455" y2="3.96875" layer="21"/>
<rectangle x1="2.92735" y1="3.95605" x2="2.94005" y2="3.96875" layer="21"/>
<rectangle x1="3.43535" y1="3.95605" x2="3.84175" y2="3.96875" layer="21"/>
<rectangle x1="4.70535" y1="3.95605" x2="5.59435" y2="3.96875" layer="21"/>
<rectangle x1="6.29285" y1="3.95605" x2="7.14375" y2="3.96875" layer="21"/>
<rectangle x1="8.65505" y1="3.95605" x2="9.49325" y2="3.96875" layer="21"/>
<rectangle x1="-0.00635" y1="3.96875" x2="0.84455" y2="3.98145" layer="21"/>
<rectangle x1="2.92735" y1="3.96875" x2="2.94005" y2="3.98145" layer="21"/>
<rectangle x1="3.43535" y1="3.96875" x2="3.84175" y2="3.98145" layer="21"/>
<rectangle x1="4.70535" y1="3.96875" x2="5.59435" y2="3.98145" layer="21"/>
<rectangle x1="6.29285" y1="3.96875" x2="7.14375" y2="3.98145" layer="21"/>
<rectangle x1="8.65505" y1="3.96875" x2="9.49325" y2="3.98145" layer="21"/>
<rectangle x1="-0.00635" y1="3.98145" x2="0.84455" y2="3.99415" layer="21"/>
<rectangle x1="2.92735" y1="3.98145" x2="2.95275" y2="3.99415" layer="21"/>
<rectangle x1="3.44805" y1="3.98145" x2="3.84175" y2="3.99415" layer="21"/>
<rectangle x1="4.70535" y1="3.98145" x2="5.59435" y2="3.99415" layer="21"/>
<rectangle x1="6.29285" y1="3.98145" x2="7.14375" y2="3.99415" layer="21"/>
<rectangle x1="8.65505" y1="3.98145" x2="9.49325" y2="3.99415" layer="21"/>
<rectangle x1="-0.00635" y1="3.99415" x2="0.84455" y2="4.00685" layer="21"/>
<rectangle x1="2.92735" y1="3.99415" x2="2.95275" y2="4.00685" layer="21"/>
<rectangle x1="3.44805" y1="3.99415" x2="3.84175" y2="4.00685" layer="21"/>
<rectangle x1="4.70535" y1="3.99415" x2="5.59435" y2="4.00685" layer="21"/>
<rectangle x1="6.29285" y1="3.99415" x2="7.14375" y2="4.00685" layer="21"/>
<rectangle x1="8.65505" y1="3.99415" x2="9.49325" y2="4.00685" layer="21"/>
<rectangle x1="-0.00635" y1="4.00685" x2="0.84455" y2="4.01955" layer="21"/>
<rectangle x1="2.92735" y1="4.00685" x2="2.95275" y2="4.01955" layer="21"/>
<rectangle x1="3.44805" y1="4.00685" x2="3.84175" y2="4.01955" layer="21"/>
<rectangle x1="4.70535" y1="4.00685" x2="5.59435" y2="4.01955" layer="21"/>
<rectangle x1="6.29285" y1="4.00685" x2="7.14375" y2="4.01955" layer="21"/>
<rectangle x1="8.65505" y1="4.00685" x2="9.49325" y2="4.01955" layer="21"/>
<rectangle x1="-0.00635" y1="4.01955" x2="0.84455" y2="4.03225" layer="21"/>
<rectangle x1="2.92735" y1="4.01955" x2="2.95275" y2="4.03225" layer="21"/>
<rectangle x1="3.44805" y1="4.01955" x2="3.82905" y2="4.03225" layer="21"/>
<rectangle x1="4.70535" y1="4.01955" x2="5.59435" y2="4.03225" layer="21"/>
<rectangle x1="6.29285" y1="4.01955" x2="7.14375" y2="4.03225" layer="21"/>
<rectangle x1="8.65505" y1="4.01955" x2="9.49325" y2="4.03225" layer="21"/>
<rectangle x1="-0.00635" y1="4.03225" x2="0.84455" y2="4.04495" layer="21"/>
<rectangle x1="2.92735" y1="4.03225" x2="2.96545" y2="4.04495" layer="21"/>
<rectangle x1="3.44805" y1="4.03225" x2="3.82905" y2="4.04495" layer="21"/>
<rectangle x1="4.70535" y1="4.03225" x2="5.59435" y2="4.04495" layer="21"/>
<rectangle x1="6.29285" y1="4.03225" x2="7.14375" y2="4.04495" layer="21"/>
<rectangle x1="8.65505" y1="4.03225" x2="9.49325" y2="4.04495" layer="21"/>
<rectangle x1="-0.00635" y1="4.04495" x2="0.84455" y2="4.05765" layer="21"/>
<rectangle x1="2.92735" y1="4.04495" x2="2.96545" y2="4.05765" layer="21"/>
<rectangle x1="3.46075" y1="4.04495" x2="3.82905" y2="4.05765" layer="21"/>
<rectangle x1="4.70535" y1="4.04495" x2="5.59435" y2="4.05765" layer="21"/>
<rectangle x1="6.29285" y1="4.04495" x2="7.14375" y2="4.05765" layer="21"/>
<rectangle x1="8.65505" y1="4.04495" x2="9.49325" y2="4.05765" layer="21"/>
<rectangle x1="-0.00635" y1="4.05765" x2="0.84455" y2="4.07035" layer="21"/>
<rectangle x1="2.92735" y1="4.05765" x2="2.96545" y2="4.07035" layer="21"/>
<rectangle x1="3.46075" y1="4.05765" x2="3.82905" y2="4.07035" layer="21"/>
<rectangle x1="4.70535" y1="4.05765" x2="5.59435" y2="4.07035" layer="21"/>
<rectangle x1="6.29285" y1="4.05765" x2="7.14375" y2="4.07035" layer="21"/>
<rectangle x1="8.65505" y1="4.05765" x2="9.49325" y2="4.07035" layer="21"/>
<rectangle x1="-0.00635" y1="4.07035" x2="0.84455" y2="4.08305" layer="21"/>
<rectangle x1="2.92735" y1="4.07035" x2="2.96545" y2="4.08305" layer="21"/>
<rectangle x1="3.46075" y1="4.07035" x2="3.82905" y2="4.08305" layer="21"/>
<rectangle x1="4.70535" y1="4.07035" x2="5.59435" y2="4.08305" layer="21"/>
<rectangle x1="6.29285" y1="4.07035" x2="7.14375" y2="4.08305" layer="21"/>
<rectangle x1="8.65505" y1="4.07035" x2="9.49325" y2="4.08305" layer="21"/>
<rectangle x1="-0.00635" y1="4.08305" x2="0.84455" y2="4.09575" layer="21"/>
<rectangle x1="2.92735" y1="4.08305" x2="2.97815" y2="4.09575" layer="21"/>
<rectangle x1="3.46075" y1="4.08305" x2="3.81635" y2="4.09575" layer="21"/>
<rectangle x1="4.70535" y1="4.08305" x2="5.59435" y2="4.09575" layer="21"/>
<rectangle x1="6.29285" y1="4.08305" x2="7.14375" y2="4.09575" layer="21"/>
<rectangle x1="8.65505" y1="4.08305" x2="9.49325" y2="4.09575" layer="21"/>
<rectangle x1="-0.00635" y1="4.09575" x2="0.84455" y2="4.10845" layer="21"/>
<rectangle x1="2.92735" y1="4.09575" x2="2.97815" y2="4.10845" layer="21"/>
<rectangle x1="3.46075" y1="4.09575" x2="3.81635" y2="4.10845" layer="21"/>
<rectangle x1="4.70535" y1="4.09575" x2="5.59435" y2="4.10845" layer="21"/>
<rectangle x1="6.29285" y1="4.09575" x2="7.14375" y2="4.10845" layer="21"/>
<rectangle x1="8.65505" y1="4.09575" x2="9.49325" y2="4.10845" layer="21"/>
<rectangle x1="-0.00635" y1="4.10845" x2="0.84455" y2="4.12115" layer="21"/>
<rectangle x1="2.92735" y1="4.10845" x2="2.97815" y2="4.12115" layer="21"/>
<rectangle x1="3.47345" y1="4.10845" x2="3.81635" y2="4.12115" layer="21"/>
<rectangle x1="4.70535" y1="4.10845" x2="5.59435" y2="4.12115" layer="21"/>
<rectangle x1="6.29285" y1="4.10845" x2="7.14375" y2="4.12115" layer="21"/>
<rectangle x1="8.65505" y1="4.10845" x2="9.49325" y2="4.12115" layer="21"/>
<rectangle x1="-0.00635" y1="4.12115" x2="0.84455" y2="4.13385" layer="21"/>
<rectangle x1="2.92735" y1="4.12115" x2="2.97815" y2="4.13385" layer="21"/>
<rectangle x1="3.47345" y1="4.12115" x2="3.81635" y2="4.13385" layer="21"/>
<rectangle x1="4.70535" y1="4.12115" x2="5.59435" y2="4.13385" layer="21"/>
<rectangle x1="6.29285" y1="4.12115" x2="7.14375" y2="4.13385" layer="21"/>
<rectangle x1="8.65505" y1="4.12115" x2="9.49325" y2="4.13385" layer="21"/>
<rectangle x1="-0.00635" y1="4.13385" x2="0.84455" y2="4.14655" layer="21"/>
<rectangle x1="2.92735" y1="4.13385" x2="2.99085" y2="4.14655" layer="21"/>
<rectangle x1="3.47345" y1="4.13385" x2="3.81635" y2="4.14655" layer="21"/>
<rectangle x1="4.70535" y1="4.13385" x2="5.59435" y2="4.14655" layer="21"/>
<rectangle x1="6.29285" y1="4.13385" x2="7.14375" y2="4.14655" layer="21"/>
<rectangle x1="8.65505" y1="4.13385" x2="9.49325" y2="4.14655" layer="21"/>
<rectangle x1="-0.00635" y1="4.14655" x2="0.84455" y2="4.15925" layer="21"/>
<rectangle x1="2.92735" y1="4.14655" x2="2.99085" y2="4.15925" layer="21"/>
<rectangle x1="3.47345" y1="4.14655" x2="3.80365" y2="4.15925" layer="21"/>
<rectangle x1="4.70535" y1="4.14655" x2="5.59435" y2="4.15925" layer="21"/>
<rectangle x1="6.29285" y1="4.14655" x2="7.14375" y2="4.15925" layer="21"/>
<rectangle x1="8.65505" y1="4.14655" x2="9.49325" y2="4.15925" layer="21"/>
<rectangle x1="-0.00635" y1="4.15925" x2="0.84455" y2="4.17195" layer="21"/>
<rectangle x1="2.92735" y1="4.15925" x2="2.99085" y2="4.17195" layer="21"/>
<rectangle x1="3.47345" y1="4.15925" x2="3.80365" y2="4.17195" layer="21"/>
<rectangle x1="4.70535" y1="4.15925" x2="5.59435" y2="4.17195" layer="21"/>
<rectangle x1="6.29285" y1="4.15925" x2="7.14375" y2="4.17195" layer="21"/>
<rectangle x1="8.65505" y1="4.15925" x2="9.49325" y2="4.17195" layer="21"/>
<rectangle x1="-0.00635" y1="4.17195" x2="0.84455" y2="4.18465" layer="21"/>
<rectangle x1="2.92735" y1="4.17195" x2="2.99085" y2="4.18465" layer="21"/>
<rectangle x1="3.48615" y1="4.17195" x2="3.80365" y2="4.18465" layer="21"/>
<rectangle x1="4.70535" y1="4.17195" x2="5.59435" y2="4.18465" layer="21"/>
<rectangle x1="6.29285" y1="4.17195" x2="7.14375" y2="4.18465" layer="21"/>
<rectangle x1="8.65505" y1="4.17195" x2="9.49325" y2="4.18465" layer="21"/>
<rectangle x1="-0.00635" y1="4.18465" x2="0.84455" y2="4.19735" layer="21"/>
<rectangle x1="2.92735" y1="4.18465" x2="3.00355" y2="4.19735" layer="21"/>
<rectangle x1="3.48615" y1="4.18465" x2="3.80365" y2="4.19735" layer="21"/>
<rectangle x1="4.70535" y1="4.18465" x2="5.59435" y2="4.19735" layer="21"/>
<rectangle x1="6.29285" y1="4.18465" x2="7.14375" y2="4.19735" layer="21"/>
<rectangle x1="8.65505" y1="4.18465" x2="9.49325" y2="4.19735" layer="21"/>
<rectangle x1="-0.00635" y1="4.19735" x2="0.84455" y2="4.21005" layer="21"/>
<rectangle x1="2.92735" y1="4.19735" x2="3.00355" y2="4.21005" layer="21"/>
<rectangle x1="3.48615" y1="4.19735" x2="3.80365" y2="4.21005" layer="21"/>
<rectangle x1="4.70535" y1="4.19735" x2="5.59435" y2="4.21005" layer="21"/>
<rectangle x1="6.29285" y1="4.19735" x2="7.14375" y2="4.21005" layer="21"/>
<rectangle x1="8.65505" y1="4.19735" x2="9.49325" y2="4.21005" layer="21"/>
<rectangle x1="-0.00635" y1="4.21005" x2="0.84455" y2="4.22275" layer="21"/>
<rectangle x1="2.92735" y1="4.21005" x2="3.00355" y2="4.22275" layer="21"/>
<rectangle x1="3.48615" y1="4.21005" x2="3.79095" y2="4.22275" layer="21"/>
<rectangle x1="4.70535" y1="4.21005" x2="5.59435" y2="4.22275" layer="21"/>
<rectangle x1="6.29285" y1="4.21005" x2="7.14375" y2="4.22275" layer="21"/>
<rectangle x1="8.65505" y1="4.21005" x2="9.49325" y2="4.22275" layer="21"/>
<rectangle x1="-0.00635" y1="4.22275" x2="0.84455" y2="4.23545" layer="21"/>
<rectangle x1="2.92735" y1="4.22275" x2="3.00355" y2="4.23545" layer="21"/>
<rectangle x1="3.48615" y1="4.22275" x2="3.79095" y2="4.23545" layer="21"/>
<rectangle x1="4.70535" y1="4.22275" x2="5.59435" y2="4.23545" layer="21"/>
<rectangle x1="6.29285" y1="4.22275" x2="7.14375" y2="4.23545" layer="21"/>
<rectangle x1="8.65505" y1="4.22275" x2="9.49325" y2="4.23545" layer="21"/>
<rectangle x1="-0.00635" y1="4.23545" x2="0.84455" y2="4.24815" layer="21"/>
<rectangle x1="2.92735" y1="4.23545" x2="3.01625" y2="4.24815" layer="21"/>
<rectangle x1="3.49885" y1="4.23545" x2="3.79095" y2="4.24815" layer="21"/>
<rectangle x1="4.70535" y1="4.23545" x2="5.59435" y2="4.24815" layer="21"/>
<rectangle x1="6.29285" y1="4.23545" x2="7.14375" y2="4.24815" layer="21"/>
<rectangle x1="8.65505" y1="4.23545" x2="9.49325" y2="4.24815" layer="21"/>
<rectangle x1="-0.00635" y1="4.24815" x2="0.84455" y2="4.26085" layer="21"/>
<rectangle x1="2.92735" y1="4.24815" x2="3.01625" y2="4.26085" layer="21"/>
<rectangle x1="3.49885" y1="4.24815" x2="3.79095" y2="4.26085" layer="21"/>
<rectangle x1="4.70535" y1="4.24815" x2="5.59435" y2="4.26085" layer="21"/>
<rectangle x1="6.29285" y1="4.24815" x2="7.14375" y2="4.26085" layer="21"/>
<rectangle x1="8.65505" y1="4.24815" x2="9.49325" y2="4.26085" layer="21"/>
<rectangle x1="-0.00635" y1="4.26085" x2="0.84455" y2="4.27355" layer="21"/>
<rectangle x1="2.92735" y1="4.26085" x2="3.01625" y2="4.27355" layer="21"/>
<rectangle x1="3.49885" y1="4.26085" x2="3.77825" y2="4.27355" layer="21"/>
<rectangle x1="4.70535" y1="4.26085" x2="5.59435" y2="4.27355" layer="21"/>
<rectangle x1="6.29285" y1="4.26085" x2="7.14375" y2="4.27355" layer="21"/>
<rectangle x1="8.65505" y1="4.26085" x2="9.49325" y2="4.27355" layer="21"/>
<rectangle x1="-0.00635" y1="4.27355" x2="0.84455" y2="4.28625" layer="21"/>
<rectangle x1="2.92735" y1="4.27355" x2="3.01625" y2="4.28625" layer="21"/>
<rectangle x1="3.49885" y1="4.27355" x2="3.77825" y2="4.28625" layer="21"/>
<rectangle x1="4.70535" y1="4.27355" x2="5.59435" y2="4.28625" layer="21"/>
<rectangle x1="6.29285" y1="4.27355" x2="7.14375" y2="4.28625" layer="21"/>
<rectangle x1="8.65505" y1="4.27355" x2="9.49325" y2="4.28625" layer="21"/>
<rectangle x1="-0.00635" y1="4.28625" x2="0.84455" y2="4.29895" layer="21"/>
<rectangle x1="2.92735" y1="4.28625" x2="3.02895" y2="4.29895" layer="21"/>
<rectangle x1="3.51155" y1="4.28625" x2="3.77825" y2="4.29895" layer="21"/>
<rectangle x1="4.70535" y1="4.28625" x2="5.59435" y2="4.29895" layer="21"/>
<rectangle x1="6.29285" y1="4.28625" x2="7.14375" y2="4.29895" layer="21"/>
<rectangle x1="8.65505" y1="4.28625" x2="9.49325" y2="4.29895" layer="21"/>
<rectangle x1="-0.00635" y1="4.29895" x2="0.84455" y2="4.31165" layer="21"/>
<rectangle x1="2.92735" y1="4.29895" x2="3.02895" y2="4.31165" layer="21"/>
<rectangle x1="3.51155" y1="4.29895" x2="3.77825" y2="4.31165" layer="21"/>
<rectangle x1="4.70535" y1="4.29895" x2="5.59435" y2="4.31165" layer="21"/>
<rectangle x1="6.29285" y1="4.29895" x2="7.14375" y2="4.31165" layer="21"/>
<rectangle x1="8.65505" y1="4.29895" x2="9.49325" y2="4.31165" layer="21"/>
<rectangle x1="-0.00635" y1="4.31165" x2="0.84455" y2="4.32435" layer="21"/>
<rectangle x1="2.92735" y1="4.31165" x2="3.02895" y2="4.32435" layer="21"/>
<rectangle x1="3.51155" y1="4.31165" x2="3.77825" y2="4.32435" layer="21"/>
<rectangle x1="4.71805" y1="4.31165" x2="5.59435" y2="4.32435" layer="21"/>
<rectangle x1="6.29285" y1="4.31165" x2="7.14375" y2="4.32435" layer="21"/>
<rectangle x1="8.65505" y1="4.31165" x2="9.49325" y2="4.32435" layer="21"/>
<rectangle x1="-0.00635" y1="4.32435" x2="0.84455" y2="4.33705" layer="21"/>
<rectangle x1="2.92735" y1="4.32435" x2="3.02895" y2="4.33705" layer="21"/>
<rectangle x1="3.51155" y1="4.32435" x2="3.76555" y2="4.33705" layer="21"/>
<rectangle x1="4.71805" y1="4.32435" x2="5.59435" y2="4.33705" layer="21"/>
<rectangle x1="6.29285" y1="4.32435" x2="7.14375" y2="4.33705" layer="21"/>
<rectangle x1="8.65505" y1="4.32435" x2="9.49325" y2="4.33705" layer="21"/>
<rectangle x1="-0.00635" y1="4.33705" x2="0.84455" y2="4.34975" layer="21"/>
<rectangle x1="2.92735" y1="4.33705" x2="3.04165" y2="4.34975" layer="21"/>
<rectangle x1="3.51155" y1="4.33705" x2="3.76555" y2="4.34975" layer="21"/>
<rectangle x1="4.71805" y1="4.33705" x2="5.59435" y2="4.34975" layer="21"/>
<rectangle x1="6.29285" y1="4.33705" x2="7.14375" y2="4.34975" layer="21"/>
<rectangle x1="8.65505" y1="4.33705" x2="9.49325" y2="4.34975" layer="21"/>
<rectangle x1="-0.00635" y1="4.34975" x2="0.84455" y2="4.36245" layer="21"/>
<rectangle x1="2.92735" y1="4.34975" x2="3.04165" y2="4.36245" layer="21"/>
<rectangle x1="3.52425" y1="4.34975" x2="3.76555" y2="4.36245" layer="21"/>
<rectangle x1="4.71805" y1="4.34975" x2="5.59435" y2="4.36245" layer="21"/>
<rectangle x1="6.29285" y1="4.34975" x2="7.14375" y2="4.36245" layer="21"/>
<rectangle x1="8.65505" y1="4.34975" x2="9.49325" y2="4.36245" layer="21"/>
<rectangle x1="-0.00635" y1="4.36245" x2="0.84455" y2="4.37515" layer="21"/>
<rectangle x1="2.92735" y1="4.36245" x2="3.04165" y2="4.37515" layer="21"/>
<rectangle x1="3.52425" y1="4.36245" x2="3.76555" y2="4.37515" layer="21"/>
<rectangle x1="4.71805" y1="4.36245" x2="5.59435" y2="4.37515" layer="21"/>
<rectangle x1="6.29285" y1="4.36245" x2="7.14375" y2="4.37515" layer="21"/>
<rectangle x1="8.65505" y1="4.36245" x2="9.49325" y2="4.37515" layer="21"/>
<rectangle x1="-0.00635" y1="4.37515" x2="0.84455" y2="4.38785" layer="21"/>
<rectangle x1="2.92735" y1="4.37515" x2="3.04165" y2="4.38785" layer="21"/>
<rectangle x1="3.52425" y1="4.37515" x2="3.76555" y2="4.38785" layer="21"/>
<rectangle x1="4.71805" y1="4.37515" x2="5.59435" y2="4.38785" layer="21"/>
<rectangle x1="6.29285" y1="4.37515" x2="7.14375" y2="4.38785" layer="21"/>
<rectangle x1="8.65505" y1="4.37515" x2="9.49325" y2="4.38785" layer="21"/>
<rectangle x1="-0.00635" y1="4.38785" x2="0.84455" y2="4.40055" layer="21"/>
<rectangle x1="2.92735" y1="4.38785" x2="3.05435" y2="4.40055" layer="21"/>
<rectangle x1="3.52425" y1="4.38785" x2="3.75285" y2="4.40055" layer="21"/>
<rectangle x1="4.71805" y1="4.38785" x2="5.59435" y2="4.40055" layer="21"/>
<rectangle x1="6.29285" y1="4.38785" x2="7.14375" y2="4.40055" layer="21"/>
<rectangle x1="8.65505" y1="4.38785" x2="9.49325" y2="4.40055" layer="21"/>
<rectangle x1="-0.00635" y1="4.40055" x2="0.84455" y2="4.41325" layer="21"/>
<rectangle x1="2.92735" y1="4.40055" x2="3.05435" y2="4.41325" layer="21"/>
<rectangle x1="3.52425" y1="4.40055" x2="3.75285" y2="4.41325" layer="21"/>
<rectangle x1="4.71805" y1="4.40055" x2="5.59435" y2="4.41325" layer="21"/>
<rectangle x1="6.29285" y1="4.40055" x2="7.14375" y2="4.41325" layer="21"/>
<rectangle x1="8.65505" y1="4.40055" x2="9.49325" y2="4.41325" layer="21"/>
<rectangle x1="-0.00635" y1="4.41325" x2="0.84455" y2="4.42595" layer="21"/>
<rectangle x1="2.92735" y1="4.41325" x2="3.05435" y2="4.42595" layer="21"/>
<rectangle x1="3.53695" y1="4.41325" x2="3.75285" y2="4.42595" layer="21"/>
<rectangle x1="4.71805" y1="4.41325" x2="5.59435" y2="4.42595" layer="21"/>
<rectangle x1="6.29285" y1="4.41325" x2="7.14375" y2="4.42595" layer="21"/>
<rectangle x1="8.65505" y1="4.41325" x2="9.49325" y2="4.42595" layer="21"/>
<rectangle x1="-0.00635" y1="4.42595" x2="0.84455" y2="4.43865" layer="21"/>
<rectangle x1="2.92735" y1="4.42595" x2="3.05435" y2="4.43865" layer="21"/>
<rectangle x1="3.53695" y1="4.42595" x2="3.75285" y2="4.43865" layer="21"/>
<rectangle x1="4.71805" y1="4.42595" x2="5.59435" y2="4.43865" layer="21"/>
<rectangle x1="6.29285" y1="4.42595" x2="7.14375" y2="4.43865" layer="21"/>
<rectangle x1="8.65505" y1="4.42595" x2="9.49325" y2="4.43865" layer="21"/>
<rectangle x1="-0.00635" y1="4.43865" x2="0.84455" y2="4.45135" layer="21"/>
<rectangle x1="2.92735" y1="4.43865" x2="3.06705" y2="4.45135" layer="21"/>
<rectangle x1="3.53695" y1="4.43865" x2="3.75285" y2="4.45135" layer="21"/>
<rectangle x1="4.71805" y1="4.43865" x2="5.59435" y2="4.45135" layer="21"/>
<rectangle x1="6.29285" y1="4.43865" x2="7.14375" y2="4.45135" layer="21"/>
<rectangle x1="8.65505" y1="4.43865" x2="9.49325" y2="4.45135" layer="21"/>
<rectangle x1="-0.00635" y1="4.45135" x2="0.84455" y2="4.46405" layer="21"/>
<rectangle x1="2.92735" y1="4.45135" x2="3.06705" y2="4.46405" layer="21"/>
<rectangle x1="3.53695" y1="4.45135" x2="3.74015" y2="4.46405" layer="21"/>
<rectangle x1="4.71805" y1="4.45135" x2="5.59435" y2="4.46405" layer="21"/>
<rectangle x1="6.29285" y1="4.45135" x2="7.14375" y2="4.46405" layer="21"/>
<rectangle x1="8.65505" y1="4.45135" x2="9.49325" y2="4.46405" layer="21"/>
<rectangle x1="-0.00635" y1="4.46405" x2="0.84455" y2="4.47675" layer="21"/>
<rectangle x1="2.92735" y1="4.46405" x2="3.06705" y2="4.47675" layer="21"/>
<rectangle x1="3.53695" y1="4.46405" x2="3.74015" y2="4.47675" layer="21"/>
<rectangle x1="4.71805" y1="4.46405" x2="5.59435" y2="4.47675" layer="21"/>
<rectangle x1="6.29285" y1="4.46405" x2="7.14375" y2="4.47675" layer="21"/>
<rectangle x1="8.65505" y1="4.46405" x2="9.49325" y2="4.47675" layer="21"/>
<rectangle x1="-0.00635" y1="4.47675" x2="0.84455" y2="4.48945" layer="21"/>
<rectangle x1="2.92735" y1="4.47675" x2="3.06705" y2="4.48945" layer="21"/>
<rectangle x1="3.54965" y1="4.47675" x2="3.74015" y2="4.48945" layer="21"/>
<rectangle x1="4.71805" y1="4.47675" x2="5.59435" y2="4.48945" layer="21"/>
<rectangle x1="6.29285" y1="4.47675" x2="7.14375" y2="4.48945" layer="21"/>
<rectangle x1="8.65505" y1="4.47675" x2="9.49325" y2="4.48945" layer="21"/>
<rectangle x1="-0.00635" y1="4.48945" x2="0.84455" y2="4.50215" layer="21"/>
<rectangle x1="2.92735" y1="4.48945" x2="3.07975" y2="4.50215" layer="21"/>
<rectangle x1="3.54965" y1="4.48945" x2="3.74015" y2="4.50215" layer="21"/>
<rectangle x1="4.71805" y1="4.48945" x2="5.59435" y2="4.50215" layer="21"/>
<rectangle x1="6.29285" y1="4.48945" x2="7.14375" y2="4.50215" layer="21"/>
<rectangle x1="8.65505" y1="4.48945" x2="9.49325" y2="4.50215" layer="21"/>
<rectangle x1="-0.00635" y1="4.50215" x2="0.84455" y2="4.51485" layer="21"/>
<rectangle x1="2.92735" y1="4.50215" x2="3.07975" y2="4.51485" layer="21"/>
<rectangle x1="3.54965" y1="4.50215" x2="3.74015" y2="4.51485" layer="21"/>
<rectangle x1="4.71805" y1="4.50215" x2="5.59435" y2="4.51485" layer="21"/>
<rectangle x1="6.29285" y1="4.50215" x2="7.14375" y2="4.51485" layer="21"/>
<rectangle x1="8.65505" y1="4.50215" x2="9.49325" y2="4.51485" layer="21"/>
<rectangle x1="-0.00635" y1="4.51485" x2="0.84455" y2="4.52755" layer="21"/>
<rectangle x1="2.92735" y1="4.51485" x2="3.07975" y2="4.52755" layer="21"/>
<rectangle x1="3.54965" y1="4.51485" x2="3.72745" y2="4.52755" layer="21"/>
<rectangle x1="4.71805" y1="4.51485" x2="5.59435" y2="4.52755" layer="21"/>
<rectangle x1="6.29285" y1="4.51485" x2="7.14375" y2="4.52755" layer="21"/>
<rectangle x1="8.65505" y1="4.51485" x2="9.49325" y2="4.52755" layer="21"/>
<rectangle x1="-0.00635" y1="4.52755" x2="0.84455" y2="4.54025" layer="21"/>
<rectangle x1="2.92735" y1="4.52755" x2="3.07975" y2="4.54025" layer="21"/>
<rectangle x1="3.54965" y1="4.52755" x2="3.72745" y2="4.54025" layer="21"/>
<rectangle x1="4.71805" y1="4.52755" x2="5.59435" y2="4.54025" layer="21"/>
<rectangle x1="6.29285" y1="4.52755" x2="7.14375" y2="4.54025" layer="21"/>
<rectangle x1="8.65505" y1="4.52755" x2="9.49325" y2="4.54025" layer="21"/>
<rectangle x1="-0.00635" y1="4.54025" x2="0.84455" y2="4.55295" layer="21"/>
<rectangle x1="2.92735" y1="4.54025" x2="3.09245" y2="4.55295" layer="21"/>
<rectangle x1="3.56235" y1="4.54025" x2="3.72745" y2="4.55295" layer="21"/>
<rectangle x1="4.71805" y1="4.54025" x2="5.34035" y2="4.55295" layer="21"/>
<rectangle x1="6.29285" y1="4.54025" x2="7.14375" y2="4.55295" layer="21"/>
<rectangle x1="8.65505" y1="4.54025" x2="9.49325" y2="4.55295" layer="21"/>
<rectangle x1="-0.00635" y1="4.55295" x2="0.84455" y2="4.56565" layer="21"/>
<rectangle x1="2.92735" y1="4.55295" x2="3.09245" y2="4.56565" layer="21"/>
<rectangle x1="3.56235" y1="4.55295" x2="3.72745" y2="4.56565" layer="21"/>
<rectangle x1="4.71805" y1="4.55295" x2="5.34035" y2="4.56565" layer="21"/>
<rectangle x1="6.29285" y1="4.55295" x2="7.14375" y2="4.56565" layer="21"/>
<rectangle x1="8.65505" y1="4.55295" x2="9.49325" y2="4.56565" layer="21"/>
<rectangle x1="-0.00635" y1="4.56565" x2="0.84455" y2="4.57835" layer="21"/>
<rectangle x1="2.92735" y1="4.56565" x2="3.09245" y2="4.57835" layer="21"/>
<rectangle x1="3.56235" y1="4.56565" x2="3.72745" y2="4.57835" layer="21"/>
<rectangle x1="4.71805" y1="4.56565" x2="5.34035" y2="4.57835" layer="21"/>
<rectangle x1="6.29285" y1="4.56565" x2="7.14375" y2="4.57835" layer="21"/>
<rectangle x1="8.65505" y1="4.56565" x2="9.49325" y2="4.57835" layer="21"/>
<rectangle x1="-0.00635" y1="4.57835" x2="0.84455" y2="4.59105" layer="21"/>
<rectangle x1="2.92735" y1="4.57835" x2="3.09245" y2="4.59105" layer="21"/>
<rectangle x1="3.56235" y1="4.57835" x2="3.71475" y2="4.59105" layer="21"/>
<rectangle x1="4.73075" y1="4.57835" x2="5.34035" y2="4.59105" layer="21"/>
<rectangle x1="6.29285" y1="4.57835" x2="7.14375" y2="4.59105" layer="21"/>
<rectangle x1="8.65505" y1="4.57835" x2="9.49325" y2="4.59105" layer="21"/>
<rectangle x1="-0.00635" y1="4.59105" x2="0.84455" y2="4.60375" layer="21"/>
<rectangle x1="2.92735" y1="4.59105" x2="3.10515" y2="4.60375" layer="21"/>
<rectangle x1="3.56235" y1="4.59105" x2="3.71475" y2="4.60375" layer="21"/>
<rectangle x1="4.73075" y1="4.59105" x2="5.34035" y2="4.60375" layer="21"/>
<rectangle x1="6.29285" y1="4.59105" x2="7.14375" y2="4.60375" layer="21"/>
<rectangle x1="8.65505" y1="4.59105" x2="9.49325" y2="4.60375" layer="21"/>
<rectangle x1="-0.00635" y1="4.60375" x2="0.84455" y2="4.61645" layer="21"/>
<rectangle x1="2.92735" y1="4.60375" x2="3.10515" y2="4.61645" layer="21"/>
<rectangle x1="3.57505" y1="4.60375" x2="3.71475" y2="4.61645" layer="21"/>
<rectangle x1="4.18465" y1="4.60375" x2="4.19735" y2="4.61645" layer="21"/>
<rectangle x1="4.73075" y1="4.60375" x2="5.34035" y2="4.61645" layer="21"/>
<rectangle x1="6.29285" y1="4.60375" x2="7.14375" y2="4.61645" layer="21"/>
<rectangle x1="8.65505" y1="4.60375" x2="9.49325" y2="4.61645" layer="21"/>
<rectangle x1="-0.00635" y1="4.61645" x2="0.84455" y2="4.62915" layer="21"/>
<rectangle x1="2.92735" y1="4.61645" x2="3.10515" y2="4.62915" layer="21"/>
<rectangle x1="3.57505" y1="4.61645" x2="3.71475" y2="4.62915" layer="21"/>
<rectangle x1="4.18465" y1="4.61645" x2="4.19735" y2="4.62915" layer="21"/>
<rectangle x1="4.73075" y1="4.61645" x2="5.34035" y2="4.62915" layer="21"/>
<rectangle x1="6.29285" y1="4.61645" x2="7.14375" y2="4.62915" layer="21"/>
<rectangle x1="8.65505" y1="4.61645" x2="9.49325" y2="4.62915" layer="21"/>
<rectangle x1="-0.00635" y1="4.62915" x2="0.84455" y2="4.64185" layer="21"/>
<rectangle x1="2.92735" y1="4.62915" x2="3.10515" y2="4.64185" layer="21"/>
<rectangle x1="3.57505" y1="4.62915" x2="3.71475" y2="4.64185" layer="21"/>
<rectangle x1="4.18465" y1="4.62915" x2="4.19735" y2="4.64185" layer="21"/>
<rectangle x1="4.73075" y1="4.62915" x2="5.34035" y2="4.64185" layer="21"/>
<rectangle x1="6.29285" y1="4.62915" x2="7.14375" y2="4.64185" layer="21"/>
<rectangle x1="8.65505" y1="4.62915" x2="9.49325" y2="4.64185" layer="21"/>
<rectangle x1="-0.00635" y1="4.64185" x2="0.84455" y2="4.65455" layer="21"/>
<rectangle x1="2.92735" y1="4.64185" x2="3.11785" y2="4.65455" layer="21"/>
<rectangle x1="3.57505" y1="4.64185" x2="3.70205" y2="4.65455" layer="21"/>
<rectangle x1="4.18465" y1="4.64185" x2="4.21005" y2="4.65455" layer="21"/>
<rectangle x1="4.73075" y1="4.64185" x2="5.34035" y2="4.65455" layer="21"/>
<rectangle x1="6.29285" y1="4.64185" x2="7.14375" y2="4.65455" layer="21"/>
<rectangle x1="8.65505" y1="4.64185" x2="9.49325" y2="4.65455" layer="21"/>
<rectangle x1="-0.00635" y1="4.65455" x2="0.84455" y2="4.66725" layer="21"/>
<rectangle x1="2.92735" y1="4.65455" x2="3.11785" y2="4.66725" layer="21"/>
<rectangle x1="3.57505" y1="4.65455" x2="3.70205" y2="4.66725" layer="21"/>
<rectangle x1="4.17195" y1="4.65455" x2="4.21005" y2="4.66725" layer="21"/>
<rectangle x1="4.73075" y1="4.65455" x2="5.34035" y2="4.66725" layer="21"/>
<rectangle x1="6.29285" y1="4.65455" x2="7.14375" y2="4.66725" layer="21"/>
<rectangle x1="8.65505" y1="4.65455" x2="9.49325" y2="4.66725" layer="21"/>
<rectangle x1="-0.00635" y1="4.66725" x2="0.84455" y2="4.67995" layer="21"/>
<rectangle x1="2.92735" y1="4.66725" x2="3.11785" y2="4.67995" layer="21"/>
<rectangle x1="3.58775" y1="4.66725" x2="3.70205" y2="4.67995" layer="21"/>
<rectangle x1="4.17195" y1="4.66725" x2="4.21005" y2="4.67995" layer="21"/>
<rectangle x1="4.73075" y1="4.66725" x2="5.34035" y2="4.67995" layer="21"/>
<rectangle x1="6.29285" y1="4.66725" x2="7.14375" y2="4.67995" layer="21"/>
<rectangle x1="8.65505" y1="4.66725" x2="9.49325" y2="4.67995" layer="21"/>
<rectangle x1="-0.00635" y1="4.67995" x2="0.84455" y2="4.69265" layer="21"/>
<rectangle x1="2.92735" y1="4.67995" x2="3.11785" y2="4.69265" layer="21"/>
<rectangle x1="3.58775" y1="4.67995" x2="3.70205" y2="4.69265" layer="21"/>
<rectangle x1="4.17195" y1="4.67995" x2="4.21005" y2="4.69265" layer="21"/>
<rectangle x1="4.73075" y1="4.67995" x2="5.34035" y2="4.69265" layer="21"/>
<rectangle x1="6.29285" y1="4.67995" x2="7.14375" y2="4.69265" layer="21"/>
<rectangle x1="8.65505" y1="4.67995" x2="9.49325" y2="4.69265" layer="21"/>
<rectangle x1="-0.00635" y1="4.69265" x2="0.84455" y2="4.70535" layer="21"/>
<rectangle x1="2.92735" y1="4.69265" x2="3.13055" y2="4.70535" layer="21"/>
<rectangle x1="3.58775" y1="4.69265" x2="3.70205" y2="4.70535" layer="21"/>
<rectangle x1="4.17195" y1="4.69265" x2="4.21005" y2="4.70535" layer="21"/>
<rectangle x1="4.74345" y1="4.69265" x2="5.32765" y2="4.70535" layer="21"/>
<rectangle x1="6.29285" y1="4.69265" x2="7.14375" y2="4.70535" layer="21"/>
<rectangle x1="8.65505" y1="4.69265" x2="9.49325" y2="4.70535" layer="21"/>
<rectangle x1="-0.00635" y1="4.70535" x2="0.84455" y2="4.71805" layer="21"/>
<rectangle x1="2.92735" y1="4.70535" x2="3.13055" y2="4.71805" layer="21"/>
<rectangle x1="3.58775" y1="4.70535" x2="3.68935" y2="4.71805" layer="21"/>
<rectangle x1="4.15925" y1="4.70535" x2="4.21005" y2="4.71805" layer="21"/>
<rectangle x1="4.74345" y1="4.70535" x2="5.32765" y2="4.71805" layer="21"/>
<rectangle x1="6.29285" y1="4.70535" x2="7.14375" y2="4.71805" layer="21"/>
<rectangle x1="8.65505" y1="4.70535" x2="9.49325" y2="4.71805" layer="21"/>
<rectangle x1="-0.00635" y1="4.71805" x2="0.84455" y2="4.73075" layer="21"/>
<rectangle x1="2.92735" y1="4.71805" x2="3.13055" y2="4.73075" layer="21"/>
<rectangle x1="3.58775" y1="4.71805" x2="3.68935" y2="4.73075" layer="21"/>
<rectangle x1="4.15925" y1="4.71805" x2="4.21005" y2="4.73075" layer="21"/>
<rectangle x1="4.74345" y1="4.71805" x2="5.32765" y2="4.73075" layer="21"/>
<rectangle x1="6.29285" y1="4.71805" x2="7.14375" y2="4.73075" layer="21"/>
<rectangle x1="8.65505" y1="4.71805" x2="9.49325" y2="4.73075" layer="21"/>
<rectangle x1="-0.00635" y1="4.73075" x2="0.84455" y2="4.74345" layer="21"/>
<rectangle x1="2.92735" y1="4.73075" x2="3.13055" y2="4.74345" layer="21"/>
<rectangle x1="3.60045" y1="4.73075" x2="3.68935" y2="4.74345" layer="21"/>
<rectangle x1="4.15925" y1="4.73075" x2="4.21005" y2="4.74345" layer="21"/>
<rectangle x1="4.74345" y1="4.73075" x2="5.32765" y2="4.74345" layer="21"/>
<rectangle x1="6.29285" y1="4.73075" x2="7.14375" y2="4.74345" layer="21"/>
<rectangle x1="8.65505" y1="4.73075" x2="9.49325" y2="4.74345" layer="21"/>
<rectangle x1="-0.00635" y1="4.74345" x2="2.26695" y2="4.75615" layer="21"/>
<rectangle x1="2.92735" y1="4.74345" x2="3.14325" y2="4.75615" layer="21"/>
<rectangle x1="3.60045" y1="4.74345" x2="3.68935" y2="4.75615" layer="21"/>
<rectangle x1="4.15925" y1="4.74345" x2="4.21005" y2="4.75615" layer="21"/>
<rectangle x1="4.74345" y1="4.74345" x2="5.32765" y2="4.75615" layer="21"/>
<rectangle x1="6.29285" y1="4.74345" x2="7.14375" y2="4.75615" layer="21"/>
<rectangle x1="8.65505" y1="4.74345" x2="9.49325" y2="4.75615" layer="21"/>
<rectangle x1="-0.00635" y1="4.75615" x2="2.26695" y2="4.76885" layer="21"/>
<rectangle x1="2.92735" y1="4.75615" x2="3.14325" y2="4.76885" layer="21"/>
<rectangle x1="3.60045" y1="4.75615" x2="3.67665" y2="4.76885" layer="21"/>
<rectangle x1="4.14655" y1="4.75615" x2="4.22275" y2="4.76885" layer="21"/>
<rectangle x1="4.74345" y1="4.75615" x2="5.32765" y2="4.76885" layer="21"/>
<rectangle x1="6.29285" y1="4.75615" x2="7.14375" y2="4.76885" layer="21"/>
<rectangle x1="8.65505" y1="4.75615" x2="9.49325" y2="4.76885" layer="21"/>
<rectangle x1="-0.00635" y1="4.76885" x2="2.26695" y2="4.78155" layer="21"/>
<rectangle x1="2.92735" y1="4.76885" x2="3.14325" y2="4.78155" layer="21"/>
<rectangle x1="3.60045" y1="4.76885" x2="3.67665" y2="4.78155" layer="21"/>
<rectangle x1="4.14655" y1="4.76885" x2="4.22275" y2="4.78155" layer="21"/>
<rectangle x1="4.75615" y1="4.76885" x2="5.32765" y2="4.78155" layer="21"/>
<rectangle x1="6.29285" y1="4.76885" x2="7.14375" y2="4.78155" layer="21"/>
<rectangle x1="8.65505" y1="4.76885" x2="9.49325" y2="4.78155" layer="21"/>
<rectangle x1="-0.00635" y1="4.78155" x2="2.26695" y2="4.79425" layer="21"/>
<rectangle x1="2.92735" y1="4.78155" x2="3.15595" y2="4.79425" layer="21"/>
<rectangle x1="3.61315" y1="4.78155" x2="3.67665" y2="4.79425" layer="21"/>
<rectangle x1="4.14655" y1="4.78155" x2="4.22275" y2="4.79425" layer="21"/>
<rectangle x1="4.75615" y1="4.78155" x2="5.32765" y2="4.79425" layer="21"/>
<rectangle x1="6.29285" y1="4.78155" x2="7.14375" y2="4.79425" layer="21"/>
<rectangle x1="8.65505" y1="4.78155" x2="9.49325" y2="4.79425" layer="21"/>
<rectangle x1="-0.00635" y1="4.79425" x2="2.26695" y2="4.80695" layer="21"/>
<rectangle x1="2.92735" y1="4.79425" x2="3.15595" y2="4.80695" layer="21"/>
<rectangle x1="3.61315" y1="4.79425" x2="3.67665" y2="4.80695" layer="21"/>
<rectangle x1="4.14655" y1="4.79425" x2="4.22275" y2="4.80695" layer="21"/>
<rectangle x1="4.75615" y1="4.79425" x2="5.31495" y2="4.80695" layer="21"/>
<rectangle x1="6.29285" y1="4.79425" x2="7.14375" y2="4.80695" layer="21"/>
<rectangle x1="8.65505" y1="4.79425" x2="9.49325" y2="4.80695" layer="21"/>
<rectangle x1="-0.00635" y1="4.80695" x2="2.26695" y2="4.81965" layer="21"/>
<rectangle x1="2.92735" y1="4.80695" x2="3.15595" y2="4.81965" layer="21"/>
<rectangle x1="3.61315" y1="4.80695" x2="3.67665" y2="4.81965" layer="21"/>
<rectangle x1="4.13385" y1="4.80695" x2="4.22275" y2="4.81965" layer="21"/>
<rectangle x1="4.75615" y1="4.80695" x2="5.31495" y2="4.81965" layer="21"/>
<rectangle x1="6.29285" y1="4.80695" x2="7.14375" y2="4.81965" layer="21"/>
<rectangle x1="8.65505" y1="4.80695" x2="9.49325" y2="4.81965" layer="21"/>
<rectangle x1="-0.00635" y1="4.81965" x2="2.26695" y2="4.83235" layer="21"/>
<rectangle x1="2.92735" y1="4.81965" x2="3.15595" y2="4.83235" layer="21"/>
<rectangle x1="3.61315" y1="4.81965" x2="3.66395" y2="4.83235" layer="21"/>
<rectangle x1="4.13385" y1="4.81965" x2="4.22275" y2="4.83235" layer="21"/>
<rectangle x1="4.75615" y1="4.81965" x2="5.31495" y2="4.83235" layer="21"/>
<rectangle x1="6.29285" y1="4.81965" x2="7.14375" y2="4.83235" layer="21"/>
<rectangle x1="8.65505" y1="4.81965" x2="9.49325" y2="4.83235" layer="21"/>
<rectangle x1="-0.00635" y1="4.83235" x2="2.26695" y2="4.84505" layer="21"/>
<rectangle x1="2.92735" y1="4.83235" x2="3.16865" y2="4.84505" layer="21"/>
<rectangle x1="3.61315" y1="4.83235" x2="3.66395" y2="4.84505" layer="21"/>
<rectangle x1="4.13385" y1="4.83235" x2="4.23545" y2="4.84505" layer="21"/>
<rectangle x1="4.76885" y1="4.83235" x2="5.31495" y2="4.84505" layer="21"/>
<rectangle x1="6.29285" y1="4.83235" x2="7.14375" y2="4.84505" layer="21"/>
<rectangle x1="8.65505" y1="4.83235" x2="9.49325" y2="4.84505" layer="21"/>
<rectangle x1="-0.00635" y1="4.84505" x2="2.26695" y2="4.85775" layer="21"/>
<rectangle x1="2.92735" y1="4.84505" x2="3.16865" y2="4.85775" layer="21"/>
<rectangle x1="3.62585" y1="4.84505" x2="3.66395" y2="4.85775" layer="21"/>
<rectangle x1="4.13385" y1="4.84505" x2="4.23545" y2="4.85775" layer="21"/>
<rectangle x1="4.76885" y1="4.84505" x2="5.30225" y2="4.85775" layer="21"/>
<rectangle x1="6.29285" y1="4.84505" x2="7.14375" y2="4.85775" layer="21"/>
<rectangle x1="8.65505" y1="4.84505" x2="9.49325" y2="4.85775" layer="21"/>
<rectangle x1="-0.00635" y1="4.85775" x2="2.26695" y2="4.87045" layer="21"/>
<rectangle x1="2.92735" y1="4.85775" x2="3.16865" y2="4.87045" layer="21"/>
<rectangle x1="3.62585" y1="4.85775" x2="3.66395" y2="4.87045" layer="21"/>
<rectangle x1="4.12115" y1="4.85775" x2="4.23545" y2="4.87045" layer="21"/>
<rectangle x1="4.76885" y1="4.85775" x2="5.30225" y2="4.87045" layer="21"/>
<rectangle x1="6.29285" y1="4.85775" x2="7.14375" y2="4.87045" layer="21"/>
<rectangle x1="8.65505" y1="4.85775" x2="9.49325" y2="4.87045" layer="21"/>
<rectangle x1="-0.00635" y1="4.87045" x2="2.26695" y2="4.88315" layer="21"/>
<rectangle x1="2.92735" y1="4.87045" x2="3.16865" y2="4.88315" layer="21"/>
<rectangle x1="3.62585" y1="4.87045" x2="3.66395" y2="4.88315" layer="21"/>
<rectangle x1="4.12115" y1="4.87045" x2="4.23545" y2="4.88315" layer="21"/>
<rectangle x1="4.78155" y1="4.87045" x2="5.30225" y2="4.88315" layer="21"/>
<rectangle x1="6.29285" y1="4.87045" x2="7.14375" y2="4.88315" layer="21"/>
<rectangle x1="8.65505" y1="4.87045" x2="9.49325" y2="4.88315" layer="21"/>
<rectangle x1="-0.00635" y1="4.88315" x2="2.26695" y2="4.89585" layer="21"/>
<rectangle x1="2.92735" y1="4.88315" x2="3.18135" y2="4.89585" layer="21"/>
<rectangle x1="3.62585" y1="4.88315" x2="3.65125" y2="4.89585" layer="21"/>
<rectangle x1="4.12115" y1="4.88315" x2="4.24815" y2="4.89585" layer="21"/>
<rectangle x1="4.78155" y1="4.88315" x2="5.30225" y2="4.89585" layer="21"/>
<rectangle x1="6.29285" y1="4.88315" x2="7.14375" y2="4.89585" layer="21"/>
<rectangle x1="8.65505" y1="4.88315" x2="9.49325" y2="4.89585" layer="21"/>
<rectangle x1="-0.00635" y1="4.89585" x2="2.26695" y2="4.90855" layer="21"/>
<rectangle x1="2.92735" y1="4.89585" x2="3.18135" y2="4.90855" layer="21"/>
<rectangle x1="3.62585" y1="4.89585" x2="3.65125" y2="4.90855" layer="21"/>
<rectangle x1="4.12115" y1="4.89585" x2="4.24815" y2="4.90855" layer="21"/>
<rectangle x1="4.78155" y1="4.89585" x2="5.28955" y2="4.90855" layer="21"/>
<rectangle x1="6.29285" y1="4.89585" x2="7.14375" y2="4.90855" layer="21"/>
<rectangle x1="8.65505" y1="4.89585" x2="9.49325" y2="4.90855" layer="21"/>
<rectangle x1="-0.00635" y1="4.90855" x2="2.26695" y2="4.92125" layer="21"/>
<rectangle x1="2.92735" y1="4.90855" x2="3.18135" y2="4.92125" layer="21"/>
<rectangle x1="3.63855" y1="4.90855" x2="3.65125" y2="4.92125" layer="21"/>
<rectangle x1="4.10845" y1="4.90855" x2="4.24815" y2="4.92125" layer="21"/>
<rectangle x1="4.79425" y1="4.90855" x2="5.28955" y2="4.92125" layer="21"/>
<rectangle x1="6.29285" y1="4.90855" x2="7.14375" y2="4.92125" layer="21"/>
<rectangle x1="8.65505" y1="4.90855" x2="9.49325" y2="4.92125" layer="21"/>
<rectangle x1="-0.00635" y1="4.92125" x2="2.26695" y2="4.93395" layer="21"/>
<rectangle x1="2.92735" y1="4.92125" x2="3.18135" y2="4.93395" layer="21"/>
<rectangle x1="3.63855" y1="4.92125" x2="3.65125" y2="4.93395" layer="21"/>
<rectangle x1="4.10845" y1="4.92125" x2="4.24815" y2="4.93395" layer="21"/>
<rectangle x1="4.79425" y1="4.92125" x2="5.28955" y2="4.93395" layer="21"/>
<rectangle x1="6.29285" y1="4.92125" x2="7.14375" y2="4.93395" layer="21"/>
<rectangle x1="8.65505" y1="4.92125" x2="9.49325" y2="4.93395" layer="21"/>
<rectangle x1="-0.00635" y1="4.93395" x2="2.26695" y2="4.94665" layer="21"/>
<rectangle x1="2.92735" y1="4.93395" x2="3.19405" y2="4.94665" layer="21"/>
<rectangle x1="3.63855" y1="4.93395" x2="3.65125" y2="4.94665" layer="21"/>
<rectangle x1="4.10845" y1="4.93395" x2="4.26085" y2="4.94665" layer="21"/>
<rectangle x1="4.79425" y1="4.93395" x2="5.27685" y2="4.94665" layer="21"/>
<rectangle x1="6.29285" y1="4.93395" x2="7.14375" y2="4.94665" layer="21"/>
<rectangle x1="8.65505" y1="4.93395" x2="9.49325" y2="4.94665" layer="21"/>
<rectangle x1="-0.00635" y1="4.94665" x2="2.26695" y2="4.95935" layer="21"/>
<rectangle x1="2.92735" y1="4.94665" x2="3.19405" y2="4.95935" layer="21"/>
<rectangle x1="4.09575" y1="4.94665" x2="4.26085" y2="4.95935" layer="21"/>
<rectangle x1="4.80695" y1="4.94665" x2="5.27685" y2="4.95935" layer="21"/>
<rectangle x1="6.29285" y1="4.94665" x2="7.14375" y2="4.95935" layer="21"/>
<rectangle x1="8.65505" y1="4.94665" x2="9.49325" y2="4.95935" layer="21"/>
<rectangle x1="-0.00635" y1="4.95935" x2="2.26695" y2="4.97205" layer="21"/>
<rectangle x1="2.92735" y1="4.95935" x2="3.19405" y2="4.97205" layer="21"/>
<rectangle x1="4.09575" y1="4.95935" x2="4.26085" y2="4.97205" layer="21"/>
<rectangle x1="4.80695" y1="4.95935" x2="5.27685" y2="4.97205" layer="21"/>
<rectangle x1="6.29285" y1="4.95935" x2="7.14375" y2="4.97205" layer="21"/>
<rectangle x1="8.65505" y1="4.95935" x2="9.49325" y2="4.97205" layer="21"/>
<rectangle x1="-0.00635" y1="4.97205" x2="2.26695" y2="4.98475" layer="21"/>
<rectangle x1="2.92735" y1="4.97205" x2="3.19405" y2="4.98475" layer="21"/>
<rectangle x1="4.09575" y1="4.97205" x2="4.27355" y2="4.98475" layer="21"/>
<rectangle x1="4.81965" y1="4.97205" x2="5.26415" y2="4.98475" layer="21"/>
<rectangle x1="6.29285" y1="4.97205" x2="7.14375" y2="4.98475" layer="21"/>
<rectangle x1="8.65505" y1="4.97205" x2="9.49325" y2="4.98475" layer="21"/>
<rectangle x1="-0.00635" y1="4.98475" x2="2.26695" y2="4.99745" layer="21"/>
<rectangle x1="2.92735" y1="4.98475" x2="3.20675" y2="4.99745" layer="21"/>
<rectangle x1="4.09575" y1="4.98475" x2="4.27355" y2="4.99745" layer="21"/>
<rectangle x1="4.81965" y1="4.98475" x2="5.26415" y2="4.99745" layer="21"/>
<rectangle x1="6.29285" y1="4.98475" x2="7.14375" y2="4.99745" layer="21"/>
<rectangle x1="8.65505" y1="4.98475" x2="9.49325" y2="4.99745" layer="21"/>
<rectangle x1="-0.00635" y1="4.99745" x2="2.26695" y2="5.01015" layer="21"/>
<rectangle x1="2.92735" y1="4.99745" x2="3.20675" y2="5.01015" layer="21"/>
<rectangle x1="4.08305" y1="4.99745" x2="4.27355" y2="5.01015" layer="21"/>
<rectangle x1="4.83235" y1="4.99745" x2="5.25145" y2="5.01015" layer="21"/>
<rectangle x1="6.29285" y1="4.99745" x2="7.14375" y2="5.01015" layer="21"/>
<rectangle x1="8.65505" y1="4.99745" x2="9.49325" y2="5.01015" layer="21"/>
<rectangle x1="-0.00635" y1="5.01015" x2="2.26695" y2="5.02285" layer="21"/>
<rectangle x1="2.92735" y1="5.01015" x2="3.20675" y2="5.02285" layer="21"/>
<rectangle x1="4.08305" y1="5.01015" x2="4.28625" y2="5.02285" layer="21"/>
<rectangle x1="4.84505" y1="5.01015" x2="5.23875" y2="5.02285" layer="21"/>
<rectangle x1="6.29285" y1="5.01015" x2="7.14375" y2="5.02285" layer="21"/>
<rectangle x1="8.65505" y1="5.01015" x2="9.49325" y2="5.02285" layer="21"/>
<rectangle x1="-0.00635" y1="5.02285" x2="2.26695" y2="5.03555" layer="21"/>
<rectangle x1="2.92735" y1="5.02285" x2="3.20675" y2="5.03555" layer="21"/>
<rectangle x1="4.08305" y1="5.02285" x2="4.28625" y2="5.03555" layer="21"/>
<rectangle x1="4.85775" y1="5.02285" x2="5.22605" y2="5.03555" layer="21"/>
<rectangle x1="6.29285" y1="5.02285" x2="7.14375" y2="5.03555" layer="21"/>
<rectangle x1="8.65505" y1="5.02285" x2="9.49325" y2="5.03555" layer="21"/>
<rectangle x1="-0.00635" y1="5.03555" x2="2.26695" y2="5.04825" layer="21"/>
<rectangle x1="2.92735" y1="5.03555" x2="3.21945" y2="5.04825" layer="21"/>
<rectangle x1="4.08305" y1="5.03555" x2="4.28625" y2="5.04825" layer="21"/>
<rectangle x1="4.87045" y1="5.03555" x2="5.22605" y2="5.04825" layer="21"/>
<rectangle x1="6.29285" y1="5.03555" x2="7.14375" y2="5.04825" layer="21"/>
<rectangle x1="8.65505" y1="5.03555" x2="9.49325" y2="5.04825" layer="21"/>
<rectangle x1="-0.00635" y1="5.04825" x2="2.26695" y2="5.06095" layer="21"/>
<rectangle x1="2.92735" y1="5.04825" x2="3.21945" y2="5.06095" layer="21"/>
<rectangle x1="4.07035" y1="5.04825" x2="4.29895" y2="5.06095" layer="21"/>
<rectangle x1="4.88315" y1="5.04825" x2="5.20065" y2="5.06095" layer="21"/>
<rectangle x1="6.29285" y1="5.04825" x2="7.14375" y2="5.06095" layer="21"/>
<rectangle x1="8.65505" y1="5.04825" x2="9.49325" y2="5.06095" layer="21"/>
<rectangle x1="-0.00635" y1="5.06095" x2="2.26695" y2="5.07365" layer="21"/>
<rectangle x1="2.92735" y1="5.06095" x2="3.21945" y2="5.07365" layer="21"/>
<rectangle x1="4.07035" y1="5.06095" x2="4.29895" y2="5.07365" layer="21"/>
<rectangle x1="4.89585" y1="5.06095" x2="5.18795" y2="5.07365" layer="21"/>
<rectangle x1="6.29285" y1="5.06095" x2="7.14375" y2="5.07365" layer="21"/>
<rectangle x1="8.65505" y1="5.06095" x2="9.49325" y2="5.07365" layer="21"/>
<rectangle x1="-0.00635" y1="5.07365" x2="2.26695" y2="5.08635" layer="21"/>
<rectangle x1="2.92735" y1="5.07365" x2="3.21945" y2="5.08635" layer="21"/>
<rectangle x1="4.07035" y1="5.07365" x2="4.31165" y2="5.08635" layer="21"/>
<rectangle x1="4.92125" y1="5.07365" x2="5.16255" y2="5.08635" layer="21"/>
<rectangle x1="6.29285" y1="5.07365" x2="7.14375" y2="5.08635" layer="21"/>
<rectangle x1="8.65505" y1="5.07365" x2="9.49325" y2="5.08635" layer="21"/>
<rectangle x1="-0.00635" y1="5.08635" x2="2.26695" y2="5.09905" layer="21"/>
<rectangle x1="2.92735" y1="5.08635" x2="3.23215" y2="5.09905" layer="21"/>
<rectangle x1="4.07035" y1="5.08635" x2="4.31165" y2="5.09905" layer="21"/>
<rectangle x1="4.94665" y1="5.08635" x2="5.12445" y2="5.09905" layer="21"/>
<rectangle x1="6.29285" y1="5.08635" x2="7.14375" y2="5.09905" layer="21"/>
<rectangle x1="8.65505" y1="5.08635" x2="9.49325" y2="5.09905" layer="21"/>
<rectangle x1="-0.00635" y1="5.09905" x2="2.26695" y2="5.11175" layer="21"/>
<rectangle x1="2.92735" y1="5.09905" x2="3.23215" y2="5.11175" layer="21"/>
<rectangle x1="4.05765" y1="5.09905" x2="4.32435" y2="5.11175" layer="21"/>
<rectangle x1="5.01015" y1="5.09905" x2="5.03555" y2="5.11175" layer="21"/>
<rectangle x1="6.29285" y1="5.09905" x2="7.14375" y2="5.11175" layer="21"/>
<rectangle x1="8.65505" y1="5.09905" x2="9.49325" y2="5.11175" layer="21"/>
<rectangle x1="-0.00635" y1="5.11175" x2="2.26695" y2="5.12445" layer="21"/>
<rectangle x1="2.92735" y1="5.11175" x2="3.23215" y2="5.12445" layer="21"/>
<rectangle x1="4.05765" y1="5.11175" x2="4.32435" y2="5.12445" layer="21"/>
<rectangle x1="6.29285" y1="5.11175" x2="7.14375" y2="5.12445" layer="21"/>
<rectangle x1="8.65505" y1="5.11175" x2="9.49325" y2="5.12445" layer="21"/>
<rectangle x1="-0.00635" y1="5.12445" x2="2.26695" y2="5.13715" layer="21"/>
<rectangle x1="2.92735" y1="5.12445" x2="3.23215" y2="5.13715" layer="21"/>
<rectangle x1="4.05765" y1="5.12445" x2="4.32435" y2="5.13715" layer="21"/>
<rectangle x1="6.29285" y1="5.12445" x2="7.14375" y2="5.13715" layer="21"/>
<rectangle x1="8.65505" y1="5.12445" x2="9.49325" y2="5.13715" layer="21"/>
<rectangle x1="-0.00635" y1="5.13715" x2="2.26695" y2="5.14985" layer="21"/>
<rectangle x1="2.92735" y1="5.13715" x2="3.24485" y2="5.14985" layer="21"/>
<rectangle x1="4.05765" y1="5.13715" x2="4.33705" y2="5.14985" layer="21"/>
<rectangle x1="6.29285" y1="5.13715" x2="7.14375" y2="5.14985" layer="21"/>
<rectangle x1="8.65505" y1="5.13715" x2="9.49325" y2="5.14985" layer="21"/>
<rectangle x1="-0.00635" y1="5.14985" x2="2.26695" y2="5.16255" layer="21"/>
<rectangle x1="2.92735" y1="5.14985" x2="3.24485" y2="5.16255" layer="21"/>
<rectangle x1="4.04495" y1="5.14985" x2="4.34975" y2="5.16255" layer="21"/>
<rectangle x1="6.29285" y1="5.14985" x2="7.14375" y2="5.16255" layer="21"/>
<rectangle x1="8.65505" y1="5.14985" x2="9.49325" y2="5.16255" layer="21"/>
<rectangle x1="-0.00635" y1="5.16255" x2="2.26695" y2="5.17525" layer="21"/>
<rectangle x1="2.92735" y1="5.16255" x2="3.24485" y2="5.17525" layer="21"/>
<rectangle x1="4.04495" y1="5.16255" x2="4.34975" y2="5.17525" layer="21"/>
<rectangle x1="6.29285" y1="5.16255" x2="7.14375" y2="5.17525" layer="21"/>
<rectangle x1="8.65505" y1="5.16255" x2="9.49325" y2="5.17525" layer="21"/>
<rectangle x1="-0.00635" y1="5.17525" x2="2.26695" y2="5.18795" layer="21"/>
<rectangle x1="2.92735" y1="5.17525" x2="3.24485" y2="5.18795" layer="21"/>
<rectangle x1="4.04495" y1="5.17525" x2="4.36245" y2="5.18795" layer="21"/>
<rectangle x1="6.29285" y1="5.17525" x2="7.14375" y2="5.18795" layer="21"/>
<rectangle x1="8.65505" y1="5.17525" x2="9.49325" y2="5.18795" layer="21"/>
<rectangle x1="-0.00635" y1="5.18795" x2="2.26695" y2="5.20065" layer="21"/>
<rectangle x1="2.92735" y1="5.18795" x2="3.25755" y2="5.20065" layer="21"/>
<rectangle x1="4.04495" y1="5.18795" x2="4.36245" y2="5.20065" layer="21"/>
<rectangle x1="6.29285" y1="5.18795" x2="7.14375" y2="5.20065" layer="21"/>
<rectangle x1="8.65505" y1="5.18795" x2="9.49325" y2="5.20065" layer="21"/>
<rectangle x1="-0.00635" y1="5.20065" x2="2.26695" y2="5.21335" layer="21"/>
<rectangle x1="2.92735" y1="5.20065" x2="3.25755" y2="5.21335" layer="21"/>
<rectangle x1="4.03225" y1="5.20065" x2="4.37515" y2="5.21335" layer="21"/>
<rectangle x1="6.29285" y1="5.20065" x2="7.14375" y2="5.21335" layer="21"/>
<rectangle x1="8.65505" y1="5.20065" x2="9.49325" y2="5.21335" layer="21"/>
<rectangle x1="-0.00635" y1="5.21335" x2="2.26695" y2="5.22605" layer="21"/>
<rectangle x1="2.92735" y1="5.21335" x2="3.25755" y2="5.22605" layer="21"/>
<rectangle x1="4.03225" y1="5.21335" x2="4.38785" y2="5.22605" layer="21"/>
<rectangle x1="6.29285" y1="5.21335" x2="7.14375" y2="5.22605" layer="21"/>
<rectangle x1="8.65505" y1="5.21335" x2="9.49325" y2="5.22605" layer="21"/>
<rectangle x1="-0.00635" y1="5.22605" x2="2.26695" y2="5.23875" layer="21"/>
<rectangle x1="2.92735" y1="5.22605" x2="3.25755" y2="5.23875" layer="21"/>
<rectangle x1="4.03225" y1="5.22605" x2="4.38785" y2="5.23875" layer="21"/>
<rectangle x1="6.29285" y1="5.22605" x2="7.14375" y2="5.23875" layer="21"/>
<rectangle x1="8.65505" y1="5.22605" x2="9.49325" y2="5.23875" layer="21"/>
<rectangle x1="-0.00635" y1="5.23875" x2="2.26695" y2="5.25145" layer="21"/>
<rectangle x1="2.92735" y1="5.23875" x2="3.27025" y2="5.25145" layer="21"/>
<rectangle x1="4.01955" y1="5.23875" x2="4.40055" y2="5.25145" layer="21"/>
<rectangle x1="6.29285" y1="5.23875" x2="7.14375" y2="5.25145" layer="21"/>
<rectangle x1="8.65505" y1="5.23875" x2="9.49325" y2="5.25145" layer="21"/>
<rectangle x1="-0.00635" y1="5.25145" x2="2.26695" y2="5.26415" layer="21"/>
<rectangle x1="2.92735" y1="5.25145" x2="3.27025" y2="5.26415" layer="21"/>
<rectangle x1="4.01955" y1="5.25145" x2="4.41325" y2="5.26415" layer="21"/>
<rectangle x1="6.29285" y1="5.25145" x2="7.14375" y2="5.26415" layer="21"/>
<rectangle x1="8.65505" y1="5.25145" x2="9.49325" y2="5.26415" layer="21"/>
<rectangle x1="-0.00635" y1="5.26415" x2="2.26695" y2="5.27685" layer="21"/>
<rectangle x1="2.92735" y1="5.26415" x2="3.27025" y2="5.27685" layer="21"/>
<rectangle x1="4.01955" y1="5.26415" x2="4.42595" y2="5.27685" layer="21"/>
<rectangle x1="6.29285" y1="5.26415" x2="7.14375" y2="5.27685" layer="21"/>
<rectangle x1="8.65505" y1="5.26415" x2="9.49325" y2="5.27685" layer="21"/>
<rectangle x1="-0.00635" y1="5.27685" x2="2.26695" y2="5.28955" layer="21"/>
<rectangle x1="2.92735" y1="5.27685" x2="3.27025" y2="5.28955" layer="21"/>
<rectangle x1="4.01955" y1="5.27685" x2="4.42595" y2="5.28955" layer="21"/>
<rectangle x1="6.29285" y1="5.27685" x2="7.14375" y2="5.28955" layer="21"/>
<rectangle x1="8.65505" y1="5.27685" x2="9.49325" y2="5.28955" layer="21"/>
<rectangle x1="-0.00635" y1="5.28955" x2="2.26695" y2="5.30225" layer="21"/>
<rectangle x1="2.92735" y1="5.28955" x2="3.28295" y2="5.30225" layer="21"/>
<rectangle x1="4.00685" y1="5.28955" x2="4.43865" y2="5.30225" layer="21"/>
<rectangle x1="6.29285" y1="5.28955" x2="7.14375" y2="5.30225" layer="21"/>
<rectangle x1="8.65505" y1="5.28955" x2="9.49325" y2="5.30225" layer="21"/>
<rectangle x1="-0.00635" y1="5.30225" x2="2.26695" y2="5.31495" layer="21"/>
<rectangle x1="2.92735" y1="5.30225" x2="3.28295" y2="5.31495" layer="21"/>
<rectangle x1="4.00685" y1="5.30225" x2="4.45135" y2="5.31495" layer="21"/>
<rectangle x1="6.29285" y1="5.30225" x2="7.14375" y2="5.31495" layer="21"/>
<rectangle x1="8.65505" y1="5.30225" x2="9.49325" y2="5.31495" layer="21"/>
<rectangle x1="-0.00635" y1="5.31495" x2="2.26695" y2="5.32765" layer="21"/>
<rectangle x1="2.92735" y1="5.31495" x2="3.28295" y2="5.32765" layer="21"/>
<rectangle x1="4.00685" y1="5.31495" x2="4.46405" y2="5.32765" layer="21"/>
<rectangle x1="6.29285" y1="5.31495" x2="7.14375" y2="5.32765" layer="21"/>
<rectangle x1="8.65505" y1="5.31495" x2="9.49325" y2="5.32765" layer="21"/>
<rectangle x1="-0.00635" y1="5.32765" x2="2.26695" y2="5.34035" layer="21"/>
<rectangle x1="2.92735" y1="5.32765" x2="3.28295" y2="5.34035" layer="21"/>
<rectangle x1="4.00685" y1="5.32765" x2="4.47675" y2="5.34035" layer="21"/>
<rectangle x1="6.29285" y1="5.32765" x2="7.14375" y2="5.34035" layer="21"/>
<rectangle x1="8.65505" y1="5.32765" x2="9.49325" y2="5.34035" layer="21"/>
<rectangle x1="-0.00635" y1="5.34035" x2="2.26695" y2="5.35305" layer="21"/>
<rectangle x1="2.92735" y1="5.34035" x2="3.29565" y2="5.35305" layer="21"/>
<rectangle x1="3.99415" y1="5.34035" x2="4.48945" y2="5.35305" layer="21"/>
<rectangle x1="6.29285" y1="5.34035" x2="7.14375" y2="5.35305" layer="21"/>
<rectangle x1="8.65505" y1="5.34035" x2="9.49325" y2="5.35305" layer="21"/>
<rectangle x1="-0.00635" y1="5.35305" x2="2.26695" y2="5.36575" layer="21"/>
<rectangle x1="2.92735" y1="5.35305" x2="3.29565" y2="5.36575" layer="21"/>
<rectangle x1="3.99415" y1="5.35305" x2="4.50215" y2="5.36575" layer="21"/>
<rectangle x1="6.29285" y1="5.35305" x2="7.14375" y2="5.36575" layer="21"/>
<rectangle x1="8.65505" y1="5.35305" x2="9.49325" y2="5.36575" layer="21"/>
<rectangle x1="-0.00635" y1="5.36575" x2="2.26695" y2="5.37845" layer="21"/>
<rectangle x1="2.92735" y1="5.36575" x2="3.29565" y2="5.37845" layer="21"/>
<rectangle x1="3.99415" y1="5.36575" x2="4.52755" y2="5.37845" layer="21"/>
<rectangle x1="6.29285" y1="5.36575" x2="7.14375" y2="5.37845" layer="21"/>
<rectangle x1="8.65505" y1="5.36575" x2="9.49325" y2="5.37845" layer="21"/>
<rectangle x1="-0.00635" y1="5.37845" x2="2.26695" y2="5.39115" layer="21"/>
<rectangle x1="2.92735" y1="5.37845" x2="3.29565" y2="5.39115" layer="21"/>
<rectangle x1="3.99415" y1="5.37845" x2="4.54025" y2="5.39115" layer="21"/>
<rectangle x1="6.29285" y1="5.37845" x2="7.14375" y2="5.39115" layer="21"/>
<rectangle x1="8.65505" y1="5.37845" x2="9.49325" y2="5.39115" layer="21"/>
<rectangle x1="-0.00635" y1="5.39115" x2="2.26695" y2="5.40385" layer="21"/>
<rectangle x1="2.92735" y1="5.39115" x2="3.30835" y2="5.40385" layer="21"/>
<rectangle x1="3.98145" y1="5.39115" x2="4.56565" y2="5.40385" layer="21"/>
<rectangle x1="6.29285" y1="5.39115" x2="7.14375" y2="5.40385" layer="21"/>
<rectangle x1="8.65505" y1="5.39115" x2="9.49325" y2="5.40385" layer="21"/>
<rectangle x1="-0.00635" y1="5.40385" x2="2.26695" y2="5.41655" layer="21"/>
<rectangle x1="2.92735" y1="5.40385" x2="3.30835" y2="5.41655" layer="21"/>
<rectangle x1="3.98145" y1="5.40385" x2="4.57835" y2="5.41655" layer="21"/>
<rectangle x1="6.29285" y1="5.40385" x2="7.14375" y2="5.41655" layer="21"/>
<rectangle x1="8.65505" y1="5.40385" x2="9.49325" y2="5.41655" layer="21"/>
<rectangle x1="-0.00635" y1="5.41655" x2="2.26695" y2="5.42925" layer="21"/>
<rectangle x1="2.92735" y1="5.41655" x2="3.30835" y2="5.42925" layer="21"/>
<rectangle x1="3.98145" y1="5.41655" x2="4.60375" y2="5.42925" layer="21"/>
<rectangle x1="6.29285" y1="5.41655" x2="7.14375" y2="5.42925" layer="21"/>
<rectangle x1="8.65505" y1="5.41655" x2="9.49325" y2="5.42925" layer="21"/>
<rectangle x1="-0.00635" y1="5.42925" x2="2.26695" y2="5.44195" layer="21"/>
<rectangle x1="2.92735" y1="5.42925" x2="3.30835" y2="5.44195" layer="21"/>
<rectangle x1="3.98145" y1="5.42925" x2="4.62915" y2="5.44195" layer="21"/>
<rectangle x1="6.29285" y1="5.42925" x2="7.14375" y2="5.44195" layer="21"/>
<rectangle x1="8.65505" y1="5.42925" x2="9.49325" y2="5.44195" layer="21"/>
<rectangle x1="-0.00635" y1="5.44195" x2="2.26695" y2="5.45465" layer="21"/>
<rectangle x1="2.92735" y1="5.44195" x2="3.30835" y2="5.45465" layer="21"/>
<rectangle x1="3.96875" y1="5.44195" x2="4.65455" y2="5.45465" layer="21"/>
<rectangle x1="6.29285" y1="5.44195" x2="7.14375" y2="5.45465" layer="21"/>
<rectangle x1="8.65505" y1="5.44195" x2="9.49325" y2="5.45465" layer="21"/>
<rectangle x1="-0.00635" y1="5.45465" x2="2.26695" y2="5.46735" layer="21"/>
<rectangle x1="2.92735" y1="5.45465" x2="3.32105" y2="5.46735" layer="21"/>
<rectangle x1="3.96875" y1="5.45465" x2="4.69265" y2="5.46735" layer="21"/>
<rectangle x1="6.29285" y1="5.45465" x2="7.14375" y2="5.46735" layer="21"/>
<rectangle x1="8.65505" y1="5.45465" x2="9.49325" y2="5.46735" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="SOIC127P700X170-9T262X351" urn="urn:adsk.eagle:package:10239960/1" locally_modified="yes" type="model">
<description>8-SOIC, 1.27 mm pitch, 7.00 mm span, 4.90 X 3.90 X 1.70 mm body, 3.51 X 2.62 mm thermal pad
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 7.00 mm span with body size 4.90 X 3.90 X 1.70 mm and thermal pad size 3.51 X 2.62 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOIC127P700X170-9T262X351"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MP1584EN">
<wire x1="-10.16" y1="12.7" x2="10.16" y2="12.7" width="0.1524" layer="94"/>
<wire x1="10.16" y1="12.7" x2="10.16" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="12.7" width="0.1524" layer="94"/>
<pin name="SW" x="15.24" y="10.16" length="middle" direction="out" rot="R180"/>
<pin name="EN" x="-15.24" y="2.54" length="middle" direction="in"/>
<pin name="COMP" x="15.24" y="-5.08" length="middle" direction="in" rot="R180"/>
<pin name="FB" x="15.24" y="2.54" length="middle" direction="in" rot="R180"/>
<pin name="BST" x="0" y="17.78" length="middle" direction="in" rot="R270"/>
<pin name="VIN" x="-15.24" y="10.16" length="middle" direction="pwr"/>
<pin name="FREQ" x="-15.24" y="-5.08" length="middle" direction="in"/>
<pin name="GND" x="0" y="-15.24" length="middle" direction="sup" rot="R90"/>
<text x="5.08" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<text x="-10.16" y="13.462" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="TIOL111_SYMBOL">
<wire x1="-12.7" y1="15.24" x2="12.7" y2="15.24" width="0.1524" layer="94"/>
<wire x1="12.7" y1="15.24" x2="12.7" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="-12.7" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="-10.16" x2="-12.7" y2="15.24" width="0.1524" layer="94"/>
<pin name="VCC_IN/OUT" x="-17.78" y="12.7" length="middle" direction="pwr"/>
<pin name="NFAULT" x="-17.78" y="7.62" length="middle" direction="out"/>
<pin name="RX" x="-17.78" y="2.54" length="middle" direction="out"/>
<pin name="TX" x="-17.78" y="-2.54" length="middle" direction="in"/>
<pin name="EN" x="-17.78" y="-7.62" length="middle" direction="in"/>
<pin name="WAKE" x="17.78" y="12.7" length="middle" direction="out" rot="R180"/>
<pin name="L+" x="17.78" y="7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="CQ" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="L-" x="17.78" y="-2.54" length="middle" direction="sup" rot="R180"/>
<pin name="ILIM_ADJ" x="17.78" y="-7.62" length="middle" direction="pas" rot="R180"/>
<text x="-7.62" y="17.78" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="M12-A_4_MALE">
<circle x="0" y="0" radius="8" width="0.8" layer="94"/>
<circle x="-3" y="3" radius="1" width="0.254" layer="94"/>
<circle x="-3" y="-3" radius="1" width="0.254" layer="94"/>
<circle x="3" y="3" radius="1" width="0.254" layer="94"/>
<circle x="3" y="-3" radius="1" width="0.254" layer="94"/>
<pin name="L+" x="12.964" y="2.492" visible="pad" length="middle" direction="pwr" rot="R180"/>
<pin name="C/Q" x="13.112" y="-2.598" visible="pad" length="middle" rot="R180"/>
<pin name="GND" x="-12.978" y="-2.492" visible="pad" length="middle" direction="sup"/>
<text x="-15.24" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-15.24" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="NC" x="-12.9592" y="2.432" visible="pad" length="middle" direction="nc"/>
<rectangle x1="-0.7" y1="5.9" x2="0.7" y2="8.3" layer="94"/>
<text x="2.158" y="4.762" size="1.4" layer="95" font="vector" ratio="14">L+</text>
<text x="-3.896" y="4.77" size="1.4" layer="95" font="vector" ratio="14">NC</text>
<text x="-4.888" y="-1.23" size="1.4" layer="95" font="vector" ratio="14">GND</text>
<text x="1.642" y="-1.23" size="1.4" layer="95" font="vector" ratio="14">C/Q</text>
</symbol>
<symbol name="FHLOGO_SYMBOL">
<text x="0" y="0" size="1.778" layer="94">FH_Aachen</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="MP1584" prefix="U">
<gates>
<gate name="0,1" symbol="MP1584EN" x="5.08" y="12.7"/>
</gates>
<devices>
<device name="SOIC8E_MPS" package="SOIC127P700X170-9T262X351">
<connects>
<connect gate="0,1" pin="BST" pad="8"/>
<connect gate="0,1" pin="COMP" pad="3"/>
<connect gate="0,1" pin="EN" pad="2"/>
<connect gate="0,1" pin="FB" pad="4"/>
<connect gate="0,1" pin="FREQ" pad="6"/>
<connect gate="0,1" pin="GND" pad="5 9"/>
<connect gate="0,1" pin="SW" pad="1"/>
<connect gate="0,1" pin="VIN" pad="7"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10239960/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TIOL1115" prefix="U">
<gates>
<gate name="G$1" symbol="TIOL111_SYMBOL" x="-25.4" y="10.16"/>
</gates>
<devices>
<device name="" package="VSON-10">
<connects>
<connect gate="G$1" pin="CQ" pad="8"/>
<connect gate="G$1" pin="EN" pad="5"/>
<connect gate="G$1" pin="ILIM_ADJ" pad="6"/>
<connect gate="G$1" pin="L+" pad="9"/>
<connect gate="G$1" pin="L-" pad="7 11"/>
<connect gate="G$1" pin="NFAULT" pad="2"/>
<connect gate="G$1" pin="RX" pad="3"/>
<connect gate="G$1" pin="TX" pad="4"/>
<connect gate="G$1" pin="VCC_IN/OUT" pad="1"/>
<connect gate="G$1" pin="WAKE" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M12-A_4_MALE_IO-LINK" prefix="M12_">
<gates>
<gate name="G$1" symbol="M12-A_4_MALE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CONEC_43-01204">
<connects>
<connect gate="G$1" pin="C/Q" pad="4"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="L+" pad="1"/>
<connect gate="G$1" pin="NC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FHLOGO" prefix="FH">
<gates>
<gate name="G$1" symbol="FHLOGO_SYMBOL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FHLOGO_PACK">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SRP6060FA-4R7M">
<description>&lt;Fixed Inductors 4.7uH 20% 11A&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SRP6060FA4R7M">
<description>&lt;b&gt;SRP6060FA-4R7M&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="0" y="2.025" dx="5.6" dy="1.55" layer="1"/>
<smd name="2" x="0" y="-2.025" dx="5.6" dy="1.55" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.3" y1="3.2" x2="3.3" y2="3.2" width="0.2" layer="51"/>
<wire x1="3.3" y1="3.2" x2="3.3" y2="-3.2" width="0.2" layer="51"/>
<wire x1="3.3" y1="-3.2" x2="-3.3" y2="-3.2" width="0.2" layer="51"/>
<wire x1="-3.3" y1="-3.2" x2="-3.3" y2="3.2" width="0.2" layer="51"/>
<wire x1="-3.3" y1="3.2" x2="3.3" y2="3.2" width="0.1" layer="21"/>
<wire x1="3.3" y1="3.2" x2="3.3" y2="-3.2" width="0.1" layer="21"/>
<wire x1="3.3" y1="-3.2" x2="-3.3" y2="-3.2" width="0.1" layer="21"/>
<wire x1="-3.3" y1="-3.2" x2="-3.3" y2="3.2" width="0.1" layer="21"/>
<wire x1="-3.8" y1="3.7" x2="3.8" y2="3.7" width="0.1" layer="51"/>
<wire x1="3.8" y1="3.7" x2="3.8" y2="-3.7" width="0.1" layer="51"/>
<wire x1="3.8" y1="-3.7" x2="-3.8" y2="-3.7" width="0.1" layer="51"/>
<wire x1="-3.8" y1="-3.7" x2="-3.8" y2="3.7" width="0.1" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="SRP6060FA-4R7M">
<wire x1="5.08" y1="0" x2="7.62" y2="0" width="0.254" layer="94" curve="-175.4"/>
<wire x1="7.62" y1="0" x2="10.16" y2="0" width="0.254" layer="94" curve="-175.4"/>
<wire x1="10.16" y1="0" x2="12.7" y2="0" width="0.254" layer="94" curve="-175.4"/>
<wire x1="12.7" y1="0" x2="15.24" y2="0" width="0.254" layer="94" curve="-175.4"/>
<text x="16.51" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="16.51" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="0" visible="pad" length="middle"/>
<pin name="2" x="20.32" y="0" visible="pad" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SRP6060FA-4R7M" prefix="L">
<description>&lt;b&gt;Fixed Inductors 4.7uH 20% 11A&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.bourns.com/docs/Product-Datasheets/SRP6060FA.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="SRP6060FA-4R7M" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SRP6060FA4R7M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ALLIED_NUMBER" value="71262025" constant="no"/>
<attribute name="ALLIED_PRICE-STOCK" value="https://www.alliedelec.com/bourns-srp6060fa-4r7m/71262025/" constant="no"/>
<attribute name="DESCRIPTION" value="Fixed Inductors 4.7uH 20% 11A" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Bourns" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="SRP6060FA-4R7M" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="652-SRP6060FA-4R7M" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=652-SRP6060FA-4R7M" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
<clearance class="0" value="0.1016"/>
</class>
<class number="2" name="GND" width="0.1524" drill="0.1524">
<clearance class="2" value="0.2032"/>
</class>
<class number="3" name="VCC" width="0.1524" drill="0">
<clearance class="3" value="0.2032"/>
</class>
<class number="4" name="IO-Link" width="0.1524" drill="0.2032">
<clearance class="4" value="0.254"/>
</class>
<class number="5" name="signals" width="0.1524" drill="0.1524">
<clearance class="5" value="0.2032"/>
</class>
</classes>
<parts>
<part name="U3" library="Arduino-clone" deviceset="NANO" device=""/>
<part name="U2" library="IO-Link_Shield" deviceset="TIOL1115" device="" value="TIOL111"/>
<part name="M12_1" library="IO-Link_Shield" deviceset="M12-A_4_MALE_IO-LINK" device=""/>
<part name="R6" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R5" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C5" library="SparkFun-Capacitors" deviceset="10UF" device="-1206-6.3V-20%" value="10uF">
<attribute name="DIELECTRIC" value="X7R"/>
<attribute name="VOLTAGE" value="25"/>
</part>
<part name="C6" library="SparkFun-Capacitors" deviceset="1.0UF" device="-1206-50V-10%" value="1.0uF">
<attribute name="DIELECTRIC" value="X7R"/>
<attribute name="VOLTAGE" value="100"/>
</part>
<part name="C7" library="SparkFun-Capacitors" library_urn="urn:adsk.eagle:library:510" deviceset="1.0UF" device="-1206-50V-10%" package3d_urn="urn:adsk.eagle:package:37426/1" value="0.1uF">
<attribute name="DIELECTRIC" value="X7R"/>
<attribute name="VOLTAGE" value="100"/>
</part>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C3" library="SparkFun-Capacitors" deviceset="1.0UF" device="-0603-16V-10%" value="0.1uF">
<attribute name="DIELECTRIC" value="X7R"/>
<attribute name="VOLTAGE" value="16"/>
</part>
<part name="D1" library="diode" library_urn="urn:adsk.eagle:library:210" deviceset="BAT60J" device="" package3d_urn="urn:adsk.eagle:package:43461/1">
<attribute name="REVERSE_POLARITY_V" value="10V"/>
</part>
<part name="FH1" library="IO-Link_Shield" deviceset="FHLOGO" device=""/>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="U1" library="IO-Link_Shield" deviceset="MP1584" device="SOIC8E_MPS" package3d_urn="urn:adsk.eagle:package:10239960/1"/>
<part name="R1" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="100k"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R3" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="40.2k"/>
<part name="R4" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="210k"/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C2" library="SparkFun-Capacitors" library_urn="urn:adsk.eagle:library:510" deviceset="100PF" device="-0603-50V-5%" package3d_urn="urn:adsk.eagle:package:37414/1" value="150pF">
<attribute name="VOLTAGE" value="10"/>
</part>
<part name="R2" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="100k"/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="L1" library="SRP6060FA-4R7M" deviceset="SRP6060FA-4R7M" device="" value="15 uH"/>
<part name="C4" library="SparkFun-Capacitors" library_urn="urn:adsk.eagle:library:510" deviceset="22UF" device="-0805-6.3V-20%" package3d_urn="urn:adsk.eagle:package:37429/1" value="22uF">
<attribute name="VOLTAGE" value="6.3V"/>
</part>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C1" library="SparkFun-Capacitors" library_urn="urn:adsk.eagle:library:510" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" package3d_urn="urn:adsk.eagle:package:37414/1" value="0.1uF">
<attribute name="VOLTAGE" value="10"/>
</part>
<part name="D2" library="diode" library_urn="urn:adsk.eagle:library:210" deviceset="BAT60J" device="" package3d_urn="urn:adsk.eagle:package:43461/1">
<attribute name="REVERSE_POLARITY_V" value="10V"/>
</part>
</parts>
<sheets>
<sheet>
<plain>
<text x="163.576" y="9.398" size="1.778" layer="94">
Project: 
IO-Link shield for Arduino Nano

Design:
Victor Chavez</text>
</plain>
<instances>
<instance part="U3" gate="G$1" x="226.06" y="73.66" smashed="yes"/>
<instance part="U2" gate="G$1" x="152.4" y="58.42" smashed="yes">
<attribute name="NAME" x="144.78" y="76.2" size="1.778" layer="95"/>
<attribute name="VALUE" x="144.78" y="43.18" size="1.778" layer="96"/>
</instance>
<instance part="M12_1" gate="G$1" x="165.1" y="104.14" smashed="yes" rot="R180">
<attribute name="NAME" x="167.64" y="93.98" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="185.166" y="115.316" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R6" gate="G$1" x="154.94" y="81.28" smashed="yes">
<attribute name="NAME" x="154.94" y="82.804" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="154.94" y="79.756" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R5" gate="G$1" x="116.84" y="66.04" smashed="yes">
<attribute name="NAME" x="116.84" y="67.564" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="116.84" y="64.516" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="GND8" gate="1" x="180.34" y="38.1" smashed="yes">
<attribute name="VALUE" x="177.8" y="35.56" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="213.36" y="139.7" smashed="yes">
<attribute name="VOLTAGE" x="213.36" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="DIELECTRIC" x="213.36" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="214.884" y="142.621" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="214.884" y="137.541" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C6" gate="G$1" x="223.52" y="139.7" smashed="yes">
<attribute name="VOLTAGE" x="223.52" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="DIELECTRIC" x="223.52" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="225.044" y="142.621" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="225.044" y="137.541" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C7" gate="G$1" x="233.68" y="139.7" smashed="yes">
<attribute name="VOLTAGE" x="233.68" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="DIELECTRIC" x="233.68" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="235.204" y="142.621" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="235.204" y="137.541" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND9" gate="1" x="223.52" y="129.54" smashed="yes">
<attribute name="VALUE" x="220.98" y="127" size="1.778" layer="96"/>
</instance>
<instance part="GND6" gate="1" x="104.14" y="53.34" smashed="yes">
<attribute name="VALUE" x="101.6" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="GND10" gate="1" x="251.46" y="53.34" smashed="yes">
<attribute name="VALUE" x="248.92" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="104.14" y="60.96" smashed="yes">
<attribute name="VOLTAGE" x="104.14" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="DIELECTRIC" x="104.14" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="105.664" y="63.881" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="105.664" y="58.801" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="D1" gate="G$1" x="76.2" y="147.32" smashed="yes" rot="R270">
<attribute name="REVERSE_POLARITY_V" x="76.2" y="147.32" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="NAME" x="78.105" y="147.066" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="72.771" y="149.606" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="FH1" gate="G$1" x="17.78" y="165.1" smashed="yes"/>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="217.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="217.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="230.505" y="5.08" size="2.54" layer="94"/>
</instance>
<instance part="U1" gate="0,1" x="50.8" y="129.54" smashed="yes">
<attribute name="VALUE" x="40.64" y="116.84" size="1.778" layer="96"/>
<attribute name="NAME" x="43.18" y="144.78" size="1.778" layer="95"/>
</instance>
<instance part="R1" gate="G$1" x="30.48" y="116.84" smashed="yes" rot="R90">
<attribute name="NAME" x="27.178" y="119.888" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="25.908" y="115.062" size="1.778" layer="96" font="vector" rot="R180" align="top-center"/>
</instance>
<instance part="GND1" gate="1" x="30.48" y="109.22" smashed="yes">
<attribute name="VALUE" x="27.94" y="106.68" size="1.778" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="86.36" y="127" smashed="yes" rot="R90">
<attribute name="NAME" x="82.55" y="128.778" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="81.28" y="123.952" size="1.778" layer="96" font="vector" rot="R180" align="top-center"/>
</instance>
<instance part="R4" gate="G$1" x="93.98" y="132.08" smashed="yes">
<attribute name="NAME" x="93.98" y="133.604" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="93.98" y="130.556" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="GND5" gate="1" x="86.36" y="119.38" smashed="yes">
<attribute name="VALUE" x="83.82" y="116.84" size="1.778" layer="96"/>
</instance>
<instance part="GND2" gate="1" x="50.8" y="109.22" smashed="yes">
<attribute name="VALUE" x="48.26" y="106.68" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="76.2" y="154.94" smashed="yes" rot="R180">
<attribute name="VALUE" x="78.74" y="157.48" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C2" gate="G$1" x="71.12" y="116.84" smashed="yes">
<attribute name="NAME" x="72.644" y="119.761" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="72.644" y="114.681" size="1.778" layer="96" font="vector"/>
<attribute name="VOLTAGE" x="71.12" y="116.84" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R2" gate="G$1" x="71.12" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="74.93" y="110.236" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="76.2" y="105.664" size="1.778" layer="96" font="vector" rot="R180" align="top-center"/>
</instance>
<instance part="GND3" gate="1" x="71.12" y="96.52" smashed="yes">
<attribute name="VALUE" x="68.58" y="93.98" size="1.778" layer="96"/>
</instance>
<instance part="L1" gate="G$1" x="81.28" y="139.7" smashed="yes">
<attribute name="NAME" x="90.17" y="143.2814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="91.44" y="138.938" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C4" gate="G$1" x="111.76" y="134.62" smashed="yes">
<attribute name="NAME" x="113.284" y="137.541" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="113.284" y="132.461" size="1.778" layer="96" font="vector"/>
<attribute name="VOLTAGE" x="111.76" y="134.62" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND7" gate="1" x="111.76" y="127" smashed="yes">
<attribute name="VALUE" x="109.22" y="124.46" size="1.778" layer="96"/>
</instance>
<instance part="C1" gate="G$1" x="58.42" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="59.436" y="157.099" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="60.706" y="154.305" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="VOLTAGE" x="58.42" y="149.86" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="D2" gate="G$1" x="121.92" y="139.7" smashed="yes">
<attribute name="REVERSE_POLARITY_V" x="121.92" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="122.174" y="141.605" size="1.778" layer="95"/>
<attribute name="VALUE" x="119.634" y="136.271" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="WAKE_PIN" class="5">
<segment>
<pinref part="U2" gate="G$1" pin="WAKE"/>
<wire x1="170.18" y1="71.12" x2="175.26" y2="71.12" width="0.1524" layer="91"/>
<label x="170.18" y="68.58" size="1.778" layer="95"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="160.02" y1="81.28" x2="175.26" y2="81.28" width="0.1524" layer="91"/>
<wire x1="175.26" y1="81.28" x2="175.26" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="D2"/>
<wire x1="236.22" y1="63.5" x2="241.3" y2="63.5" width="0.1524" layer="91"/>
<label x="241.3" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX" class="5">
<segment>
<pinref part="U2" gate="G$1" pin="RX"/>
<wire x1="134.62" y1="60.96" x2="132.08" y2="60.96" width="0.1524" layer="91"/>
<label x="129.54" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="RX1"/>
<wire x1="236.22" y1="55.88" x2="241.3" y2="55.88" width="0.1524" layer="91"/>
<label x="241.3" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="TX"/>
<wire x1="134.62" y1="55.88" x2="132.08" y2="55.88" width="0.1524" layer="91"/>
<label x="129.54" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="TX0"/>
<wire x1="236.22" y1="53.34" x2="241.3" y2="53.34" width="0.1524" layer="91"/>
<label x="241.3" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="NFAULT_PIN" class="5">
<segment>
<pinref part="U3" gate="G$1" pin="D3"/>
<wire x1="236.22" y1="66.04" x2="241.3" y2="66.04" width="0.1524" layer="91"/>
<label x="241.3" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="NFAULT"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="121.92" y1="66.04" x2="134.62" y2="66.04" width="0.1524" layer="91"/>
<label x="124.46" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="CQ" class="4">
<segment>
<pinref part="U2" gate="G$1" pin="CQ"/>
<wire x1="170.18" y1="60.96" x2="193.04" y2="60.96" width="0.1524" layer="91"/>
<wire x1="193.04" y1="60.96" x2="193.04" y2="116.84" width="0.1524" layer="91"/>
<wire x1="193.04" y1="116.84" x2="151.988" y2="116.84" width="0.1524" layer="91"/>
<pinref part="M12_1" gate="G$1" pin="C/Q"/>
<wire x1="151.988" y1="116.84" x2="151.988" y2="106.738" width="0.1524" layer="91"/>
<label x="182.88" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="L+" class="4">
<segment>
<pinref part="M12_1" gate="G$1" pin="L+"/>
<wire x1="152.136" y1="101.648" x2="152.136" y2="86.106" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="L+"/>
<wire x1="152.136" y1="86.106" x2="187.96" y2="86.106" width="0.1524" layer="91"/>
<wire x1="187.96" y1="86.106" x2="187.96" y2="66.04" width="0.1524" layer="91"/>
<wire x1="187.96" y1="66.04" x2="170.18" y2="66.04" width="0.1524" layer="91"/>
<label x="182.88" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="213.36" y1="144.78" x2="223.52" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="223.52" y1="144.78" x2="233.68" y2="144.78" width="0.1524" layer="91"/>
<junction x="223.52" y="144.78"/>
<label x="223.52" y="147.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="0,1" pin="VIN"/>
<wire x1="35.56" y1="139.7" x2="27.94" y2="139.7" width="0.1524" layer="91"/>
<label x="27.94" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="EN" class="5">
<segment>
<pinref part="U2" gate="G$1" pin="EN"/>
<wire x1="134.62" y1="50.8" x2="132.08" y2="50.8" width="0.1524" layer="91"/>
<label x="129.54" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="D4"/>
<wire x1="236.22" y1="68.58" x2="241.3" y2="68.58" width="0.1524" layer="91"/>
<label x="241.3" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="2">
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="233.68" y1="137.16" x2="223.52" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="223.52" y1="137.16" x2="213.36" y2="137.16" width="0.1524" layer="91"/>
<junction x="223.52" y="137.16"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="223.52" y1="132.08" x2="223.52" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="104.14" y1="55.88" x2="104.14" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND2"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="236.22" y1="60.96" x2="251.46" y2="60.96" width="0.1524" layer="91"/>
<wire x1="251.46" y1="60.96" x2="251.46" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<pinref part="U2" gate="G$1" pin="L-"/>
<pinref part="U3" gate="G$1" pin="GND"/>
<pinref part="M12_1" gate="G$1" pin="GND"/>
<wire x1="170.18" y1="55.88" x2="180.34" y2="55.88" width="0.1524" layer="91"/>
<wire x1="180.34" y1="55.88" x2="190.5" y2="55.88" width="0.1524" layer="91"/>
<wire x1="190.5" y1="55.88" x2="203.2" y2="55.88" width="0.1524" layer="91"/>
<wire x1="178.078" y1="106.632" x2="190.5" y2="106.632" width="0.1524" layer="91"/>
<wire x1="190.5" y1="106.632" x2="190.5" y2="55.88" width="0.1524" layer="91"/>
<junction x="190.5" y="55.88"/>
<junction x="180.34" y="55.88"/>
<label x="185.42" y="55.88" size="1.778" layer="95"/>
<wire x1="180.34" y1="55.88" x2="180.34" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="U1" gate="0,1" pin="GND"/>
<wire x1="50.8" y1="111.76" x2="50.8" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="76.2" y1="152.4" x2="76.2" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="71.12" y1="99.06" x2="71.12" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="111.76" y1="129.54" x2="111.76" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V+" class="3">
<segment>
<pinref part="U2" gate="G$1" pin="VCC_IN/OUT"/>
<label x="129.54" y="71.12" size="1.778" layer="95"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="149.86" y1="81.28" x2="134.62" y2="81.28" width="0.1524" layer="91"/>
<wire x1="134.62" y1="81.28" x2="134.62" y2="71.12" width="0.1524" layer="91"/>
<junction x="134.62" y="71.12"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="111.76" y1="66.04" x2="111.76" y2="71.12" width="0.1524" layer="91"/>
<wire x1="111.76" y1="71.12" x2="134.62" y2="71.12" width="0.1524" layer="91"/>
<wire x1="104.14" y1="66.04" x2="111.76" y2="66.04" width="0.1524" layer="91"/>
<junction x="111.76" y="66.04"/>
<pinref part="C3" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="5.5V"/>
<wire x1="203.2" y1="60.96" x2="198.12" y2="60.96" width="0.1524" layer="91"/>
<label x="198.12" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="124.46" y1="139.7" x2="132.08" y2="139.7" width="0.1524" layer="91"/>
<label x="132.08" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="FB" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="86.36" y1="132.08" x2="88.9" y2="132.08" width="0.1524" layer="91"/>
<pinref part="U1" gate="0,1" pin="FB"/>
<wire x1="66.04" y1="132.08" x2="86.36" y2="132.08" width="0.1524" layer="91"/>
<junction x="86.36" y="132.08"/>
<label x="68.58" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="FREQ" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="U1" gate="0,1" pin="FREQ"/>
<wire x1="30.48" y1="121.92" x2="30.48" y2="124.46" width="0.1524" layer="91"/>
<wire x1="30.48" y1="124.46" x2="35.56" y2="124.46" width="0.1524" layer="91"/>
<label x="30.48" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="SW" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="76.2" y1="139.7" x2="76.2" y2="144.78" width="0.1524" layer="91"/>
<pinref part="U1" gate="0,1" pin="SW"/>
<wire x1="66.04" y1="139.7" x2="76.2" y2="139.7" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="81.28" y1="139.7" x2="76.2" y2="139.7" width="0.1524" layer="91"/>
<junction x="76.2" y="139.7"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="60.96" y1="149.86" x2="66.04" y2="149.86" width="0.1524" layer="91"/>
<wire x1="66.04" y1="149.86" x2="66.04" y2="139.7" width="0.1524" layer="91"/>
<junction x="66.04" y="139.7"/>
<label x="68.58" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="COMP" class="0">
<segment>
<pinref part="U1" gate="0,1" pin="COMP"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="66.04" y1="124.46" x2="71.12" y2="124.46" width="0.1524" layer="91"/>
<wire x1="71.12" y1="124.46" x2="71.12" y2="121.92" width="0.1524" layer="91"/>
<label x="66.04" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="71.12" y1="111.76" x2="71.12" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BST" class="0">
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="53.34" y1="149.86" x2="50.8" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U1" gate="0,1" pin="BST"/>
<wire x1="50.8" y1="149.86" x2="50.8" y2="147.32" width="0.1524" layer="91"/>
<label x="45.72" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="5V_+" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="104.14" y1="132.08" x2="99.06" y2="132.08" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="2"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="101.6" y1="139.7" x2="104.14" y2="139.7" width="0.1524" layer="91"/>
<wire x1="104.14" y1="139.7" x2="111.76" y2="139.7" width="0.1524" layer="91"/>
<wire x1="104.14" y1="132.08" x2="104.14" y2="139.7" width="0.1524" layer="91"/>
<junction x="104.14" y="139.7"/>
<label x="109.22" y="144.78" size="1.778" layer="95"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="111.76" y1="139.7" x2="119.38" y2="139.7" width="0.1524" layer="91"/>
<junction x="111.76" y="139.7"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
